<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->string('store_name');
            $table->string('store_image')->nullable();
            $table->text('store_address')->nullable();
            $table->tinyInteger('store_status')->default(0)->nullable();
            $table->string('store_lat')->nullable();
            $table->string('store_long')->nullable();
            $table->integer('totalproducts')->nullable();
            $table->integer('lowestdiscount')->nullable();
            $table->integer('totalbranches')->nullable();
            $table->integer('totalactiveoffer')->nullable();
            $table->integer('views')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
