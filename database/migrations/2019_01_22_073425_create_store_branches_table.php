<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_branches', function (Blueprint $table) {
            $table->increments('id');
//            $table->integer('userid')->nullable();
            $table->integer('storeid')->nullable();
            $table->string('branch_name');
            $table->text('branch_address')->nullable();
            $table->tinyInteger('branch_status')->default(0)->nullable();
            $table->string('branch_lat')->nullable();
            $table->string('branch_long')->nullable();
            $table->integer('views')->nullable();
            $table->string('slug')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_branches');
    }
}
