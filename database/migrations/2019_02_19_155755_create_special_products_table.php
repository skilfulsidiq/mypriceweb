<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('storeid')->default(0)->nullable();
            $table->integer('categoryid');
            $table->integer('storeproductid')->nullable();
            $table->integer('storeid')->nullable();
            $table->integer('storebranchid')->nullable();
            $table->string('productname')->nullable();
            $table->text('productimage')->nullable();
            $table->text('productdesc')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('price')->nullable();
            $table->string('measurement')->nullable();
            $table->string('size')->nullable();
            $table->date('offerexpire')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_products');
    }
}
