<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoryid');
            $table->integer('storeproductid')->nullable();
            $table->integer('storeid')->nullable();
            $table->integer('storebranchid')->nullable();
            $table->string('productname')->nullable();
            $table->text('productimage')->nullable();
            $table->text('productdesc')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('price')->nullable();
            $table->string('measurement')->nullable();
            $table->string('size')->nullable();
            $table->integer('dealprice')->nullable();
            $table->tinyInteger('isspecial')->default(0)->nullable();
            $table->integer('dealpercentage')->nullable();
            $table->integer('lowestdiscount')->nullable();
            $table->date('dealexpire')->nullable();
            $table->tinyInteger('dealstatus')->default(0)->nullable();
            $table->integer('views')->nullable();
            $table->string('slug')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
