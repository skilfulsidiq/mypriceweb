<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->nullable();
            $table->integer('storeid')->nullable();
            $table->string('store_name')->nullable();
//            $table->string('storebranch_name')->nullable();
            $table->string('store_user_name')->nullable();
            $table->string('store_user_email')->nullable();
            $table->string('role')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_users');
    }
}
