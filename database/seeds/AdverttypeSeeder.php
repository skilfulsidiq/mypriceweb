<?php

use Illuminate\Database\Seeder;

class AdverttypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advert_types')->insert([
                ['adtype_name'=>'mobilehome'],
                ['adtype_name'=>'loginpage'],
                ['adtype_name'=>'registerpage'],
                ['adtype_name'=>'dashboard']
            ]
        );
    }
}
