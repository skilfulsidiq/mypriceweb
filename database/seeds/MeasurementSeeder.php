<?php

use Illuminate\Database\Seeder;

class MeasurementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_measurements')->insert([
                ['title'=>'cl'],
                ['title'=>'kg'],
                ['title'=>'g'],
                ['title'=>'m']
            ]
        );
    }
}
