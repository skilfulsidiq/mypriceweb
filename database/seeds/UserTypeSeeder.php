<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usertypes')->insert([
            ['user_type_name'=>'admin'],
            ['user_type_name'=>'store'],
            ['user_type_name'=>'storebranch'],
            ['user_type_name'=>'user']
                ]
        );
    }
}
