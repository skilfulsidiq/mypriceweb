<?php
namespace App\Helpers;
use App\Models\Store;
use App\Models\StoreBranch;
use Illuminate\Support\Facades\Session;
use Auth;
class StoreAndBranchStatus{

    public static function storestatus(){
        $storestatus = false;
        $storenotready=false;
        $branchestatus = false;
        $storeuserstatus=false;
        $statusmessage='';
        $msgtitle='';
        $userid= Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        if($store){
            if($store->store_status == 1){
                $storestatus = true;

            }else {
                $storestatus = false;
            }
        }
       return $storestatus;

    }
}

