<?php

use App\Models\Store;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

function storestatus()
{
    $storestatus = false;
    $storenotready = false;
    $branchestatus = false;
    $storeuserstatus = false;
    $statusmessage = '';
    $msgtitle = '';
    $userid = Auth::user()->id;
    $store = Store::where(['userid' => $userid])->first();
    if ($store->store_status == 1) {
        $storestatus = true;

    } else {
        $storestatus = false;
    }
    return View::share('store3', $storestatus);
}