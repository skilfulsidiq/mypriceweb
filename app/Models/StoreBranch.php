<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class StoreBranch extends Model
{
    use Sluggable;
    use SoftDeletes;
    protected $date = ['deleted_at'];
    protected $fillable = ['storeid','branch_name','branch_address','branch_status','branch_lat','branch_long','views','slug'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'branch_name'
            ]
        ];
    }
    /*
     * Relationship
     */
    public function store(){
        return $this->belongsTo(Store::class,'storeid');
    }
    public function product(){
        return $this->hasMany(Product::class,'storebranchid');
    }
    public function storeuser(){
        return $this->belongsToMany(User::class,'store_branch_store_user','storebranchid','storeuserid');
    }
    public function specialproduct(){
        return $this->hasMany(SpecialProduct::class,'storebranchid');
    }
//    public function storeuser(){
////        return $this->hasMany(StoreUser::class,'userid');
////    }
///
    /*
     * operation
     */
    public function getBranchNameAttribute(){
        return ucwords($this->attributes['branch_name']);
    }
}
