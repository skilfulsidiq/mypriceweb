<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use SoftDeletes;
    use Sluggable;
    protected $date = ['deleted_at'];
    protected $fillable=['category_name','searchname','category_image','slug'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'category_name'
            ]
        ];
    }
    /*
     * Relationship
     */
    public function createproduct(){
        return $this->hasMany(CreateProduct::class,'categoryid');
    }
    public function product(){
        return $this->hasMany(Product::class,'categoryid');
    }
    public function storeproduct(){
        return $this->hasMany(StoreProduct::class,'categoryid');
    }
    public function specialproduct(){
        return $this->hasMany(SpecialProduct::class,'categoryid');
    }
    /*
     * operations
     */
    public function getCategoryNameAttribute(){
        return ucwords($this->attributes['category_name']);
    }
}
