<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Auth;

class Store extends Model
{
    use Sluggable;
    use SoftDeletes;
    protected $date = ['deleted_at'];
    protected $fillable = ['store_name','store_image','store_address','store_status','store_lat','store_long','views','slug','totalproducts','lowestdiscout'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'store_name'
            ]
        ];
    }
    /*
     * Relationship
     */
    public function storebranch(){
        return $this->hasMany(StoreBranch::class,'storeid');
    }
    public function product(){
        return $this->hasMany(Product::class,'storeid');
    }

    public function user(){
        return $this->belongsTo(User::class,'userid');
    }
    public function storeproduct(){
        return $this->hasMany(StoreProduct::class,'storeid');
    }
    public function storeuser(){
        return $this->hasMany(StoreUser::class,'storeid');
    }
    public function advert(){
        return $this->hasMany(Advert::class,'advert_sponsor');
    }
    public function banner(){
        return $this->hasMany(Banner::class,'storeid');
    }
    public function specialproduct(){
        return $this->hasMany(SpecialProduct::class,'storeid');
    }
    /*
     * operation
     */
    public function getStoreNameAttribute(){
        return ucwords($this->attributes['store_name']);
    }
    public function storeid(){
        $userid = Auth::user()->id;
        $storeid = $this->userid == $userid ? $this->id : 0;
        return $storeid;
    }

}
