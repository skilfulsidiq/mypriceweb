<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class StoreProduct extends Model
{
    use Sluggable;
    protected $fillable = ['storeid','createproductid','storeproduct_name','storeproduct_image','storeproduct_desc','categoryid','slug'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'storeproduct_name'
            ]
        ];
    }

    public function store(){
        return $this->belongsTo(Store::class,'storeid');
    }
    public function createproduct(){
        return $this->belongsTo(CreateProduct::class,'createproductid');
    }
    public function category(){
        return $this->belongsTo(Category::class,'categoryid');
    }
    public function specialproduct(){
        return $this->hasMany(SpecialProduct::class,'storeproductid');
    }
    public function product(){
        return $this->hasMany(Product::class,'storeproductid');
    }
}
