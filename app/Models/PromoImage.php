<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoImage extends Model
{
    protected $fillable =['bannerid','promoimage'];

    public function banner(){
        return $this->belongsTo(Banner::class,'bannerid');
    }
}
