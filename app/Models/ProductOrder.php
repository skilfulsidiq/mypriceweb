<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOrder extends Model
{
    use Sluggable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['orderid','productid','price','quantity','customer_name','customer_phone','delivering_status'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'customer_name'
            ]
        ];
    }
    /*
     * Relationship
     */
    public function order(){
        return $this->belongsTo(Order::class,'orderid');
    }
    public function product(){
        return $this->belongsTo(Product::class,'productid');
    }
}
