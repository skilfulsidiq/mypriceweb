<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class SpecialProduct extends Model
{
    use Sluggable;
//    use SoftDeletes;
//    protected $date = ['deleted_at'];
    protected $fillable = ['categoryid','createproductid', 'storeproductid', 'storeid','storebranchid','productname','quantity','price','measurementid',  'offerexpire','measurement','size','slug'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'productname'
            ]
        ];
    }

    public function category(){
        return $this->belongsTo(Category::class,'categoryid');
    }
    public function createproduct(){
        return $this->belongsTo(CreateProduct::class,'createproductid');
    }
    public function store(){
        return $this->belongsTo(Store::class,'storeid');
    }
    public function storebranch(){
        return $this->belongsTo(StoreBranch::class,'storebranchid');
    }
    public function storeproduct(){
        return $this->belongsTo(StoreProduct::class,'storeproductid');
    }
}
