<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
class Order extends Model
{
    use Sluggable;
    use SoftDeletes;
    protected $date = ['deleted_at'];
    protected $fillable = ['userid','order_number','order_status','slug'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'order_number'
            ]
        ];
    }
    /*
     * Relationship
     */
    public function user(){
        return $this->belongsTo(User::class,'userid');
    }
    public function productorder(){
        return $this->hasMany(ProductOrder::class,'orderid');
    }
}
