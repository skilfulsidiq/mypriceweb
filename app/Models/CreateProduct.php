<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class CreateProduct extends Model
{
    use SoftDeletes;
    use Sluggable;
    protected $date = ['deleted_at'];
    protected $fillable = ['categoryid','product_name','product_image','product_description','product_status','slug'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'product_name'
            ]
        ];
    }
    /*
     * Relationship
     */
    public function category(){
        return $this->belongsTo(Category::class,'categoryid');
    }
    public function product(){
        return $this->hasMany(Product::class,'createproductid');
    }
    public function storeproduct(){
        return $this->hasMany(StoreProduct::class,'createproductid');
    }
    public function specialproduct(){
        return $this->hasMany(SpecialProduct::class,'createproductid');
    }
}
