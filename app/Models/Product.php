<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use Sluggable;
    use SoftDeletes;
    protected $date = ['deleted_at'];
    protected $fillable = ['categoryid','createproductid', 'storeproductid', 'storeid','storebranchid','productname','quantity','price','measurementid','dealprice','dealstatus','dealpercentage', 'lowestdiscout',  'dealexpire','measurement','size','views','slug'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'productname'
            ]
        ];
    }
/*
 * Relationship
 */
    public function category(){
        return $this->belongsTo(Category::class,'categoryid');
    }
    public function createproduct(){
        return $this->belongsTo(CreateProduct::class,'createproductid');
    }
    public function store(){
        return $this->belongsTo(Store::class,'storeid');
    }
    public function storebranch(){
        return $this->belongsTo(StoreBranch::class,'storebranchid');
    }

    public function dealalert(){
        return $this->hasMany(DealAlert::class,'productid');
    }
    public function productorder(){
        return $this->hasMany(ProductOrder::class,'productid');
    }
    public function storeproduct(){
        return $this->belongsTo(StoreProduct::class,'storeproductid');
    }

    /*
     * Operations
     */
    public function getProductNameAttribute(){
        return ucwords($this->attributes['productname']);
    }
    public function setProductStatusAttribute(){
        $today = Carbon::now();
        if($this->attributes['dealexpire'] > $today){
            $this->attributes['dealstatus'] = 1;
        }else{
            $this->attributes['dealstatus'] =0;
        }


    }

}
