<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealAlert extends Model
{
    protected $fillable = ['productid','price','deviceid'];
    public function product(){
        return $this->belongsTo(Product::class,'productid');
    }
    public function user(){
        return $this->belongsTo(User::class,'userid');
    }
}
