<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class StoreUser extends Model
{
    use Sluggable;
    protected $fillable = ['userid','storeid','store_name', 'store_user_name','store_user_email','role','slug'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'store_user_name'
            ]
        ];
    }

    public function store(){
        return $this->belongsTo(Store::class,'storeid');
    }
    public function user(){
        return $this->belongsTo(User::class,'userid');
    }
//    public function storebranch(){
//        return $this->belongsTo(storebranch::class,'userid');
//    }
    public function storebranch(){
        return $this->belongsToMany(StoreBranch::class,'store_branch_store_user','storeuserid','storebranchid');
    }
}
