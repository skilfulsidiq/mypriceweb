<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Advert extends Model
{
    use SoftDeletes;
    use Sluggable;
    public $adspon = 'adverttype'.'advert_sponsor';
    protected $dates = ['deleted_at'];
    protected $fillable = ['advert_image','advert_sponsor','expire_date','advert_status', 'adverttype'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'expire_date'
            ]
        ];
    }

//    public function adverttype(){
//        return $this->belongsTo(AdvertType::class,'advertypeid');
//    }
    public function store(){
        return $this->belongsTo(Store::class,'advert_sponsor');
    }
}
