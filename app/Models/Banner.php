<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{

    use Sluggable;
    protected $fillable = ['promoname','storeid','bannerimage','promoexpire','slug'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'promoname'
            ]
        ];
    }



    public function store(){
        return $this->belongsTo(Store::class,'storeid');
    }
    public function promoimage(){
        return $this->hasMany(PromoImage::class,'bannerid');
    }
}
