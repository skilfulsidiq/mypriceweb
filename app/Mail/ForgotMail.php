<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $systemgen;
    public function __construct($name, $systemgen)
    {
        $this->name = $name;
        $this->systemgen = $systemgen;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->name;
        $systemgen= $this->systemgen;
        return $this->subject('Password reset')->view('emails.forgot',compact('name','systemgen'));
    }
}
