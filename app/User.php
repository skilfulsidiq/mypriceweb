<?php

namespace App;

use App\Models\DealAlert;
use App\Models\Order;
use App\Models\Store;
use App\Models\StoreBranch;
use App\Models\StoreUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use Sluggable;
//    use SoftDeletes;
//    protected $dates = ['deleted_at'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','usertype','email_verified_at','isverified','slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
//        $this->attributes['password'] = bcrypt($password);
        $this->attributes['password'] = Hash::make($password);
    }
    /*
     * Relationship
     */
    public function storeuser(){
        return $this->hasOne(StoreUser::class,'userid');
    }
    public function order(){
        return $this->hasMany(Order::class,'userid');
    }
    public function store(){
        return $this->hasMany(Store::class,'userid');
    }
    public function alert(){
        return $this->hasMany(DealAlert::class,'userid');
    }
//    public function storebranch(){
//        return $this->belongsTo(StoreBranch::class,'userid');
//    }


    /*
     * functions
     */
    public function isNew()
    {
        $authUser = $this->id;
        $store = Store::where(['userid'=>$authUser])->first();
//         dd($shop);
        if(!empty($store)){
            return false;
        }else{
            return true;
        }
    }
    public function isAdmin(){
        if($this->usertype === 'admin'){
            return true;
        }else{
            return false;
        }
    }
    public function isStore(){
        if($this->usertype === 'store'){
            return true;
        }else{
            return false;
        }
    }
    public function isStoreBranch(){
        if($this->usertype === 'storebranch'){
            return true;
        }else{
            return false;
        }
    }
}
