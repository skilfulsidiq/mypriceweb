<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\SpecialProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function tenOnGoingDeal(){
        $today = Carbon::now();
        $deals = Product::with('storebranch','store','category')->where('dealexpire','>',$today)->inRandomOrder()->take(9)->get();
        return response()->json($deals,200);
    }

    public function hotdeals(){
            $today = Carbon::now();
            $deals = Product::with('storebranch','store','category')->where('dealexpire','>',$today)->where('dealpercentage','>=',40)->inRandomOrder()->get();
            return response()->json($deals,200);
    }

    public function latestdeals(){
        $today = Carbon::now();
        $deals = Product::with('storebranch','store','category')->orderByDesc('updated_at')->where('dealexpire','>',$today)->get();
        return response()->json($deals,200);
    }


    public function onGoingDeals(){
        $today = Carbon::now();
        $deals = Product::with('storebranch','store','category')->where('dealexpire','>',$today)
            ->inRandomOrder()->get();
        return response()->json($deals,200);
    }

    public function productDetail($id){
        $products = Product::with('store','storebranch')->findOrFail($id);
        return response()->json($products,200);
    }
    public function searchProduct($product){
        $today = Carbon::now();
        $search = str_replace(' ','',strtolower($product));
        $products = Product::with('storebranch','category','store')->where('dealexpire','>',$today)->where('productname','like','%'.$search.'%')->get();
        return response()->json($products,200);
    }

    public function allProductsFromStoreBranches($storeid){
        $today = Carbon::now();
        $products = Product::with('storebranch','store','category')->where(['storeid'=>$storeid])->where('dealexpire','>',$today)->inRandomOrder()->get();
        return response()->json($products,200);
    }

    public function hotDealByCategory($categoryid){
        $today = Carbon::now();
        $products = Product::with('storebranch','store','category')->where(['categoryid'=>$categoryid])->where('dealexpire','>',$today)
            ->where('dealpercentage','>=',40)
            ->inRandomOrder()->get();
        return response()->json($products,200);
    }
    public function latestDealByCategory($categoryid){
        $today = Carbon::now();
        $products = Product::with('storebranch','store','category')->where(['categoryid'=>$categoryid])->where('dealexpire','>',$today)
            ->orderByDesc('updated_at')->get();
        return response()->json($products,200);
    }

    public function hotDealByStore($storeid){
        $today = Carbon::now();
        $products = Product::with('storebranch','store','category')->where(['storeid'=>$storeid])->where('dealexpire','>',$today)
            ->where('dealpercentage','>=',40)
            ->inRandomOrder()->get();
        return response()->json($products,200);
    }
    public function latestDealByStore($storeid){
        $today = Carbon::now();
        $products = Product::with('storebranch','store','category')->where(['storeid'=>$storeid])->where('dealexpire','>',$today)
            ->orderByDesc('updated_at')->get();
        return response()->json($products,200);
    }


    public function allSpecialProductsFromStoreBranches($storeid){
        $today = Carbon::now();
        $products = SpecialProduct::with('storebranch','store','category')->where(['storeid'=>$storeid])->where('offerexpire','>',$today)->inRandomOrder()->get();
        return response()->json($products,200);
    }


}
