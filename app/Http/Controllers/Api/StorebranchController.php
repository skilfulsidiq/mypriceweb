<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\StoreBranch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StorebranchController extends Controller
{
    public function allBranchProduct($branchid){
        $products = Product::with('storebranch','category','store')->where(['storebranchid'=>$branchid])->inRandomOrder()->get();
        return response()->json($products,200);
    }
    public function searchBranchProduct($branchid,$searchword){
        $search = str_replace(' ','',strtolower($searchword));
        $products = Product::with('storebranch','category','store')->where(['storebranchid'=>$branchid])->where('productname','like','%'.$search.'%')->inRandomOrder()->get();
        return response()->json($products,200);
    }
}
