<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\SpecialProduct;
use App\Models\Store;
use App\Models\StoreBranch;
use App\Models\StoreProduct;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
class StoreController extends Controller
{
    public function firstFiveStores(){
        $store = Store::latest()->inRandomOrder()->take(5)->get();
        return response()->json($store,200);
    }
    public function allstores(){
        $today = Carbon::now();
        $store = Store::withCount(['product'=>function($q) use ($today){
            $q->where('dealexpire','>',$today);


        },'banner'=>function($k) use ($today){
            $k->where('promoexpire','>',$today);
        } ])->where(['store_status'=>1])->get();


//        $store =Store::all();
        return response()->json($store,200);
    }
    public function storeBranches($storeid){
        $storebranches = StoreBranch::where(['storeid'=>$storeid])->get();
        return response()->json($storebranches,200);
    }
    public function storeDetails($storeid){
        $today = Carbon::now();
        $store = Store::findOrFail($storeid);
        $totalactivedeals = Product::where(['storeid'=>$storeid])->where('dealexpire','>',$today)->count();
        $result = ['store'=>$store,'activedeals'=>$totalactivedeals];
        return response()->json($result,200);

    }

    public function searchForStore($store){
        $today = Carbon::now();
        $search = str_replace(' ','',strtolower($store));
        $store = Store::withCount(['product'=>function($q) use ($today){
            $q->where('dealexpire','>',$today);


        },'banner'=>function($k) use ($today){
            $k->where('promoexpire','>',$today);
        } ])->where(['store_status'=>1])->where('store_name','like','%'.$search.'%')->get();
        return response()->json($store,200);
    }
    public function searchStoreProduct($storeid,$searchword){
        $today = Carbon::now();
        $search = str_replace(' ','',strtolower($searchword));
        $products = Product::with('storebranch','category','store')->where(['storeid'=>$storeid])->where('dealexpire','>',$today)->where('productname','like','%'.$search.'%')->inRandomOrder()->get();
        return response()->json($products,200);
    }

    public function searchStoreSpecialProduct($storeid,$searchword){
        $today = Carbon::now();
        $search = str_replace(' ','',strtolower($searchword));
        $products = SpecialProduct::with('storebranch','category','store')->where(['storeid'=>$storeid])->where('offerexpire','>',$today)->where('productname','like','%'.$search.'%')->inRandomOrder()
            ->get();
        return response()->json($products,200);
    }


}
