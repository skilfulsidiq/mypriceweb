<?php

namespace App\Http\Controllers\Api;

use App\Models\Advert;
use App\Models\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PromoImage;
use Carbon\Carbon;
class AdvertController extends Controller
{

    public function mobileAppAdvert(){
        $today = Carbon::now();
        $adverts = Advert::where('expire_date','>',$today)->where(['adverttype'=>'mobilehome'])->inRandomOrder()->take(3)->get();
        return response()->json($adverts,200);
    }



    public function advertLevelTwo(){
        $advert = Advert::where(['adverttype'=>2,'status'=>1])->inRandomOrder()->take(2)->get();
        //   $new =   $this->array_random($advert,2);
        return response()->json($advert);
    }
    public function advertLevelThree(){
        $advert = Advert::where(['adverttype'=>3,'status'=>1])->inRandomOrder()->take(3)->get();
        return response()->json($advert);
    }

    public function listBranches($id){
        $branches = DB::table('branches')->select('branches.*')->where(["shopownerid"=>$id])->get();
        return response()->json($branches);
    }

    public function bannerlist($storeid){
        $today = Carbon::now();
        $banners = Banner::with('store','promoimage')->where('promoexpire','>',$today)->where(['storeid'=>$storeid])->get();
        // dd($banners);
        
        foreach($banners as $img){
            //  $list = explode(',',$img->promoimage);
                $list = $img->promoimage;
                $pro =[];
                foreach($list as $l){
                    $pro[]=$l->promoimage;
                }
                // dd($pro);
             $img->setAttribute('galary',$pro);
             
        }
      
       
        return response()->json($banners,200);
    }
}
