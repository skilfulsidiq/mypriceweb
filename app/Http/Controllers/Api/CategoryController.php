<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function allCategory(){
        $category = Category::orderBy('category_name','asc')->get();
        return response()->json($category, 200);
    }
}
