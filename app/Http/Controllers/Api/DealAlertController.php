<?php

namespace App\Http\Controllers\Api;

use App\Models\DealAlert;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealAlertController extends Controller
{
    public function addDealAlert(Request $request){

        $deviceid = $request['deviceid'];
        $productid = $request['productid'];
        $alert = DealAlert::where(['deviceid'=>$deviceid,'productid'=>$productid])->first();
        if(!empty($alert)){
            $save = 1;
            return response()->json($save);
        }
        $alert = new DealAlert();
        $alert->userid = $request->get('userid');
        $alert->productid = $request->get('productid');
        $alert->price = $request->get('price');
        $alert->deviceid = $request->get('deviceid');
        if($alert->save()){
            $save = 2;
            return response()->json($save);

        }else{
            $save = 0;
            return response()->json($save);

        }
    }

    public function allUserAlert($userid){
        $userid = (int)$userid;
        $alert = DealAlert::where(['userid'=>$userid])->get();
        $currentalert = [];
            foreach ($alert as $a){
                $product = Product::where(['id'=>$a->productid])->first();
                $currentalert[]=$product;
            }
        return response()->json($currentalert);

    }
    public function deleteUserAlert($userid,$productid){
        $userid = (int)$userid;
        $productid =(int)$productid;
        $alert = DealAlert::where(['userid'=>$userid,'productid'=>$productid])->first();
        if($alert->delete()){
            $save = true;
            return response()->json($save);
        }else{
            $save = false;
            return response()->json($save);
        }


    }
}
