<?php
/* created by skilfulsidiq (osodiq@gmail.com)
/*  9/5/2018  11:40AM
**/
namespace App\Http\Controllers\Api;

use App\Mail\ForgotMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
//use Validator;
use Illuminate\Support\Facades\Hash;

class ApiAuthController extends Controller
{
    //
    public $successStatus = 200;

    public function login(Request $request){
        if(Auth::attempt(['email'=>request('email'),'password'=>request('password')])){
            $user = Auth::user();
            $reply = ['status'=>"success",'data'=>$user];
            return response()->json($reply,200);
        }else{
            return response()->json(['status'=>'failed'],400);
        }
    }
//    regiser
    public function register(Request $request){
        $input = $request->all();


        $validator = Validator::make($request->all(), ['name'=>'required','email'=>'required','password'=>'required']);

        if($validator->fails()){
            return response()->json(['status'=>'falied'],401);
        }
        $user = User::where(['email'=>$request['email']])->first();
        if (!empty($user->email)){
            $reply = ['status' => 'failed','message'=>'User Already Exist', 'data' => $user];
            return response()->json($reply,401);
        }

//        $input['password'] =Hash::make($input['password']);
        $input['usertype'] ='user';
        // $user = new User();
        // $user->name = $request['name'];
        // $user->email = $request['email'];
        // $user->password = $request['password'];
        // $user->usertype = 'user';
        $user = User::create($input);
        $data = ['status' => "success", 'data' => $user];
        return response()->json($data);

    }

    public function updateProfile(Request $request){
        $status='';
        $userid = $request->get('userid');
        $name = $request->get('name');
        $email = $request->get('email');

        $user = User::find($userid);
        $user->name = $name;
        $user->email = $email;
        if($user->save()){
            $status = true;
            return response()->json($status);
        }else{
            $status = false;
            return response()->json($status);
        }
    }

    public function passwordReset(Request $request){
        $email = $request->get("email");
//        $systemgen = substr(md5(uniqid(mt_rand(), true)), 0, 10);

//        $systempassword =bcrypt($systemgen);
        $digit = 4;
        $numbergen = rand(pow(10,$digit-1),pow(10,$digit)-1);
        $user = User::where(['email'=>$email])->first();
        $systemgen = $user->name.'@'.$numbergen;
        if($user){
            $name =  $user->name;
            $user->password = $systemgen;
            if($user->save()){
                Mail::to($email)->send(new ForgotMail( $name, $systemgen));
                $reply = ['status'=>"success",'message'=>'check your mail for instructions', 'data'=>$user];
                // return response()->json(['data'=>"your password has been reset, check your mail for detail"]);
                 return response()->json($reply);
            }
        }else{
            return response()->json(['error'=>"Email doesn't exist!"]);
        }


    }
    public function changePassword(Request $request){
        $email = $request->get("email");
        $oldpassword = $request->get("oldpassword");

        $newpassword = $request->get("newpassword");
        $user = User::where(['email'=>$email])->first();
        if (Hash::check($oldpassword, $user->password)) {
            // The old password matches the hash in the database
//            echo $user->name;
            $user->password = $newpassword;
            if($user->save()){
                $reply = ['status'=>"success", 'data'=>$user];
                return response()->json("password updated");
            }else{
                  $reply = ['status'=>"failed", 'message'=>'something went wrong, try again !'];
                return response()->json($reply);
            }
        }else{
            $reply = ['status'=>"failed", 'message'=>'incorrect email or password'];
            return response()->json($reply);
        }


    }

    public function updateEmail(Request $request){
        $id = $request->get('userid');
        $email = $request->get("email");
        $user = User::where(['id'=>$id])->first();
        // echo $id;
        // $user = User::where(['id'=>$id])->first();
        // echo $user;
        $user->email = $email;
        if($user->save()){
            return response()->json("password updated");
        }else{
            return response()->json("Error , try again !");
        }
    }

}
