<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

//    protected $redirectTo = '/home';
    public function redirectPath()
    {
        $loggeduser = Auth::user()->usertype;
//        dd($loggeduser);
        switch ($loggeduser) {
            case 'admin':
                return route('admin');
                break;
            case 'store':
                return route('store');
                break;
            case 'storebranch':
                return route('storebranch');
                break;
            default:
                return route('itfail');
                break;
        }
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
