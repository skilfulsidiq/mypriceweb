<?php

namespace App\Http\Controllers\Admin;

use App\Models\Advert;
use App\Models\AdvertType;
use App\Models\Banner;
use App\Models\PromoImage;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class AdvertController extends Controller
{
    public function alladverts(){
        $today = Carbon::now();
        $adverttypes = AdvertType::all();
        $stores = Store::all();
        $adverts =  Advert::orderBy('created_at','desc')->get();
        return view('admin.adverts.adverts',compact('adverttypes','adverts','stores'));
    }

    public function addAdvert(Request $request){
         $this->validate($request,['adverttype'=>'required','advert_sponsor'=>'required',
         'advert_expire'=>'required'
         ,'advert_image'=>'required|mimes:jpg,PNG,png,jpeg|max:2048']);

        $sponsor = $request['advert_sponsor'];

        $uploadname = str_replace(' ','',$sponsor);

        $advert = new Advert();
        $destinationPath= 'storage/uploads/adverts/';
        if ($request->hasFile('advert_image')) {
            $categoryphoto = $request->file('advert_image');
            $extension = $categoryphoto->getClientOriginalName();
            $filename = $uploadname . time() . '.' . $extension;
            $filename = str_replace(' ','',$filename);
            $move = $categoryphoto->move($destinationPath, $filename);
            $url = $request->getSchemeAndHttpHost();
            $photo = $url.'/'.$destinationPath.$filename;
            $advert->advert_image = $photo;
            $img = Image::make($move)->save();
        }


         $advert->adverttype = $request['adverttype'];
         $advert->advert_sponsor = $sponsor;
         $advert->expire_date = $request['advert_expire'];
         if($advert->save()){
             $notification =[
               'message'=>'advert added successfully',
               'alert-type'=>'success'
             ];
             return redirect()->route('admin.alladvert')->with($notification);
         }else{
             return back();
         }
    }
    public function updateAdvert(Request $request,$slug){
//        $this->validate($request,[
//            'advert_image'=>'mimes:jpg,PNG,png,jpeg|max:2048']);

        $sponsor = $request['advert_sponsor'];

        $uploadname = str_replace(' ','',$sponsor);

        $advert = Advert::where(['slug'=>$slug])->first();
        $destinationPath= 'storage/uploads/adverts/';
        if ($request->hasFile('advert_image')) {
            $categoryphoto = $request->file('advert_image');
            $extension = $categoryphoto->getClientOriginalName();
            $filename = $uploadname . time() . '.' . $extension;
            $filename = str_replace(' ','',$filename);
            $move = $categoryphoto->move($destinationPath, $filename);
            $url = $request->getSchemeAndHttpHost();
            $photo = $url.'/'.$destinationPath.$filename;
            $advert->advert_image = $photo;
            $img = Image::make($move)->save();
        }


        $advert->adverttype = $request['adverttype'];
        $advert->advert_sponsor = $sponsor;
        $advert->expire_date = $request['advert_expire'];
        if($advert->save()){
            $notification =[
                'message'=>'advert updated successfully',
                'alert-type'=>'success'
            ];
            return redirect()->route('admin.alladvert')->with($notification);
        }
        return back();
    }
    public function deleteAdvert($slug){
        $ad  = Advert::where(['slug'=>$slug])->first();
        $ad->delete();
        $notification =[
            'message'=>'advert deleted successfully',
            'alert-type'=>'success'
        ];
        return redirect()->route('admin.alladvert')->with($notification);
    }

    public function allbanners(){
        $today = Carbon::now();
        $banners = Banner::with('store')->latest()->get();
        $stores = Store::all();
        return view('admin.adverts.banners',compact('stores','banners'));
    }

    public function addBanner(Request $request){
        $this->validate($request,['storeid'=>'required','promoexpire'=>'required',
            'bannerimage'=>'required|mimes:jpg,PNG,png,jpeg|max:2048',
            'promoname'=>'required'

        ]);
//        dd($request->all());
        $promophoto = $request->file('promoimage');

        $storeid = $request['storeid'];
        $store = Store::where(['id'=>$storeid])->first();
        $storename = $store->store_name;
        $uploadname = str_replace(' ','',$storename);

        $banner = new Banner();
        $destinationPath= 'storage/uploads/banners/';
        if ($request->hasFile('bannerimage')) {
            $bannerphoto = $request->file('bannerimage');
            $extension = $bannerphoto->getClientOriginalName();
            $filename = $uploadname . time() . '.' . $extension;
            $filename = str_replace(' ','',$filename);
            $move = $bannerphoto->move($destinationPath, $filename);
            $url = $request->getSchemeAndHttpHost();
            $photo = $url.'/'.$destinationPath.$filename;
            $banner->bannerimage = $photo;
            $img = Image::make($move)->save();
        }

        $banner->promoname=$request['promoname'];
        $banner->storeid=$storeid;
        $banner->promoexpire=$request['promoexpire'];
        if($banner->save()){
            if(!empty($promophoto)){
                if ($request->hasFile('promoimage')) {
                    $path= 'storage/uploads/promo/';

                        foreach ($promophoto as $p){
                            $extension = $p->getClientOriginalName();
                            $filename = $uploadname . time() . '.' . $extension;
                            $filename = str_replace(' ','',$filename);
                            $move = $p->move($path, $filename);
                            $url = $request->getSchemeAndHttpHost();
                            $photo = $url.'/'.$path.$filename;
                            $img = Image::make($move)->save();
                            PromoImage::create([
                                'bannerid'=>$banner->id,
                                'promoimage'=>$photo
                            ]);
                        }

                }
            }
            $notification =[
                'message'=>'banner added successfully',
                'alert-type'=>'success'
            ];
            return redirect()->route('admin.allbanner')->with($notification);
        }else{
            return back();
        }

    }
    public function updateBanner(Request $request,$slug){



        $storeid = $request['storeid'];
        $store = Store::where(['id'=>$storeid])->first();
        $storename = $store->store_name;
        $uploadname = str_replace(' ','',$storename);

        $banner = Banner::where(['slug'=>$slug])->first();
        $destinationPath= 'storage/uploads/banners/';
        if ($request->hasFile('bannerimage')) {
            $bannerphoto = $request->file('bannerimage');
            $extension = $bannerphoto->getClientOriginalName();
            $filename = $uploadname . time() . '.' . $extension;
            $filename = str_replace(' ','',$filename);
            $move = $bannerphoto->move($destinationPath, $filename);
            $url = $request->getSchemeAndHttpHost();
            $photo = $url.'/'.$destinationPath.$filename;
            $banner->bannerimage = $photo;
            $img = Image::make($move)->save();
        }

        $banner->promoname=$request['promoname'];
        $banner->storeid=$storeid;
        $banner->promoexpire=$request['promoexpire'];
        if($banner->save()){
            $notification =[
                'message'=>'banner updated successfully',
                'alert-type'=>'success'
            ];
            return redirect()->route('admin.allbanner')->with($notification);
        }else{
            return back();
        }

    }
    public function deleteBanner($slug){
        $banner = Banner::where(['slug'=>$slug])->first();
        $banner->delete();
        $notification =[
            'message'=>'banner deleted successfully',
            'alert-type'=>'success'
        ];
        return redirect()->route('admin.allbanner')->with($notification);

    }
}
