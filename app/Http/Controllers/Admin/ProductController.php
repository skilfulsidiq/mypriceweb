<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\CreateProduct;
use App\Models\Product;
use App\Models\SpecialProduct;
use App\Models\StoreProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    public function allproduct(){
        $products = CreateProduct::where(['product_status'=>1])->latest()->get();
        $categories = Category::all();
        return view('admin.products.allproducts',compact('products','categories'));
    }
    public function pendingProduct(){
        $products = CreateProduct::where(['product_status'=>0])->latest()->get();
        return view('admin.products.request',compact('products'));
    }
    public function dealProduct(){
        $products = Product::where(['dealstatus'=>1])->latest()->get();
        return view('admin.products.request',compact('products'));
    }
    public function createProduct(){
        $categories = Category::pluck('name','id');
        return view('admin.products.createproduct',compact('categories'));
    }
    public function storeProduct(Request $request){
        $this->validate($request,['product_name'=>'required','product_image'=>'required|mimes:jpg,PNG,png,jpeg|max:2048','categoryid'=>'required']);

        $productname = $request->get('product_name');

        $uploadname = str_replace(' ','',$productname);
        $product = new CreateProduct();
        if($request->hasFile('product_image')){
            $file = $request->file('product_image');
            $filename =strtolower(str_replace(' ','',$uploadname.time().$file->getClientOriginalName()));
            $url = $request->getSchemeAndHttpHost();
            $location = 'storage/uploads/products/';

            $imagename = $location.$filename;
            $photo = $url.'/'.$imagename;
            $move = $file->move($location,$filename);
            $img = Image::make($move)->save();
            $product->product_image = $photo;

        }
        $product->product_name = $productname;
        $product->categoryid = $request->get('categoryid');
        $product->product_description = $request->get('product_description');
        $product->product_status = 1;
        if($product->save()){
            $notification = [
                'message' =>$product->product_name .' created Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.product.add')->with($notification);
        }
    }
    public function editProduct($slug){
        $categories = Category::pluck('name','id');
        $product = CreateProduct::where(['slug'=>$slug])->first();
        return view('admin.products.createproduct',compact('categories','product'));
    }
    public function updateProduct(Request $request,$slug){
        $this->validate($request,['product_name'=>'required','categoryid'=>'required']);

        $productname = $request->get('product_name');


        $uploadname = str_replace(' ','',$productname);
        $product = CreateProduct::where(['slug'=>$slug])->first();
        // $saveimage='';
//        $todeleteImg =storage_path($product->product_image);
//        if (File::exists($todeleteImg)) { // unlink or remove previous image from folder
//            unlink($todeleteImg);
//        }
        if($request->hasFile('product_image')){
            $file = $request->file('product_image');
            $filename =strtolower(str_replace(' ','',$uploadname.time().$file->getClientOriginalName()));
            $url = $request->getSchemeAndHttpHost();
            $location = 'storage/uploads/products/';

            $imagename = $location.$filename;
            $photo = $url.'/'.$imagename;
            $move = $file->move($location,$filename);
            $img = Image::make($move)->save();
            $product->product_image = $photo;

        }
        $product->product_name = $productname;
        $product->categoryid = $request->get('categoryid');
        $product->product_description = $request->get('product_description');
        $product->product_status = 1;
        if($product->save()){
            $storeproducts = StoreProduct::where(['createproductid'=>$product->id])->get();
            if(!empty($storeproducts)){
                foreach ($storeproducts as $storeproduct){
                    $storeproduct->storeproduct_name = $productname;
                    $storeproduct->categoryid= $request->get('categoryid');
                    $storeproduct->storeproduct_image=$product->product_image;
                    $storeproduct->storeproduct_description=$request->get('product_description');
                    $storeproduct->save();
                    $coreproducts=Product::where(['storeproductid'=>$storeproduct->id])->get();
                    //  $corespecialproducts=SpecialProduct::where(['storeproductid'=>$storeproduct->id])->get();
                    if(!empty($coreproducts)){
                        foreach ($coreproducts as $coreproduct){
                            $coreproduct->productname = $productname;
                            $coreproduct->productimage = $product->product_image;
                            $coreproduct->categoryid=$request->get('categoryid');
                            $coreproduct->productdesc=$request->get('product_description');
                            $coreproduct->save();
                        }

                    }
                    // if(!empty($corespecialproducts)){
                    //     foreach($corespecialproducts as $coreproduct){
                    //            $coreproduct->productname=$productname;
                    //         $coreproduct->productimage=$saveimage;
                    //         $coreproduct->categoryid=$request->get('categoryid');
                    //         $coreproduct->productdesc=$request->get('product_description');
                    //         $coreproduct->save();
                    //     }
                    // }

                }






            }

            $notification = [
                'message' =>$product->product_name .' created Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.product.all')->with($notification);
        }
    }

    public function deleteProduct($slug){
        $product =CreateProduct::where(['slug'=>$slug])->first();
        $product->delete();
        if ($product->save()){
            $notification = [
                'message' =>$product->product_name .' deleted Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.product.all')->with($notification);
        }
    }
    public function approveCreateProduct($slug){
        $product = CreateProduct::where(['slug'=>$slug])->first();
        $product->product_status = 1;
        if ($product->save()){
            $notification = [
                'message' =>$product->product_name .' activated Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.product.pending')->with($notification);
        }
    }



    public function activateProduct($slug){
        $product = Product::where(['slug'=>$slug])->first();
        $product->product_status = 1;
        if ($product->save()){
            $notification = [
                'message' =>$product->product_name .' activated Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.product.all')->with($notification);
        }
    }
    public function deactivateProduct($slug){
        $product =Product::where(['slug'=>$slug])->first();
        $product->product_status = 0;
        if ($product->save()){
            $notification = [
                'message' =>$product->product_name .' deactivated Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.product.all')->with($notification);
        }
    }
}
