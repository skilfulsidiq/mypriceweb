<?php

namespace App\Http\Controllers\Admin;

use App\Models\Advert;
use App\Models\Product;
use App\Models\Store;
use App\Models\StoreUser;
use App\User;
use App\Usertype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use session;
use DB;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function dashboard(){
        $products = Product::count();
        $stores = Store::count();
        $users = User::count();
        $adverts = Advert::count();
        $storelist = Store::all();
        return view('admin.dashboard', compact('products','users','adverts','stores','storelist'));

    }


    /*
     * Permission
     */

    public function addPermissionForm(){
        $role = Role::all();
        return view('accesslevels.adminpermission',compact($role));
    }
    public function allpermission(){
        $permissions = Permission::all();
        $roles = Role::all();
        return view('accesslevels.adminpermission',compact('permissions','roles'));
    }
    public function addNewPermission(Request $request){
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);
        $name = $request['name'];
        $roles = $request['roles'];
        $permission = new Permission();
        $permission->name = $name;

        if($permission->save()){

            if (!empty($request['roles'])) { //If one or more role is selected
                foreach ($roles as $role) {

                    $r = Role::where(['id'=> $role])->firstOrFail(); //Match input role to db record
                    $permission->assignRole($r);
//                $permission = Permission::where(['name'=> $name])->first(); //Match input //permission to db record
//                $r->givePermissionTo($permission);
                }
            }
            $notification = ['message'=>$permission->name.' is added successfully','alert-type'=>'success'];
            return redirect()->route('admin.permission')->with($notification);
        }



    }

    public function updatePermission(Request $request, $id) {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);
        $input = $request->all();
        if($permission->fill($input)->save()){
            $notification = ['message'=>'permission updated successfully','alert-type'=>'success'];

            return redirect()->route('admin.permission')
                ->with($notification);
        }



    }
    public function destroyPermission($id) {
        $permission = Permission::findOrFail($id);

        //Make it impossible to delete this specific permission
        if ($permission->name == "Administer roles & permissions") {
            $notification = ['message'=>'Cannot delete this Permission','alert-type'=>'warning'];
            return redirect()->route('admin.permission')
                ->with($notification);
        }

        $permission->delete();
        $notification = ['message'=>'Permission deleted','alert-type'=>'success'];
        return redirect()->route('admin.permissions')->with($notification);

    }
    /*
     * Roles
     */
    public function allroles(){
        $roles = Role::all();
        $permission = Permission::all();
        return view('accesslevels.adminroles',compact('roles','permission'));
    }

    public function addNewRole(Request $request) {
        //Validate name and permissions field
        $this->validate($request, [
                'name'=>'required|unique:roles',
            ]
        );

        $name = $request['name'];
        $permissions = $request['permissions'];
        $role = new Role();
        $role->name = $name;

        if($role->save()){
            if(!empty($permissions)){
                $role->syncPermissions($permissions);
                //Looping thru selected permissions
//                foreach ($permissions as $permission) {
//                    $p = Permission::where(['id'=> $permission])->firstOrFail();
//                    //Fetch the newly created role and assign permission
//                    $role = Role::where('name', '=', $name)->first();
//                    $role->givePermissionTo($p);
//                }
            }
            $notification = ['message'=>'Role'. $role->name.' added!','alert-type'=>'success'];
            return redirect()->route('admin.role')
                ->with($notification);
        }

    }

    public function updateRole(Request $request, $id) {

        $role = Role::findOrFail($id);//Get role with the given id
        //Validate name and permission fields
        $this->validate($request, [
            'name'=>'required|unique:roles,name,'.$id,
            'permissions' =>'required',
        ]);

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();

        $p_all = Permission::all();//Get all permissions

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $role->givePermissionTo($p);  //Assign permission to role
        }
        $notification = ['message'=>'Role'. $role->name.' updated!','alert-type'=>'success'];
        return redirect()->route('admin.role')
            ->with($notification);
    }
    public function destroyRole($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        $notification = ['message'=>'Role deleted!','alert-type'=>'success'];
        return redirect()->route('admin.role')
            ->with($notification);

    }

    /*
     * Create User
     */
    public function allusers()
    {
        $roles = Role::all();
        $usertypes = Usertype::all();
        $allusers = User::latest()->get();

//        dd($storeusers);
        return view('admin.users.alluser', compact('roles', 'usertypes', 'allusers'));
    }
    public function storeusers(){
        $roles = Role::all();
        $usertypes = Usertype::all();
        $storeusers = DB::table('users')->select('users.*','stores.store_name','stores.slug AS storeslug')
            ->join('stores','users.id','=','stores.userid')->where(['users.usertype'=>'store'])
            ->latest()->get();
//        dd($storeusers);
        return view('admin.users.allstoreusers',compact('roles','usertypes','storeusers'));
    }
    public function addUser(Request $request){
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6'
        ]);
        $usertype = $request['usertype'];
        $user = User::create($request->only('email', 'name', 'password','usertype'));
        $roles = $request['roles']; //Retrieving only the email and password data
        if($user){
            if (isset($roles)) {

                foreach ($roles as $role) {
                    $role_r = Role::where(['id'=> $role])->firstOrFail();
                    $user->assignRole($role_r); //Assigning role to user
                }
            }


            $notification = [
                'message' =>$user->name .' created Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.users')->with($notification);
        }

       //Retrieving the roles field
        //Checking if a role was selected


    }
    public function updateUser(Request $request, $slug)
    {

        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email',
        ]);
        $roles = $request['roles'];
        $user = User::where(['slug' => $slug])->first();
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->usertype = $request['usertype'];
        $user->password = $request['password'];
        //Retrieving only the email and password data
        if ($user->save()) {
            $storeuser = StoreUser::where(['userid'=>$user->id])->first();
            if(!empty($storeuser)){
                $storeuser->store_user_name = $user->name;
                $storeuser->store_user_email = $user->email;
                $storeuser->save();
            }


            $notification = [
                'message' => $user->name . ' updated Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->back()->with($notification);
        }
    }

        //Retrieving the roles field
        //Checking if a role was selected

    public function deleteuser($slug){
        $user = User::where(['slug'=>$slug])->first();
        $user->delete();
        $notification = [
            'message' =>$user->name .' deleted Successfully',
            'alert-type' => 'success'
        ];
        return redirect()->route('admin.users')->with($notification);
    }

    public function settings(){
        return view('admin.settings.index');

    }

    public function mobileuser(){
        $allusers = User::where(['usertype'=>'user'])->latest()->get();
        $roles = Role::all();
        $usertypes = Usertype::all();
        return view('admin.users.alluser', compact('roles', 'usertypes', 'allusers'));
    }
    public function storeBranchUsers($storeslug){
        $usertypes = Usertype::all();
        $store = Store::where(['slug'=>$storeslug])->first();
        $storeusers = StoreUser::where(['storeid'=>$store->id])->get();
        return view('admin.users.branchusers',compact('store','storeusers','usertypes'));

    }

}
