<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create() {
        //Get all roles and pass it to the view
        $roles = Role::all();
        return view('users.create', ['roles'=>$roles]);
    }

    public function store(Request $request) {
        //Validate name, email and password fields
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed'
        ]);

        $user = User::create($request->only('email', 'name', 'password','usertype')); //Retrieving only the email and password data

        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
//        if (isset($roles)) {
//
//            foreach ($roles as $role) {
//                $role_r = Role::where('id', '=', $role)->firstOrFail();
//                $user->assignRole($role_r); //Assigning role to user
//            }
//        }
        $notification = [
            'message' =>'Your complaint has been logged',
            'alert-type' => 'success'
        ];
        //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with($notification);
    }

}
