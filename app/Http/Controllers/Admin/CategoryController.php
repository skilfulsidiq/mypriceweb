<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Routing\UrlGenerator;

class CategoryController extends Controller
{
    public function index()
    {

        $categories = Category::all();
        return view('admin.category.allcategories',compact('categories'));
    }

    public function store(Request $request)
    {

        $this->validate($request,
            ['category_name'=>'required',
                'category_image'=>'required|mimes:jpg,PNG,png,jpeg|max:2048'
            ]);
        $categoryname = $request->get('category_name');

        $uploadname = str_replace(' ','',$categoryname);

        $category = new Category();
        $destinationPath= 'storage/uploads/categories/';
        if ($request->hasFile('category_image')) {
            $categoryphoto = $request->file('category_image');
            $extension = $categoryphoto->getClientOriginalName();
            $filename = $uploadname . time() . '.' . $extension;
            $filename = str_replace(' ','',$filename);
            $move = $categoryphoto->move($destinationPath, $filename);
            $url = $request->getSchemeAndHttpHost();
            $photo = $url.'/'.$destinationPath.$filename;
            $category->category_image = $photo;
            $img = Image::make($move)->save();
        }
        $category->category_name = $categoryname;
        if($category->save()){
            $notification = [
                'message' =>$category->category_name .' added successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.category.all')->with($notification);
        }else{
            return back()->withInput($request->all());
        }
    }



    public function edit($id)
    {
        $category = Category::findOrFail($id);

        $categories = Category::all();
        return view('categories.edit',compact('category','categories'));
    }

    public function update(Request $request,$slug){
        $this->validate($request,
            ['category_name'=>'required',
            ]);
        $categoryname = $request->get('name');
        $uploadname = str_replace(' ','',$categoryname);

        $category = Category::where(['slug'=>$slug])->first();
//        $todeleteImg =storage_path($category->category_image);
//        if (File::exists($todeleteImg)) { // unlink or remove previous image from folder
//            unlink($todeleteImg);
//        }
        $destinationPath= 'storage/uploads/categories/';
        if ($request->hasFile('category_image')) {
            $categoryphoto = $request->file('category_image');
            $extension = $categoryphoto->getClientOriginalName();
            $filename = $uploadname . time() . '.' . $extension;
            $filename = str_replace(' ','',$filename);
            $move = $categoryphoto->move($destinationPath, $filename);
            $url = $request->getSchemeAndHttpHost();
            $photo = $url.'/'.$destinationPath.$filename;
            $category->category_image = $photo;
            $img = Image::make($move)->save();
        }
        $category->category_name = $request->get('category_name');
        if($category->save()){
            $notification = [
                'message' =>'Category updated successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.category.all')->with($notification);
        }else{
            return back()->withInput($request->all());
        }
    }
    public function destroy($slug)
    {
        $category = Category::where(['slug'=>$slug])->first();
        if($category->delete()){
            $notification = [
                'message' =>'Category deleted successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.category.all')->with($notification);
        }
//        Category::destroy($id);

    }
}
