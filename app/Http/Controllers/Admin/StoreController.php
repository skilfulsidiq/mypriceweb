<?php

namespace App\Http\Controllers\Admin;

use App\Mail\SetupStoreMail;
use App\Mail\StoreBranchUserMail;
use App\Models\Category;
use App\Models\CreateProduct;
use App\Models\Product;
use App\Models\ProductMeasurement;
use App\Models\SpecialProduct;
use App\Models\StoreBranch;
use App\Models\StoreProduct;
use App\Models\StoreUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use Spatie\Geocoder\Facades\Geocoder;

class StoreController extends Controller
{
    public function allstores(){
        $stores = Store::where(['store_status'=>0])->latest()->get();
        return view('admin.stores.activatedstores', compact('stores'));
    }

    public function pendingStores(){
        $stores = Store::where(['store_status' => 0])->latest()->get();
        return view('admin.stores.pendingstores', compact('stores'));
    }

    public function addStore(Request $request){
        $this->validate($request, [
            'store_name' => 'required|unique:stores',
            'store_address' => 'required',
            'store_image' => 'required|mimes:jpg,PNG,png,jpeg|max:2048'
        ]);
        $storename = $request->get('store_name');
        $storeaddress = $request->get('store_address');
        $lat='';
        $long='';
        if ($storeaddress) {
            try{
                $geo = Geocoder::getCoordinatesForAddress($storeaddress);
                $lat = $geo['lat'];
                $long = $geo['lng'];
            }catch (\Exception $e){

            }
        }
        $uploadname = str_replace(' ', '', $storename);
        $store = new Store();
        if ($request->hasFile('store_image')) {
            $file = $request->file('store_image');
            $filename = strtolower(str_replace(' ', '', $uploadname . time() . $file->getClientOriginalName()));
            $url = $request->getSchemeAndHttpHost();
            $location = 'storage/uploads/stores/';

            $imagename = $location . $filename;
            $photo = $url . '/' . $imagename;
            $move = $file->move($location, $filename);
            $img = Image::make($move)->save();
            // dd($imagename);
            $store->store_image = $photo;

        }
        $store->userid = 0;
        $store->store_name = $storename;
        $store->store_address = $storeaddress;
        $store->store_long = $long;
        $store->store_lat = $lat;
        $store->store_status = 1;
        if ($store->save()) {
            $notification = [
                'message' => $storename . ' was set up Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.store.all')->with($notification);
        }
    }

    public function editStore($slug){
        $store = Store::where(['slug' => $slug])->first();
        return view('admin.stores.editstore', compact('store'));
    }

    public function updateStore(Request $request, $slug){

        $storename = $request->get('store_name');
        $storeaddress = $request->get('store_address');
        $userid = $request->get('userid');
        $lat='';
        $long='';
        if ($storeaddress) {
            try{
                $geo = Geocoder::getCoordinatesForAddress($storeaddress);
                $lat = $geo['lat'];
                $long = $geo['lng'];
            }catch (\Exception $e){

            }
        }
        $uploadname = str_replace(' ', '', $storename);
        $store = Store::where(['slug' => $slug])->first();

        if ($request->hasFile('store_image')) {
            $file = $request->file('store_image');
            $filename = strtolower(str_replace(' ', '', $uploadname . time() . $file->getClientOriginalName()));
            $url = $request->getSchemeAndHttpHost();
            $location = 'storage/uploads/stores/';

            $imagename = $location . $filename;
            $photo = $url . '/' . $imagename;
            $move = $file->move($location, $filename);
            $img = Image::make($move)->fit(150, 100)->save();
            // dd($imagename);
            $store->store_image = $photo;

        }

        $store->store_name = $storename;
        $store->store_address = $storeaddress;
        $store->store_long = $long;
        $store->store_lat = $lat;
        if ($store->save()) {
            $notification = [
                'message' => $storename . ' updated',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.store.storedetail')->with($notification);
        }
    }

    public function activateStore($slug){
        $store = Store::where(['slug' => $slug])->first();
        $user = User::findOrFail($store->userid);
        $username = $user->name;
        $useremail = $user->email;
        $store->store_status = 1;


        if ($store->save()) {
            $data = ['name' => $username, 'storename' => $store->store_name];
            try {
                Mail::to([$useremail, 'info@myprice.com.ng'])->send(new SetupStoreMail($data));
            } catch (\Exception $e) {
                $notification = [
                    'message' => $store->store_name . ' mail is not sent, reactivate to send mail',
                    'alert-type' => 'danger'
                ];
                return redirect()->route('admin.store.all')->with($notification);
            }
            $notification = [
                'message' => $store->store_name . ' activated',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.store.all')->with($notification);
        }
    }

    public function deactivateStore($slug)
    {
        $store = Store::where(['slug' => $slug])->first();
        $store->store_status = 0;
        if ($store->save()) {
            $notification = [
                'message' => $store->store_name . ' deactivated',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.store.storedetail')->with($notification);
        }
    }


    public function storedetail()
    {
        $stores = Store::where(['store_status' => 1])->orderBy('updated_at','desc')->paginate(6);

        return view('admin.stores.activatedstores', compact('stores'));
    }

    /*
     * Store branches operation
     */
    public function storebranchesAndUsers($slug)
    {
        $today = Carbon::now();
        $measurements = ProductMeasurement::all();
        $categories = Category::all();
        $store = Store::where(['slug' => $slug,'store_status'=>1])->first();
        if(!empty($store)){
            $storeid = $store->id;
            $branches = StoreBranch::where(['storeid' => $storeid])->latest()->get();
            $storeusers =  StoreUser::where(['storeid' => $storeid])->latest()->get();

            $storeproduct = StoreProduct::where(['storeid'=>$storeid])->pluck('createproductid')->toArray();
            $createproduct = CreateProduct::where(['product_status'=>1])->pluck('id')->toArray();
            $p = array_diff($createproduct,$storeproduct);//check for difference
            $toBeAddedproducts = [];
            $toBeAddedSpecialproducts=[];
            if(!empty($p)){
                foreach($p as $k){
                    $t =   CreateProduct::where(['id'=>$k])->first();
                    $toBeAddedproducts[]=$t;
                }
            }
            $storeproducts = StoreProduct::where(['storeid'=>$storeid])->get();
            $storebranchesProducts = Product::with('storebranch','category','store')->where(['storeid'=>$storeid])->get();
            $specialProducts = SpecialProduct::with('storebranch','store','category')->where(['storeid' => $storeid])->get();

            $totalstoreusers = StoreUser::where(['storeid'=>$store->id])->count();
            return view('admin.stores.storebranches', compact('branches', 'store','storeusers','toBeAddedproducts','storeproducts','storebranchesProducts','today','measurements','categories','totalstoreusers','specialProducts'));
        }else{
            return view('admin.stores.storebranches', compact('branches', 'store','storeusers','toBeAddedproducts','storeproducts','storebranchesProducts','today','measurements','categories','totalstoreusers','specialProducts'));
        }

//        dd($storebranchesProducts);

    }

    public function createstorebranch(Request $request)
    {
        $this->validate($request, ['branch_name' => 'required', 'branch_address' => 'required']);
        $branchname = $request['branch_name'];
        $address = $request['branch_address'];
        $store = $request['storeid'];
        $userid = $request['userid'];

        $lat='';
        $long='';
        if ($address) {
            try{
                $geo = Geocoder::getCoordinatesForAddress($address);
                $lat = $geo['lat'];
                $long = $geo['lng'];
            }catch (\Exception $e){

            }
        }
        $branch = new StoreBranch();
        $branch->branch_name = $branchname;
        $branch->storeid = $store;


        $branch->branch_address = $address;
        $branch->branch_lat = $lat;
        $branch->branch_long = $long;
        if($branch->save()){
            $newstore = Store::findOrFail($store);
        $newstore->totalbranches +=1;
        $newstore->save();
            $notification=['message'=> $newstore->store_name .' '.$branch->branch_name.' branch created successfully','alert-type'=>'success'];
            return redirect()->back()->with($notification);
        }
        $notification=['message'=>'something went wrong, try again','alert-type'=>'info'];
        return back()->with($notification);


    }
    public function updateStoreBranch(Request $request,$slug){
        $branchname = $request['branch_name'];
        $address = $request['branch_address'];
        $lat='';
        $long='';
        if ($address) {
            try{
                $geo = Geocoder::getCoordinatesForAddress($address);
                $lat = $geo['lat'];
                $long = $geo['lng'];
            }catch (\Exception $e){

            }
        }
        $branch = StoreBranch::where(['slug'=>$slug])->first();
        $branch->branch_name = $branchname;
        $branch->branch_address = $address;
        $branch->branch_lat = $lat;
        $branch->branch_long = $long;
        if($branch->save()){
            $notification=['message'=> $branch->branch_name.' branch updated successfully','alert-type'=>'success'];
            return redirect()->back()->with($notification);
        }
        $notification=['message'=>'something went wrong, try again','alert-type'=>'info'];
        return back()->with($notification);
    }
    public function deleteStoreBranch($slug){
        $storebranch = StoreBranch::where(['slug'=>$slug])->first();
        if($storebranch->delete()){
            $storebranch->storeuser()->sync($storebranch->id);
            $notification=['message'=>$storebranch->branch_name.' remove successfully','alert-type'=>'info'];
            return back()->with($notification);
        }
    }


    public function createStoreBranchUser(Request $request){
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6'
        ]);
        $branches = $request['storebranches'];

        $email = $request['email'];
        $user =  User::where(['email'=>$email])->first();
        $storeuser = StoreUser::where(['store_user_email'=>$email])->first();
        if(!empty($user) || !empty($storeuser)){
            $notification = [
                'message' =>  'Account for '.$email.'already exist',
                'alert-type' => 'info'
            ];
            return back()->with($notification);
        }else{
            $user = new User();
            $storeuser = new StoreUser();
            $user = new User();
            $user->name = $request['name'];
            $user->email = $email;
            $user->password = $request['password'];
            $user->usertype = 'storebranch';
            if($user->save()){
                $storeuser->userid = $user->id;
                $storeuser->storeid = $request['storeid'];
                $storeuser->store_name = $request['store_name'];
                $storeuser->store_user_name =  $request['name'];
                $storeuser->store_user_email =  $email;
                if($storeuser->save()){
                    if(!empty($branches)){
                        foreach($branches as $b){
                            $storeuser->storebranch()->attach($b);
                        }
                    }
                    $data = ['name'=>$request['name'],'email'=>$email,'password'=>$request['password']];
                    try{
                        Mail::to([$email,'osodiq@gmail.com'])->send(new StoreBranchUserMail($data));
                    }catch (\Exception $e){

                    }
                    $notification =[
                        'message'=>$user->name . ' account has been created',
                        'alert-type'=>'success'
                    ];
                    return  redirect()->back()->with($notification);
                }
            }else{
                $notification =[
                    'message'=>'something went wrong, try again!',
                    'alert-type'=>'info'
                ];
                return back()->with($notification);
            }


        }
    }
    public function updateStoreBranchUser(Request $request,$slug ){

        $branches = $request['storebranches'];
        $email = $request['email'];
        $storeuser = StoreUser::where(['slug'=>$slug])->first();

        if(!empty($storeuser)){
            $user =  User::findOrFail($storeuser->userid);

            $user->name = $request['name'];
            $user->email = $email;
            $user->password = $request['password'];
            if($user->save()){
                $storeuser->userid = $user->id;
                $storeuser->store_user_name =  $request['name'];
                $storeuser->store_user_email =  $email;
                if($storeuser->save()){
                    if(!empty($branches)){
                        foreach($branches as $b){
                            $storeuser->storebranch()->attach($b);
                        }
                    }

                    $notification =[
                        'message'=>$user->name . ' account has been updated',
                        'alert-type'=>'success'
                    ];
                    return  redirect()->back()->  with($notification);
                }
            }

        }else{
            $notification =[
                'message'=>'something went wrong, try again!',
                'alert-type'=>'info'
            ];
            return back()->with($notification);
        }




    }
    public function deleteStoreUser($slug){

        $storeuser = StoreUser::where(['slug'=>$slug])->first();
        $storeuser->storebranch()->sync($storeuser->id);
        $user = User::findOrFail($storeuser->userid);
        $storeuser->delete();
        if($user->delete()){
            $notification=['message'=>$storeuser->store_user_name.' remove successfully','alert-type'=>'info'];
            return back()->with($notification);
        }
    }

    /*
     * store products
     */

    public function addStoreProduct(Request $request,$slug){

        if($request->has('createproductid')){
            $res = $request->get('createproductid');
            $userid = Auth::user()->id;
            $store = Store::where(['slug'=>$slug])->first();
            if(!empty($store)){
                foreach ($res as $r){
                    $pro = CreateProduct::findOrfail($r);
                    $storepro = new StoreProduct();
                    $storepro->storeid =$store->id;
                    $storepro->createproductid = $pro->id;
                    $storepro->categoryid = $pro->categoryid;
                    $storepro->storeproduct_name = $pro->product_name;
                    $storepro->storeproduct_image = $pro->product_image;
                    $storepro->storeproduct_description = $pro->product_description;
                    $storepro->save();
                    $store->totalproducts +=1;
                    $store->save();

                }

                $notification = ['message'=>'Product added','alert-type'=>'success'];
                return redirect()->back()->with($notification);
            }
            else {
                $notification = ['message' => 'Your is not yet to be set up', 'alert-type' => 'info'];
                return back()->with($notification);
            }
        }else{
            $notification = ['message'=>'No product is selected','info'];
            return back()->with($notification);
        }








    }
    public function removeProduct($slug){
        $userid = Auth::user()->id;
        $storeproduct = StoreProduct::where(['slug'=>$slug])->first();
        $store = Store::where(['id'=>$storeproduct->storeid])->first();
        $storeproduct->delete();
        if(!empty($store)){
            $store->totalproducts -=1;
            $store->save();
            $notification = ['message'=>'Product deleted','alert-type'=>'success'];
            return redirect()->back()->with($notification);
        }
        $notification = ['message'=>'Store is yet to be set up','alert-type'=>'info'];
        return back()->with($notification);

    }

    public function addProduct(Request $request){

        $this->validate($request,[
            'categoryid'=>'required',
            'storebranchid'=>'required',
            'quantity'=>'required',
            'price'=>'required'
        ]);
        $producttype = $request['producttype'];
        $storeproductid = $request['productid'];
        $branchid= $request['storebranchid'];
        $price = $request['price'];
        $dealprice = $request['dealprice'];
        $storeproduct = StoreProduct::findOrFail($storeproductid);
        $productname = $storeproduct->storeproduct_name;
        $productimage = $storeproduct->storeproduct_image;
        $productdesc = $storeproduct->storeproduct_description;

        $percentage = 0;
        if(!empty($dealprice)){
            $decreaseamount = $price - $dealprice;
            $percentage = $decreaseamount/$price *100;
        }

        $product = new Product();
        $product->storeid = $storeproduct->storeid;
        $product->storebranchid = $branchid;
        $product->storeproductid = $storeproductid;
        $product->categoryid = $request['categoryid'];

        $product->productname = $productname;
        $product->productimage = $productimage;
        $product->productdesc = $productdesc;
        $product->isspecial = $producttype;

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->dealprice = $request['dealprice'];
        $product->dealexpire = $request['dealexpire'];
        $product->dealpercentage= (int)floor($percentage);
        if($product->save()){
            $lowestdiscount = Product::where(['storeid'=>$product->storeid])->min('dealpercentage');
            $store = Store::where(['id'=>$product->storeid])->first();
            if(!empty($store)){
                $store->lowestdiscount = $lowestdiscount;
                $store->save();
            }

            $notification = [
                'message'=>$product->productname.' added successfully',
                'alert_type'=>'success'
            ];
            return redirect()->back()->with($notification);
        }


    }
    public function updateProduct(Request $request,$slug){


        $price = $request['price'];
        $dealprice = $request['dealprice'];
        $producttype = $request['producttype'];
        $percentage = 0;
        if(!empty($dealprice)){
            $decreaseamount = $price - $dealprice;
            $percentage = $decreaseamount/$price *100;
        }

        $product = Product::where(['slug'=>$slug])->first();

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->dealprice = $request['dealprice'];
        $product->dealexpire = $request['dealexpire'];
        $product->dealpercentage= $percentage;
        $product->isspecial = $producttype;
        if($product->save()){
            $lowestdiscount = Product::where(['storeid'=>$product->storeid])->min('dealpercentage');
            $store = Store::where(['id'=>$product->storeid])->first();
            if(!empty($store)){
                $store->lowestdiscount = $lowestdiscount;
                $store->save();
            }
            $notification = [
                'message'=>$product->productname.' updated successfully',
                'alert_type'=>'success'
            ];
            return redirect()->back()->with($notification);
        }


    }

    public function loadProductsByCategory($branchid,$id){
        $userid = Auth::user()->id;
        $branch = StoreBranch::where(['id'=>$branchid])->first();
        $showproduct=[];
        if(!empty($branch)){

            $storeproduct = StoreProduct::where(['storeid'=>$branch->storeid,'categoryid'=>$id])->pluck('id')->toArray();
            $branchproduct = Product::where(['storebranchid'=>$branch->id,'categoryid'=>$id])->pluck('storeproductid')->toArray();
            $diff  = array_diff($storeproduct,$branchproduct);
            foreach ($diff as $new){
                $newproduct = StoreProduct::where(['id'=>$new])->get();
                $showproduct[]= $newproduct;
            }
            $product = collect($showproduct);

            return response()->json($product);
        }






    }
}
