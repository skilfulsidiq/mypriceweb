<?php

namespace App\Http\Controllers\Admin;

use App\Models\StoreBranch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreBranchController extends Controller
{
    public function allStoresBranches(){

        $branches = StoreBranch::with('store')->latest()->get();
        return view('admin.stores.storebranches',compact('branches'));
    }
}
