<?php

namespace App\Http\Controllers\Store;

use App\Models\Category;
use App\Models\CreateProduct;
use App\Models\Store;
use App\Models\StoreProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{

    public function allStoreProduct(){

       $userid = Auth::user()->id;
       $store = Store::where(['userid'=>$userid])->first();
        $products = StoreProduct::where(['storeid'=>$store->id])->get();
        $createproducts = CreateProduct::all();
        $categories  = Category::all();
        $storeproduct = StoreProduct::where(['storeid'=>$store->id])->pluck('createproductid')->toArray();
        $createproduct = CreateProduct::where(['product_status'=>1])->pluck('id')->toArray();
        $p = array_diff($createproduct,$storeproduct);
//        dd($p);
        $toBeAddedproducts = [];
        if(!empty($p)){
            foreach($p as $k){
                $t =   CreateProduct::where(['id'=>$k])->first();
                $toBeAddedproducts[]=$t;
            }
        }

        return view('store.products.allproduct',compact('products','createproducts','store','categories','toBeAddedproducts'));
    }
    public function addProductForm(){
        $userid = Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        $storeproduct = StoreProduct::where(['storeid'=>$store->id])->pluck('createproductid')->toArray();
        $createproduct = CreateProduct::where(['product_status'=>1])->pluck('id')->toArray();
        $p = array_diff($createproduct,$storeproduct);
//        dd($p);
        $toBeAddedproducts = [];
        if(!empty($p)){
            foreach($p as $k){
                $t =   CreateProduct::where(['id'=>$k])->first();
                $toBeAddedproducts[]=$t;
            }
        }
        return view('store.products.addproduct',compact('toBeAddedproducts','store'));
    }
    public function addProduct(Request $request){
//        $this->validate($request,['createproductid']);

        if($request->has('createproductid')){
            $res = $request->get('createproductid');
            $userid = Auth::user()->id;
            $store = Store::where(['userid'=>$userid])->first();
            if(!empty($store)){
                foreach ($res as $r){
                    $pro = CreateProduct::findOrfail($r);
                    $storepro = new StoreProduct();
                    $storepro->storeid = ($store->id)?$store->id:0;
                    $storepro->createproductid = $pro->id;
                    $storepro->categoryid = $pro->categoryid;
                    $storepro->storeproduct_name = $pro->product_name;
                    $storepro->storeproduct_image = $pro->product_image;
                    $storepro->storeproduct_description = $pro->product_description;
                    $storepro->save();
                    $store->totalproducts +=1;
                    $store->save();

                }

                $notification = ['message'=>'Product added','alert-type'=>'success'];
                return redirect()->route('store.product.all')->with($notification);
            }
            else {
                $notification = ['message' => 'Your is not yet to be set up', 'alert-type' => 'info'];
                return back()->with($notification);
            }
        }else{
            $notification = ['message'=>'No product is selected','info'];
            return back()->with($notification);
        }








    }
    public function removeProduct($slug){
        $userid = Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();

        if(!empty($store)){
            $storeproduct = StoreProduct::where(['slug'=>$slug])->first();
            $storeproduct->delete();
            $store->totalproducts -=1;
            $store->save();
            $notification = ['message'=>'Product deleted','alert-type'=>'success'];
            return redirect()->route('store.product.all')->with($notification);
        }
        $notification = ['message'=>'Your is yet to be set up','alert-type'=>'info'];
        return back()->with($notification);

    }

    public function requestProductForm(){

        $categories = Category::all();
        return view('store.products.addproduct',compact('categories'));
    }
    public function requestProduct(Request $request){
        $this->validate($request,['product_name'=>'required','product_image'=>'required|mimes:jpg,PNG,png,jpeg|max:2048','categoryid'=>'required']);

        $productname = $request->get('product_name');

        $uploadname = str_replace(' ','',$productname);
        $product = new CreateProduct();
        if($request->hasFile('product_image')){
            $file = $request->file('product_image');
            $filename =strtolower(str_replace(' ','',$uploadname.time().$file->getClientOriginalName()));
            $url = $request->getSchemeAndHttpHost();
            $location = 'storage/uploads/products/';

            $imagename = $location.$filename;
            $photo = $url.'/'.$imagename;
            $move = $file->move($location,$filename);
            $img = Image::make($move)->save();
            $product->product_image = $photo;

        }
        $product->product_name = $productname;
        $product->categoryid = $request->get('categoryid');
        $product->product_description = $request->get('product_description');
        if($product->save()){
            $notification = [
                'message' =>$product->product_name .' request sent Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('store.product.all')->with($notification);
        }
    }
    public function proudctByCategory($categoryid){
        $userid = Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        if(!empty($store)){
            $storeproducts = StoreProduct::where(['storeid'=>$store->id,'categoryid'=>$categoryid])->get();
            return response()->json($storeproducts,200);
        }

    }
}
