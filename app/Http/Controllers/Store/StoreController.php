<?php

namespace App\Http\Controllers\Store;

use App\Helpers\StoreAndBranchStatus;
use App\Mail\StoreBranchUserMail;
use App\Models\Advert;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductMeasurement;
use App\Models\Store;
use App\Models\StoreBranch;
use App\Models\StoreProduct;
use App\Models\StoreUser;
use App\Models\DealAlert;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use Spatie\Geocoder\Facades\Geocoder;
use Session;
use DB;
use StoreStatus;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(){

//        $storestatus = StoreAndBranchStatus::storestatus();
        $storestatus = false;
        $storenotready=false;
        $branchestatus = false;
        $storeuserstatus=false;
        $statusmessage='';
        $msgtitle='';
        $userid= Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        
        $roles = Role::all();
        if (!empty($store)){
            $storeid =$store->id;
            if($store->store_status == 1){
                $storestatus = true;
                $storeusers = StoreUser::where(['storeid'=>$storeid])->get();
                $totalstoreusers = StoreUser::where(['storeid'=>$storeid])->count();

               

                $branch = StoreBranch::where(['storeid'=>$storeid])->get();
                $totalbranch = StoreBranch::where(['storeid'=>$storeid])->count();

                $totalproduct = StoreProduct::where(['storeid'=>$storeid])->count();

                $today = Carbon::today();
                $totaldeals = Product::where(['storeid'=>$storeid])->where('dealprice','!=',null)
                ->where('dealexpire','>=',$today)->count();

                // $storeuserss = StoreUser::where(['storeid'=>$store->id])->latest()->get();

//                $storeuserss = DB::table('store_users')->select('store_users.*','store_branches.branch_name')
//                ->leftJoin('store_branches','store_users.userid','=','store_branches.userid')
//                    ->where(['store_users.storeid'=>$store->id])
//                ->latest()->get();

                if(!empty($branch) && $totalbranch > 0){
                    $branchestatus = true;

                    $readystorebranches =  StoreBranch::where(['storeid'=>$storeid])->latest()->get();

                    return view('store.dashboard',compact('store','storeusers','storestatus','storeuserstatus','msgtitle','statusmessage','roles','storenotready', 'totalstoreusers', 'totalbranch', 'totalproduct', 'totaldeals','readystorebranches'));
                }else{
                    $branchestatus = false;
                    $msgtitle = "Store Set Up";
                    $statusmessage = "You have to set up your store branch";
                    return view('store.new.newbranch',compact('store','storeusers','storeid','storestatus','storeuserstatus', 'branchestatus', 'msgtitle','statusmessage','roles','storenotready', 'totalstoreusers', 'totalbranch', 'totalproduct', 'totaldeals', 'storeuserss'));
                }

//

//                if(!empty($storeusers) && $totalstoreusers > 0){
//                    $storeuserstatus=true;
//                    return view('store.dashboard',compact('store','storeusers','storestatus','storeuserstatus','msgtitle','statusmessage','roles','storenotready', 'totalstoreusers', 'totalbranch', 'totalproduct', 'totaldeals', 'storeuserss'));
//
//                }else {
//
//                    $storeuserstatus = false;
//                    $msgtitle = "Store Set Up";
//                    $statusmessage = "You have to set up store manager and branch";
//                    return view('store.new.addmanager',compact('store','storeusers','storestatus','branchestatus','storeuserstatus','msgtitle','statusmessage','roles','storenotready', 'totalstoreusers', 'totalbranch', 'totalproduct', 'totaldeals', 'storeuserss'));
//                }


            }
            elseif ($store->store_status==0){
                $storestatus=false;
                $storenotready=true;
                $statusmessage="Note: Your ".ucfirst($store->store_name)." account will be activated after we quickly verify your application (it will take a couple of hours). When verified, you will receive an email from our support Center. Kindly contact support center for more enquires";
                $msgtitle="Congratulations! Your have successfully created your store.";
                return view('store.new.setupstore',compact('store','storeusers','storestatus','branchestatus','storeuserstatus','msgtitle','statusmessage','roles','storenotready', 'totalstoreusers', 'totalbranch', 'totalproduct', 'totaldeals', 'storeuserss'));
            }
            else{
                $storestatus = false;
                return view('store.new.setupstore',compact('store','storeusers','storestatus','branchestatus','storeuserstatus','msgtitle','statusmessage','roles','storenotready', 'totalstoreusers', 'totalbranch', 'totalproduct', 'totaldeals', 'storeuserss'));

            }

        }else{
            $storestatus = false;
            return view('store.new.setupstore',compact('store','storeusers','storestatus','branchestatus','storeuserstatus','msgtitle','statusmessage','roles','storenotready', 'totalstoreusers', 'totalbranch', 'totalproduct', 'totaldeals', 'storeuserss'));
        }
//        View::share('storestatus', $storestatus,'storeuserstatus',$storeuserstatus,
//            'branchstatus',$branchestatus);

    }

    public function setupStore(Request $request){
        $this->validate($request,[
            'store_name'=>'required',
            'store_image'=>'required|mimes:jpg,PNG,png,jpeg|max:2048'
        ]);
        $storename = $request->get('store_name');
        $storeaddress = $request->get('store_address');
        $lat='';
        $long='';
        if($storeaddress){

            try{
                $geo = Geocoder::getCoordinatesForAddress($storeaddress);
                $lat = $geo['lat'];
                $long = $geo['lng'];
            }catch(\Exception $e){}
        }
        $uploadname = str_replace(' ','',$storename);
        $store = new Store();
        if($request->hasFile('store_image')){
            $file = $request->file('store_image');
            $filename =strtolower(str_replace(' ','',$uploadname.time().$file->getClientOriginalName()));
            $url = $request->getSchemeAndHttpHost();
            $location = 'storage/uploads/stores/';

            $imagename = $location.$filename;
            $photo = $url.'/'.$imagename;
            $move = $file->move($location,$filename);
            $img = Image::make($move)->save();
            // dd($imagename);
            $store->store_image = $photo;

        }
        $store->userid = Auth::user()->id;
        $store->store_name = $storename;
        $store->store_address = $storeaddress;
        $store->store_long = $long;
        $store->store_lat = $lat;
        if($store->save()){
            $notification = [
                'message' =>$storename .' was set up Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('store')->with($notification);
        }else{
            return back();
        }
    }

    public function addStoreBranchAdmin(Request $request){
//        $userid= Auth::user()->id;
//        $store = Store::where(['userid'=>$userid])->first();
        $this->validate($request,
            ['email'=>'required','password'=>'required','name'=>'required']
        );
        $email = $request->get('email');
        $user = User::where(['email'=>$email])->first();
        if(!empty($user)){
            $notification = [
                'message'=> $user->email.' already exist',
                'alert-type'=>'info'
            ];return redirect()->route('store')->with($notification);
        }else{
            $newuser = new User();
            $newuser->name = $request['name'];
            $newuser->email = $email;
            $newuser->password = $request['password'];
            $newuser->usertype = 'storebranch';


            if($newuser->save()){
                $storeuser = new StoreUser();
                $storeuser->userid = $newuser->id;
                $storeuser->storeid = $request['storeid'];
                $storeuser->store_name = $request['store_name'];
                $storeuser->store_user_name =  $request['name'];
                $storeuser->store_user_email =  $email;
                if($storeuser->save()){
                    $data = ['name'=>$request['name'],'email'=>$email,'password'=>$request['password']];
                    try{
                        Mail::to([$email])->send(new StoreBranchUserMail($data));
                    }catch (Exception $e){

                    }
                    $notification =[
                        'message'=>$newuser->name . 'has been added',
                        'alert-type'=>'success'
                    ];
                    return redirect()->route('store')->with($notification);
                }else{
                    $notification =[
                        'message'=>$newuser->name . 'was set up with wrong credential, contact MyPrice Admin',
                        'alert-type'=>'info'
                    ];
                    return redirect()->route('store')->with($notification);
                }





            }

        }


    }

    public function setUpBranch(Request $request){
        $this->validate($request,[
            'branch_name'=>'required',
            'branch_address'=>'required'

        ]);
        $userid= Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        if(!empty($store)){
                $storeid = $store->id;
                $branchaddress = $request->get('branch_address');
                $lat='';
                $long='';
                if($branchaddress){
                    $geo = Geocoder::getCoordinatesForAddress($branchaddress);
                    $lat = $geo['lat'];
                    $long = $geo['lng'];
                }

                $branch = new StoreBranch();
                $branch->storeid = $storeid;
                $branch->branch_name = $request->get('branch_name');
                $branch->branch_address = $branchaddress;
                $branch->branch_lat = $lat;
                $branch->branch_long = $long;
                if($branch->save()){
                    $store->totalbranches+=1;
                    $store->save();
                    $notification = [
                        'message'=> $branch->branch_name .' was set up successfully',
                        'alert-type'=>'success'
                    ];
                    return redirect()->route('store')->with($notification);
                }


        }else{
            $notification = [
                'message'=> 'Incomplete store set up, contact the admin',
                'alert-type'=>'info'
            ];
            return redirect()->back()->with($notification);
        }



    }

    public function branchDetail($branchslug){
        $thebranch = StoreBranch::with('store')->where(['slug'=>$branchslug])->first();
//        dd($thebranch->store);
        $storeimage='';
        $storename='';
        $branchname = '';
        $totalbranchproducts =0;
        $measurements = ProductMeasurement::all();
        $categories = Category::all();
        $today = Carbon::now();
        if(!empty($thebranch)){
            $branchname = $thebranch->branch_name;

            $branchid=$thebranch->id;
            $totalbranchproducts= Product::where(['storebranchid'=>$branchid])->count();
            $branchproducts= Product::with('category','store','storebranch')->where(['storebranchid'=>$branchid])->get();

            return view('store.branchdetail',compact('thebranch','branchname','totalbranchproducts','branchproducts','measurements','categories','today'));
        }






    }

    public function loadProductsByCategory($branchid,$id)
    {
        $userid = Auth::user()->id;
        $branch = StoreBranch::where(['id' => $branchid])->first();
        $showproduct = [];
        if (!empty($branch)) {

            $storeproduct = StoreProduct::where(['storeid' => $branch->storeid, 'categoryid' => $id])->pluck('id')->toArray();
//            if(!empty())
            $branchproduct = Product::where(['storebranchid' => $branch->id, 'categoryid' => $id])->pluck('storeproductid')->toArray();
            $diff = array_diff($storeproduct, $branchproduct);
            foreach ($diff as $new) {
                $newproduct = StoreProduct::where(['id' => $new])->get();
                $showproduct[] = $newproduct;
            }
            $product = collect($showproduct);

            return response()->json($product);
        }
    }

    public function addBranchProduct(Request $request){

        $this->validate($request,[
            'categoryid'=>'required',
            'storebranchid'=>'required',
            'quantity'=>'required',
            'price'=>'required'
        ]);

        $storeproductid = $request['productid'];
        $branchid= $request['storebranchid'];
        $storeproduct = StoreProduct::findOrFail($storeproductid);
        $productname = $storeproduct->storeproduct_name;
        $productimage = $storeproduct->storeproduct_image;
        $productdesc = $storeproduct->storeproduct_description;



        $price = $request['price'];
        $dealprice = $request['dealprice'];
        $decreaseamount = $price - $dealprice;
        $percentage = $decreaseamount/$price *100;


        $product = new Product();
        $product->storeid = $storeproduct->storeid;
        $product->storebranchid = $branchid;
        $product->storeproductid = $storeproductid;
        $product->categoryid = $request['categoryid'];

        $product->productname = $productname;
        $product->productimage = $productimage;
        $product->productdesc = $productdesc;

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->dealprice = $request['dealprice'];
        $product->dealexpire = $request['dealexpire'];
        $product->dealpercentage= (int)floor($percentage);
        if($product->save()){
            $lowestdiscount = Product::where(['storeid'=>$product->storeid])->min('dealpercentage');
            $store = Store::where(['id'=>$product->storeid])->first();
            if(!empty($store)){
                $store->lowestdiscount = $lowestdiscount;
                $store->save();
            }

            $notification = [
                'message'=>$product->productname.' added successfully',
                'alert_type'=>'success'
            ];
            return redirect()->back()->with($notification);
        }


    }
    public function updateProduct(Request $request,$slug){

        $price = $request['price'];
        $dealprice = $request['dealprice'];
        $decreaseamount = $price - $dealprice;
        $percentage = $decreaseamount/$price *100;

        $product = Product::where(['slug'=>$slug])->first();
        if(!empty($product)){
            $product->quantity = $request['quantity'];
            $product->price = $request['price'];

            $product->measurement = $request['measurement'];
            $product->size = $request['size'];
            $product->dealprice = $request['dealprice'];
            $product->dealexpire = $request['dealexpire'];
            $product->dealpercentage= $percentage;
            if($product->save()){
                $storeid = $product->storeid;
                $lowestdiscount = Product::where(['storeid'=>$storeid])->min('dealpercentage');
                $store = Store::where(['id'=>$storeid])->first();
                if(!empty($store)){
                    $store->lowestdiscount = $lowestdiscount;
                    $store->save();
                }
                $notification = [
                    'message'=>$product->productname.' updated successfully',
                    'alert_type'=>'success'
                ];
                return redirect()->back()->with($notification);
            }
        }




    }







//    other
    public function allstoreusers(){
        $userid= Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        if(!empty($store)){
            $storeusers =  DB::table('store_users')->select('store_users.*','store_branches.branch_name')
                ->leftJoin('store_branches','store_users.userid','=','store_branches.userid')
                ->where(['store_users.storeid'=>$store->id])
                ->latest()->get();
            $storeuserss = StoreUser::where(['store_users.storeid'=>$store->id])->latest()->get();
            return view('store.userlist',compact('storeusers','store', 'storeuserss'));
        }


        $notification = [
            'message'=> 'Your store is yet to be set up',
            'alert-type'=>'info'
        ];
        return back()->with($notification);
    }

    public function allbranchlist(){
        $userid= Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        if(!empty($store)){

            $storeuserss = StoreUser::where(['storeid'=>$store->id])->latest()->get();
            $addbranchStatus=false;
            $msg='';

            $storeusers = StoreUser::where(['storeid'=>$store->id])->pluck('userid')->toArray();

            $branches = StoreBranch::where(['storeid'=>$store->id])->pluck('userid')->toArray();

            $diff = array_diff($storeusers, $branches);
//        dd($diff);
            $tobeaddedusers=[];
            if(!empty($diff)){
                $addbranchStatus = true;
                foreach ($diff as $d){
                    $users = StoreUser::where(['userid'=>$d])->first();
                    $tobeaddedusers[]=$users;

                }
            }else{
                $msg='You have to add the store manager first, before setting up store';
                $addbranchStatus=false;
            }

            return view('store.branchlist',compact('branches','store','addbranchStatus','msg','tobeaddedusers'));
        }
        $notification = [
            'message'=> 'Your store is yet to be set up',
            'alert-type'=>'info'
        ];
        return back()->with($notification);

    }

    public function addBranchUserForm(){
        $userid= Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
//
        return view('store.addbranchadmin',compact('store'));
    }
    public function addBranchForm(){
        $addbranchStatus=false;
        $msg='';
        $userid= Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        if(!empty($store)){
            $storeusers = StoreUser::where(['storeid'=>$store->id])->pluck('userid')->toArray();


            $branch = StoreBranch::where(['storeid'=>$store->id])->pluck('userid')->toArray();

            $diff = array_diff($storeusers, $branch);
//            dd($diff);
            $tobeaddedusers=[];
            if(!empty($diff)){
                $addbranchStatus = true;
                foreach ($diff as $d){
                    $users = StoreUser::where(['userid'=>$d])->first();
                    $tobeaddedusers[]=$users;

                }
            }else{
                $msg='You have to add the store manager first, before setting up store';
                $addbranchStatus=false;
            }
            return view('store.addbranch',compact('msg','tobeaddedusers','addbranchStatus','store'));
        }
        $notification = [
            'message'=> 'your store is yet to be activated',
            'alert-type'=>'success'
        ];
        return back()->with($notification);



    }
    
    public function addStoreBranchAdminSecond(Request $request){
        $this->validate($request,
            ['email'=>'required','password'=>'required','name'=>'required']
        );
        $email = $request->get('email');
        $user = User::where(['email'=>$email])->first();
        if(!empty($user)){
            $notification = [
                'message'=> $user->email.' already exist',
                'alert-type'=>'info'
            ];
            return  redirect()->route('store.branchuserlist')->  with($notification);
        }else{
            $newuser = new User();
            $newuser->name = $request['name'];
            $newuser->email = $email;
            $newuser->password = $request['password'];
            $newuser->usertype = 'storebranch';
            if($newuser->save()){
                $storeuser = new StoreUser();
                $storeuser->userid = $newuser->id;
                $storeuser->storeid = $request['storeid'];
                $storeuser->store_name = $request['store_name'];
                $storeuser->store_user_name =  $request['name'];
                $storeuser->store_user_email =  $email;
                if($storeuser->save()){
                    $data = ['name'=>$request['name'],'email'=>$email,'password'=>$request['password']];
                    try{
                        Mail::to([$email,'osodiq@gmail.com'])->send(new StoreBranchUserMail($data));
                    }catch (Exception $e){

                    }
                    $notification =[
                        'message'=>$newuser->name . 'has been added',
                        'alert-type'=>'success'
                    ];
                    return  redirect()->route('store.branchuserlist')->  with($notification);
                }
                $notification =[
                    'message'=>$newuser->name . 'was set up with wrong credential, contact MyPrice Admin',
                    'alert-type'=>'info'
                ];
                return  redirect()->route('store.branchuserlist')->  with($notification);


            }
        }
        return back();


    }

    public function updatestoreuser(Request $request, $slug)
    {
//         $this->validate($request, [
//             'name' => 'required|max:120',
//             'email' => 'required|email',
//         ]);
//         dd($request->all());
        $storeuser = StoreUser::where(['slug'=>$slug])->first();
        $storeuser->store_user_name = $request['name'];
        $storeuser->store_user_email=$request['email'];

        $user = User::where(['id' => $storeuser->userid])->first();
//        dd($user);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->password = $request['password'];
         //Retrieving only the email and password data
         if ($user->save() && $storeuser->save()) {

             $notification = [
                 'message' => $user->name . ' updated Successfully',
                 'alert-type' => 'success'
             ];
             return redirect()->route('store.branchuserlist')->with($notification);
         }
        $notification = [
            'message' => 'something went wrong, try again!',
            'alert-type' => 'danger'
        ];
         return back()->with($notification);
        
    }
    public function deleteUser($slug){
        $storeuser = StoreUser::where(['slug'=>$slug])->first();
        $user = User::where(['id'=>$storeuser->userid])->first();
        if($storeuser->delete() && $user->delete()){
            $notification = [
                'message' => $user->name . ' deleted Successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('store.branchuserlist')->with($notification);
        }
        $notification = [
            'message' => 'something went wrong, try again!',
            'alert-type' => 'danger'
        ];
        return back()->with($notification);

    }


    public function setUpBranchSecond(Request $request){

        $userid= Auth::user()->id;
        $store = Store::where(['userid'=>$userid])->first();
        $this->validate($request,[
            'branch_name'=>'required',
            'branch_address'=>'required'

        ]);

        $storeid = $store->id;
        $storename = $store->store_name;
//        dd($request->all());
        $branchaddress = $request->get('branch_address');
        if($branchaddress){
            $lat='';
            $long='';
            try{
                $geo = Geocoder::getCoordinatesForAddress($branchaddress);
                $lat = $geo['lat'];
                $long = $geo['lng'];
            }catch(\Exception $e){

            }
        }
        $branch = new StoreBranch();

        $userid = $request['userid'];

        $branch->userid = $userid;
        $branch->storeid = $storeid;
        $branch->branch_name = $request->get('branch_name');
        $branch->branch_address = $branchaddress;
        $branch->branch_lat = $lat;
        $branch->branch_long = $long;
        if($branch->save()){
            if(!empty($store)){
                $store->totalbranches+=1;
                $store->save();
            }
            $notification = [
                'message'=> $branch->branch_name .' was set up successfully',
                'alert-type'=>'success'
            ];
            return redirect()->route('store.branchuserlist')->with($notification);
        }
    }

    public function detachedUserFromBranch($slug){
        $branchuser = StoreUser::where(['slug'=>$slug])->first();
        $storebranch = StoreBranch::where(['userid'=>$branchuser->userid])->first();
        $storebranch->userid=null;
        if($storebranch->save()){
            $notification = [
                'message'=> $branchuser->store_user_name .' was removed as '.$storebranch->branch_name .' successfully',
                'alert-type'=>'success'
            ];
            return redirect()->route('store.branchuserlist')->with($notification);
        }
    }



}
