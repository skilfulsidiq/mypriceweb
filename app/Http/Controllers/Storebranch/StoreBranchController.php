<?php

namespace App\Http\Controllers\Storebranch;

use App\Models\Product;
use App\Models\ProductMeasurement;
use App\Models\Store;
use App\Models\StoreBranch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StoreBranchController extends Controller
{
    public function __construct()
    {
        $this->middleware('storebranch');
    }

    public function index()
    {
        $userid = Auth::user()->id;
        $today = Carbon::now();
        $userIsready = false;
        $measurements = ProductMeasurement::all();


        $branch = StoreBranch::with('store')->where(['userid' => $userid])->first();
        if (!empty($branch)) {
            $totalproduct = Product::where(['storebranchid' => $branch->id])->count();
            $totalactivedeal = Product::where(['storebranchid' => $branch->id])->where('dealexpire', '>', $today)
                ->count();
            $hotdeals = Product::where(['storebranchid' => $branch->id])->where('dealexpire', '>', $today)
                ->get();
            $userIsready = true;
            return view('storebranch.dashboard', compact('branch', 'hotdeals', 'totalactivedeal', 'totalproduct','userIsready','today','measurements'));
        }

        $msg="You are not assigned a branch yet, contact your store manager for action";
        return view('storebranch.new.nobranch',compact('userIsready','msg','today'));
    }

}
