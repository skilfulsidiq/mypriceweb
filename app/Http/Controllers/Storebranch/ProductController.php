<?php

namespace App\Http\Controllers\Storebranch;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductMeasurement;
use App\Models\Store;
use App\Models\StoreProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Auth;
use App\Models\StoreBranch;

class ProductController extends Controller
{
    public function allproducts(){
        $userid = Auth::user()->id;
        $today = Carbon::today();
        $branch = StoreBranch::where(['userid'=>$userid])->first();
        $measurements = ProductMeasurement::all();
        $categories = Category::all();
        if(!empty($branch)){
            $products = Product::with('category')->where(['storebranchid'=>$branch->id])->latest()->get();

            return view('storebranch.product.branchproductlist',compact('products','categories','branch','today','measurements'));
        }
        $notification = [
            'message'=>'Branch is yet to be Set up',
            'alert_type'=>'info'
        ];
        return view('storebranch.product.branchproductlist',compact('products','categories','today'))->with($notification);


    }
    public function hotDealproducts(){
        $userid = Auth::user()->id;
        $today = Carbon::today();
        $branch = StoreBranch::where(['userid'=>$userid])->first();
        $measurements = ProductMeasurement::all();
        $categories = Category::all();
        if(!empty($branch)){
            $hotdeals = Product::with('category')->where(['storebranchid' => $branch->id])->where('dealexpire', '>', $today)->latest()->get();
//            dd($branch);

            return view('storebranch.product.hotdeal',compact('hotdeals','categories','branch','today','measurements'));
        }
        $notification = [
            'message'=>'Branch is yet to be Set up',
            'alert_type'=>'info'
        ];
        return view('storebranch.product.branchproductlist',compact('products','categories','today'))->with($notification);




    }
    public function loadProductsByCategory($id){
        $userid = Auth::user()->id;
        $branch = StoreBranch::where(['userid'=>$userid])->first();
        $showproduct=[];
        if(!empty($branch)){

            $storeproduct = StoreProduct::where(['storeid'=>$branch->storeid,'categoryid'=>$id])->pluck('id')->toArray();
            $branchproduct = Product::where(['storebranchid'=>$branch->id,'categoryid'=>$id])->pluck('storeproductid')->toArray();
            $diff  = array_diff($storeproduct,$branchproduct);
            foreach ($diff as $new){
                $newproduct = StoreProduct::where(['id'=>$new])->get();
                $showproduct[]= $newproduct;
            }
                $product = collect($showproduct);
//            dd($product);
            return response()->json($product);
        }






    }
    public function addProductform(){
        $session_storeid='';
        $userid = Auth::user()->id;
        $branch = StoreBranch::where(['userid'=>$userid])->first();
        $measurements = ProductMeasurement::all();
        $categories = Category::all();
        $session_storeid =$branch->storeid;
        $showproduct=[];
        $storeproduct = StoreProduct::where(['storeid'=>$session_storeid])->pluck('id')->toArray();
        $branchproduct = Product::where(['storebranchid'=>$branch->id])->pluck('storeproductid')->toArray();
        $diff  = array_diff($storeproduct,$branchproduct);
        foreach ($diff as $new){
            $newproduct = StoreProduct::with('store','category')->where(['id'=>$new])->get();
            $showproduct[]= $newproduct;
        }
//        dd($newproduct);
        return view('storebranch.product.addproduct',compact('showproduct','categories','branch','measurements'));

    }
    public function addProduct(Request $request){

        $this->validate($request,[
            'categoryid'=>'required',
            'quantity'=>'required',
            'price'=>'required'
        ]);

        $storeproductid = $request['productid'];
//        $storeproductid = $request['productid'];
        $branchid= $request['branchid'];
        $storeproduct = StoreProduct::findOrFail($storeproductid);
        $productname = $storeproduct->storeproduct_name;
        $productimage = $storeproduct->storeproduct_image;
        $productdesc = $storeproduct->storeproduct_description;
        $storeproductid = $storeproduct->id;
        $storebranchid = $storeproduct->storebranchid;



        $price = $request['price'];
        $dealprice = $request['dealprice'];
        $decreaseamount = $price - $dealprice;
        $percentage = $decreaseamount/$price *100;


        $product = new Product();
        $product->storeid = $storeproduct->storeid;
        $product->storebranchid = $branchid;
        $product->storeproductid = $storeproductid;
        $product->categoryid = $request['categoryid'];

        $product->productname = $productname;
        $product->productimage = $productimage;
        $product->productdesc = $productdesc;

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->dealprice = $request['dealprice'];
        $product->dealexpire = $request['dealexpire'];
        $product->dealpercentage= (int)floor($percentage);
        if($product->save()){
            $lowestdiscount = Product::where(['storeid'=>$product->storeid])->min('dealpercentage');
            $store = Store::where(['id'=>$product->storeid])->first();
            if(!empty($store)){
                $store->lowestdiscount = $lowestdiscount;
                $store->save();
            }

            $notification = [
                'message'=>$product->productname.' added successfully',
                'alert_type'=>'success'
            ];
            return redirect()->route('storebranch.product.form')->with($notification);
        }


    }
    public function updateProduct(Request $request,$slug){


        $price = $request['price'];
        $dealprice = $request['dealprice'];
        $decreaseamount = $price - $dealprice;
        $percentage = $decreaseamount/$price *100;

        $product = Product::where(['slug'=>$slug])->first();

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->dealprice = $request['dealprice'];
        $product->dealexpire = $request['dealexpire'];
        $product->dealpercentage= $percentage;
        if($product->save()){
            $lowestdiscount = Product::where(['storeid'=>$product->storeid])->min('dealpercentage');
            $store = Store::where(['id'=>$product->storeid])->first();
            if(!empty($store)){
                $store->lowestdiscount = $lowestdiscount;
                $store->save();
            }
            $notification = [
                'message'=>$product->productname.' updated successfully',
                'alert_type'=>'success'
            ];
            return redirect()->route('storebranch.allproduct')->with($notification);
        }


    }
}
