<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\SpecialProduct;
use App\Models\Store;
use App\Models\StoreBranch;
use App\Models\StoreProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function resetPasswordForm(){
        return view('auth.reset');
    }
    public function resetPassword(Request $request){
        $this->validate($request,[
            'password'=> ['required', 'string', 'min:6', 'confirmed'],
            ]);
        $user = Auth::user();
        $user->password = $request['password'];
        $user->save();
        $notification = [
            'message' =>'password changed Successfully',
            'alert-type' => 'success'
        ];
        $usertype = $user->usertype;
        switch ($usertype) {
            case 'admin':
                return redirect()->route('admin')->with($notification);
                break;
            case 'store':
                return redirect()->route('store')->with($notification);
                break;
            case 'storebranch':
                return redirect()->route('storebranch')->with($notification);
                break;
            default:
                return route('itfail');
                break;
        }




    }


    public function addDiscountProduct(Request $request){

        $this->validate($request,[
            'categoryid'=>'required',
            'storebranchid'=>'required',
            'quantity'=>'required',
            'price'=>'required'
        ]);

        $storeproductid = $request['productid'];
        $branchid= $request['storebranchid'];
        $storeproduct = StoreProduct::findOrFail($storeproductid);
        $productname = $storeproduct->storeproduct_name;
        $productimage = $storeproduct->storeproduct_image;
        $productdesc = $storeproduct->storeproduct_description;



        $price = $request['price'];
        $dealprice = $request['dealprice'];
        $decreaseamount = $price - $dealprice;
        $percentage = $decreaseamount/$price *100;


        $product = new Product();
        $product->storeid = $storeproduct->storeid;
        $product->storebranchid = $branchid;
        $product->storeproductid = $storeproductid;
        $product->categoryid = $request['categoryid'];

        $product->productname = $productname;
        $product->productimage = $productimage;
        $product->productdesc = $productdesc;

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->dealprice = $request['dealprice'];
        $product->dealexpire = $request['dealexpire'];
        $product->dealpercentage= (int)floor($percentage);
        if($product->save()){
            $storeid=$product->storeid;
            $lowestdiscount = Product::where(['storeid'=>$storeid])->min('dealpercentage');
            $store = Store::where(['id'=>$storeid])->first();
            if(!empty($store)){
                $store->lowestdiscount = $lowestdiscount;
                $store->save();
            }

            $notification = [
                'message'=>$product->productname.' added successfully',
                'alert_type'=>'success'
            ];
            return redirect()->back()->with($notification);
        }


    }
    public function updateDiscountProduct(Request $request,$slug){


        $price = $request['price'];
        $dealprice = $request['dealprice'];
        $decreaseamount = $price - $dealprice;
        $percentage = $decreaseamount/$price *100;

        $product = Product::where(['slug'=>$slug])->first();

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->dealprice = $request['dealprice'];
        $product->dealexpire = $request['dealexpire'];
        $product->dealpercentage= $percentage;
        if($product->save()){
            $storeid = $product->storeid;
            $lowestdiscount = Product::where(['storeid'=>$storeid])->min('dealpercentage');
            $store = Store::where(['id'=>$storeid])->first();
            if(!empty($store)){
                $store->lowestdiscount = $lowestdiscount;
                $store->save();
            }
            $notification = [
                'message'=>$product->productname.' updated successfully',
                'alert_type'=>'success'
            ];
            return redirect()->back()->with($notification);
        }


    }

    public function addSpecialProduct(Request $request){
        $this->validate($request,[
            'categoryid'=>'required',
            'storebranchid'=>'required',
            'quantity'=>'required',
            'price'=>'required',
            'offerexpire'=>'required'
        ]);

        $storeproductid = $request['productid'];
        $branchid= $request['storebranchid'];
        $storeproduct = StoreProduct::findOrFail($storeproductid);
        $productname = $storeproduct->storeproduct_name;
        $productimage = $storeproduct->storeproduct_image;
        $productdesc = $storeproduct->storeproduct_description;



        $product = new SpecialProduct();
        $product->storeid = $storeproduct->storeid;
        $product->storebranchid = $branchid;
        $product->storeproductid = $storeproductid;
        $product->categoryid = $request['categoryid'];

        $product->productname = $productname;
        $product->productimage = $productimage;
        $product->productdesc = $productdesc;

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->offerexpire = $request['offerexpire'];
        if($product->save()){
            $storeid=$product->storeid;
            $store = Store::where(['id'=>$storeid])->first();
            if(!empty($store)){
                $store->totalactiveoffer +=1;
                $store->save();
            }

            $notification = [
                'message'=>$product->productname.' added successfully',
                'alert_type'=>'success'
            ];
            return redirect()->back()->with($notification);
        }
    }
    public function updateSpecialProduct(Request $request,$slug){
//        dd($request->all());
//        $storeproductid = $request['productid'];
//        $branchid= $request['storebranchid'];
//        $storeproduct = StoreProduct::findOrFail($storeproductid);
//        $productname = $storeproduct->storeproduct_name;
//        $productimage = $storeproduct->storeproduct_image;
//        $productdesc = $storeproduct->storeproduct_description;



        $product = SpecialProduct::where(['slug'=>$slug])->first();
//        $product->storeid = $storeproduct->storeid;
//        $product->storebranchid = $branchid;
//        $product->storeproductid = $storeproductid;
//        $product->categoryid = $request['categoryid'];

//        $product->productname = $productname;
//        $product->productimage = $productimage;
//        $product->productdesc = $productdesc;

        $product->quantity = $request['quantity'];
        $product->price = $request['price'];

        $product->measurement = $request['measurement'];
        $product->size = $request['size'];
        $product->offerexpire = $request['offerexpire'];
        if($product->save()){
//            $storeid=$product->storeid;
//            $store = Store::where(['id'=>$storeid])->first();
//            if(!empty($store)){
//                $store->totalactiveoffer +=1;
//                $store->save();
//            }

            $notification = [
                'message'=>$product->productname.' updated successfully',
                'alert_type'=>'success'
            ];
            return redirect()->back()->with($notification);
        }
    }
    public function deleteSpecialProduct($slug){
        $product = SpecialProduct::where(['slug'=>$slug])->first();
        $store = Store::where(['id'=>$product->storeid])->first();
        $product->delete();
        $store->totalactiveoffer -=1;
        $store->save();
    }

    public function loadProductsByCategory($branchid,$id){
        $userid = Auth::user()->id;
        $branch = StoreBranch::where(['id'=>$branchid])->first();
        $showproduct=[];
        if(!empty($branch)){

            $storeproduct = StoreProduct::where(['storeid'=>$branch->storeid,'categoryid'=>$id])->pluck('id')->toArray();
            $branchproduct = Product::where(['storebranchid'=>$branch->id,'categoryid'=>$id])->pluck('storeproductid')->toArray();
            $diff  = array_diff($storeproduct,$branchproduct);
            foreach ($diff as $new){
                $newproduct = StoreProduct::where(['id'=>$new])->get();
                $showproduct[]= $newproduct;
            }
            $product = collect($showproduct);

            return response()->json($product);
        }






    }
}
