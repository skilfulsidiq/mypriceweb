<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{

//    protected $auth;
//    public function __construct(Guard $auth){
//        $this->auth = $auth;
//    }

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $usetype = Auth::user()->usertype;
            switch ($usetype) {
                case 'admin':
                    return  redirect()->route('admin');
                    break;
                case 'store':
                    return  redirect()->route('store');
                    break;
                case 'storebranch':
                    return  redirect()->route('storebranch');
                    break;
                case 'user':
                    return  redirect()->route('user');
                    break;

                default:
                    # code...
                    return  redirect()->route('user');
                    break;
            }
//            return redirect('/home');
        }

        return $next($request);

//        return $next($request);
    }
}
