<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $loggeduser = Auth::user()->usertype;
            switch ($loggeduser) {
                case 'admin':
                    return $next($request);
                    break;
                case 'store':
                    return redirect()->route('store');
                    break;
                case 'storebranch':
                    return redirect()->route('storebranch');
                    break;
                default:
                    new Response(view('authorized'));
                    break;
            }
        }

    }
}
