<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'v2', 'namespace'=>'Api'],function() {

    Route::post('login', 'ApiAuthController@login');
    Route::post('register', 'ApiAuthController@register');
    Route::post('updateProfile', 'ApiAuthController@updateProfile');
    Route::post('passwordReset', 'ApiAuthController@passwordReset')->name('passwordReset');
    Route::post('changePassword', 'ApiAuthController@changePassword')->name('changePassword');
    Route::post('updateEmail', 'ApiAuthController@updateEmail')->name('changeEmail');


    //category api
    Route::get('categories','CategoryController@allCategory');

    //products
    Route::get('firstproductlist','ProductController@tenOnGoingDeal');
    Route::get('hotdealproductlist','ProductController@hotdeals');
    Route::get('latestdealproductlist','ProductController@latestdeals');
    Route::get('productdetails/{id}','ProductController@productDetail');
    Route::get('productlist','ProductController@onGoingDeals');
    Route::get('storebranchesproductlist/{storeid}','ProductController@allProductsFromStoreBranches')
    ;//
    Route::get('specialoffer/{storeid}','ProductController@allSpecialProductsFromStoreBranches');

    Route::get('searchproduct/{product}','ProductController@searchProduct');


    Route::get('hotdealbycategory/{categoryid}','ProductController@hotDealByCategory');
    Route::get('latestdealbycategory/{categoryid}','ProductController@latestDealByCategory');
    Route::get('hotdealbystore/{storeid}','ProductController@hotDealByStore');
    Route::get('latestdealbystore/{storeid}','ProductController@latestDealByStore');



    //store api


    Route::get('storelist','StoreController@allstores');//list of stores
    Route::get('storebranches/{storeid}','StoreController@storeBranches');//list of al store branches
    Route::get('storedetails/{storeid}','StoreController@storeDetails');//store detail

    Route::get('firstfivestores','StoreController@firstFiveStores');
    Route::get('searchforstore/{store}','StoreController@searchForStore');//search for store
    Route::get('searchforstoreproduct/{storeid}/{searchword}','StoreController@searchStoreProduct');
    Route::get('searchspecialproduct/{storeid}/{searchword}','StoreController@searchStoreSpecialProduct');

//branches api
    Route::get('branchproducts/{branchid}','StorebranchController@allBranchProduct');
    Route::get('searchforbranchproducts/{branchid}/{search}','StorebranchController@searchBranchProduct');

    Route::get('advertlist','AdvertController@mobileAppAdvert');
    Route::get('bannerlist/{storeid}','AdvertController@bannerlist');

    Route::post('dealalert','DealAlertController@addDealAlert');
    Route::get('alluseralert/{deviceid}','DealAlertController@allUserAlert');
    Route::get('deleteuseralert/{deviceid}/{alertid}','DealAlertController@deleteUserAlert');


});
