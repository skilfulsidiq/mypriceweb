<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use App\Models\Product;
//use App\Models\SpecialProduct;

Route::get('/', function () {

    return view('auth.login');
});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('itfail');
//general routes
Route::get('resetpassword','GeneralController@resetPasswordForm')->name('general.resetpasswordform');
Route::post('resetpassword','GeneralController@resetPassword')->name('general.resetpassword');

Route::post('addproduct/discount','GeneralController@addDiscountProduct')->name('general.product.add');
Route::post('updateproduct/discount/{slug}','GeneralController@updateDiscountProduct')->name('general.product.update');

Route::post('addproduct/special','GeneralController@addSpecialProduct')->name('general.product.addspecial');
Route::post('updateproduct/special/{slug}','GeneralController@updateSpecialProduct')->name('general.product.updatespecial');

Route::get('deleteproduct/special/{slug}','GeneralController@deleteSpecialProduct')->name('general.product.deletespecial');




     Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>'admin'],function(){

         Route::get('dashboard','AdminController@dashboard')->name('admin');
         Route::get('users','AdminController@mobileuser')->name('admin.users');
         Route::get('storeusers','AdminController@storeusers')->name('admin.users.store');
         Route::get('storebranchusers/{slug}','AdminController@storeBranchUsers')->name('admin.users.storebranch.users');

         Route::post('adduser','AdminController@addUser')->name('admin.user.add');
         Route::post('adduser/update/{slug}','AdminController@updateUser')->name('admin.user.update');
         Route::get('user/delete/{slug}','AdminController@deleteuser')->name('admin.user.delete');
         //permission
         Route::get('permission','AdminController@allpermission')->name('admin.permission');
         Route::post('permission','AdminController@addNewPermission')->name('admin.permission.add');
         Route::get('permission/edit/{id}','AdminController@editPermission')->name('admin.permission.edit');
         Route::post('permission/update/{id}','AdminController@updatePermission')->name('admin.permission.update');
         Route::get('permission/delete{id}','AdminController@destroyPermission')->name('admin.permission.delete');

         Route::get('resetpassword','AdminController@resetPassword')->name('admin.resetpassword');




         //Role
         Route::get('role','AdminController@allroles')->name('admin.role');
         Route::post('role','AdminController@addNewRole')->name('admin.role.add');
         Route::get('role/edit/{id}','AdminController@editRole')->name('admin.role.edit');
         Route::post('role/update/{id}','AdminController@updateRole')->name('admin.role.update');
         Route::get('role/delete{id}','AdminController@destroyRole')->name('admin.role.delete');


         //store
         Route::get('stores','StoreController@allstores')->name('admin.store.all');//list all store
         Route::get('stores/pending','StoreController@pendingStores')->name('admin.store.pending');//list of pending stores
         Route::get('stores/activated','StoreController@storedetail')->name('admin.store.activated');//activated store
         Route::post('stores','StoreController@addStore')->name('admin.store.add');//add stores
         Route::get('stores/edit/{id}','StoreController@editStore')->name('admin.store.edit');
         Route::post('stores/update/{slug}','StoreController@updateStore')->name('admin.store.update');
         Route::get('stores/activate/{slug}','StoreController@activateStore')->name('admin.store.activate');
         Route::get('stores/deactivate/{slug}','StoreController@deactivateStore')->name('admin.store.deactivate');
         //storebranches
         Route::get('stores/branches/{slug}','StoreController@storebranchesAndUsers')->name('admin.store.branches');
         Route::post('stores/branches','StoreController@createstorebranch')->name('admin.store.createstorebranch');
         Route::post('stores/branches/update/{slug}','StoreController@updateStoreBranch')->name('admin.store.updatestorebranch');
         Route::get('stores/branches/delete/{slug}','StoreController@deleteStoreBranch')->name('admin.store.deletestorebranch');

         Route::post('stores/users','StoreController@createStoreBranchUser')->name('admin.store.createstoreuser');
         Route::post('stores/users/update/{slug}','StoreController@updateStoreBranchUser')->name('admin.store.updatestoreuser');
         Route::get('stores/users/delete/{slug}','StoreController@deleteStoreUser')->name('admin.store.deletestoreuser');
            //store product
         Route::get('stores/product/{slug}','StoreController@storeproduct')->name('admin.store.products');
         Route::post('stores/product/{slug}','StoreController@addStoreProduct')->name('admin.store.addproduct');
         Route::get('stores/product/remove/{slug}','StoreController@removeProduct')->name('admin.store.removeproduct');

         //branches product
         Route::post('stores/branch/product/add','StoreController@addProduct')->name('admin.store.branch.addproduct');
         Route::post('stores/branch/product/update/{slug}','StoreController@updateProduct')->name('admin.store.branch.updateproduct');
         Route::get('stores/branches/loadproduct/{branchid}/{categoryid}','StoreController@loadProductsByCategory');



        //category
        Route::get('categories','CategoryController@index')->name('admin.category.all');
        Route::post('categories','CategoryController@store')->name('admin.category.add');
        Route::get('category/edit/{id}','CategoryController@edit')->name('admin.category.edit');
        Route::post('category/update/{slug}','CategoryController@update')->name('admin.category.update');
        Route::get('category/{slug}','CategoryController@destroy')->name('admin.category.delete');

        //products
         Route::get('products','ProductController@allproduct')->name('admin.product.all');
         Route::get('products/pending','ProductController@pendingProduct')->name('admin.product.pending');
         Route::get('products/deal','ProductController@dealProduct')->name('admin.product.deal');

         Route::post('products','ProductController@storeProduct')->name('admin.product.add');
         Route::get('product/edit/{slug}','ProductController@editProduct')->name('admin.product.edit');
         Route::post('product/update/{slug}','ProductController@updateProduct')->name('admin.product.update');
         Route::get('product/delete/{slug}','ProductController@deleteProduct')->name('admin.product.delete');

         Route::get('product/approve/{slug}','ProductController@approveCreateProduct')->name('admin.product.approve');


         Route::get('product/activate/{slug}','ProductController@activateProduct')->name('admin.product.activate');
         Route::get('product/deactivate/{slug}','ProductController@deactivateProduct')->name('admin.product.deactivate');


         Route::get('adverts','AdvertController@alladverts')->name('admin.alladvert');
         Route::post('adverts','AdvertController@addAdvert')->name('admin.advert.add');
         Route::post('advert/{slug}/update','AdvertController@updateAdvert')->name('admin.advert.update');
         Route::get('adverts/delete/{slug}','AdvertController@deleteAdvert')->name('admin.advert.delete');



         Route::get('banners','AdvertController@allbanners')->name('admin.allbanner');
         Route::post('banners','AdvertController@addBanner')->name('admin.banner.add');
         Route::post('banner/{slug}/update','AdvertController@updateBanner')->name('admin.banner.update');
         Route::get('banner/delete/{slug}','AdvertController@deleteBanner')->name('admin.banner.delete');
     });

    Route::group(['prefix'=>'store', 'namespace'=>'Store', 'middleware'=>'store'],function(){
        Route::get('dashboard', 'StoreController@dashboard')->name('store');

        Route::post('setupstore', 'StoreController@setupStore')->name('store.setupstore');
        Route::post('addbranchadmin', 'StoreController@addStoreBranchAdmin')->name('store.addadmin');

        Route::post('setupbranch', 'StoreController@setUpBranch')->name('store.setupbranch');
        Route::get('branchdetail/{slug}', 'StoreController@branchDetail')->name('store.branchdetail');

        //products
        Route::get('branchdetail/branches/loadproduct/{branchid}/{categoryid}','StoreController@loadProductsByCategory');
        Route::post('branches/products/add','StoreController@addBranchProduct')->name('store.branch.addproduct');
        Route::post('branch/product/update/{slug}','StoreController@updateProduct')->name('store.branch.updateproduct');




        Route::get('branchuserform', 'StoreController@addBranchUserForm')->name('store.addbranchuserform');
        Route::get('branchuserlist', 'StoreController@allstoreusers')->name('store.branchuserlist');
        Route::post('addbranchadmincont', 'StoreController@addStoreBranchAdminSecond')->name('store.addadminsecond');

        Route::post('storeuser/update/{slug}','StoreController@updatestoreuser')->name('store.user.update');
        Route::get('storeuser/delete/{slug}','StoreController@deleteUser')->name('store.user.delete');
        Route::get('unassigneduser/{slug}', 'StoreController@detachedUserFromBranch')->name('store.user.detachedUser');

        Route::get('branchform', 'StoreController@addBranchForm')->name('store.addbranchform');
        Route::get('branchlist', 'StoreController@allbranchlist')->name('store.branchlist');
        Route::post('setupbranchcont', 'StoreController@setUpBranchSecond')->name('store.setupbranchsecond');




        Route::get('storeproduct', 'ProductController@allStoreProduct')->name('store.product.all');
        Route::get('addproductform', 'ProductController@addProductForm')->name('store.product.newform');
        Route::get('requestproduct', 'ProductController@requestProductForm')->name('store.product.addform');
        Route::post('requestproduct', 'ProductController@requestProduct')->name('store.product.request');
        Route::post('addproduct', 'ProductController@addProduct')->name('store.product.add');
        Route::post('requestproduct', 'ProductController@requestProduct')->name('store.product.request');
        Route::get('deleteproduct/{slug}', 'ProductController@removeProduct')->name('store.product.delete');
        Route::get('storeproduct/bycategory/{categoryid}', 'ProductController@proudctByCategory')->name('store.productbycategory');

//        Route::get('storeuser','UserController@allStoreUser')->name('store.users');

    });



    Route::group(['prefix'=>'storebranch', 'namespace'=>'Storebranch', 'middleware'=>'storebranch'],function(){
        Route::get('dashboard', 'StoreBranchController@index')->name('storebranch');

        Route::get('branchproduct', 'ProductController@allproducts')->name('storebranch.allproduct');
        Route::get('hotdealproduct', 'ProductController@hotDealproducts')->name('storebranch.hotdeal');

        Route::get('addproduct', 'ProductController@addProductform')->name('storebranch.product.form');
        Route::get('loadproduct/{catgoryid}', 'ProductController@loadProductsByCategory')->name('storebranch.product.loadproduct');
        Route::post('addproduct', 'ProductController@addProduct')->name('storebranch.product.add');
        Route::post('updateproduct/{slug}', 'ProductController@updateProduct')->name('storebranch.product.update');
    });

