<nav class="navbar bg-success top-navbar col-lg-12 col-12 p-0">
    <div class="container">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            <a class="navbar-brand brand-logo" href="#"><img src="{{asset('static/images/price_logo.png')}}" alt=""/></a>
            <a class="navbar-brand brand-logo-mini" href=""><img src="{{asset('static/images/myprice_icon.png')}}" alt="" style="width: 50px; height: 50px;"/></a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
            <ul class="navbar-nav navbar-nav-right">

                <li class="nav-item dropdown">
                    <h5 style="font-weight: bold">{{Auth::user()->name}}</h5>
                    {{--<a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">--}}
                        {{--<i class="ti-bell mx-0"></i>--}}
                        {{--<span class="count"></span>--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">--}}
                        {{--<p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>--}}
                        {{--<a class="dropdown-item preview-item">--}}
                            {{--<div class="preview-thumbnail">--}}
                                {{--<div class="preview-icon bg-info">--}}
                                    {{--<i class="ti-user mx-0"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="preview-item-content">--}}
                                {{--<h6 class="preview-subject font-weight-normal">New user registration</h6>--}}
                                {{--<p class="font-weight-light small-text mb-0 text-muted">--}}
                                    {{--2 days ago--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                </li>
                <li class="nav-item nav-profile dropdown">
                    <a class="nav-link" href="#" data-toggle="dropdown" id="profileDropdown">
                        <img src="{{asset('static/images/avatar.png')}}" alt="Administrator"/>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                        <a href="{{ route('general.resetpasswordform')}}" class="dropdown-item">
                            <i class="ti-lock text-success"></i>
                            Change Password
                        </a>

                        <a href="{{ route('logout') }}" class="dropdown-item"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="ti-power-off text-success"></i>Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}</form>

                    </div>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
                <span class="ti-menu"></span>
            </button>
        </div>
    </div>
</nav>