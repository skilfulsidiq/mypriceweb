<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyPrice') }}</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    {{--<!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->--}}
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mycss.css') }}" rel="stylesheet">
    <!-- RGB 102 187 102 -->

    <link rel="stylesheet" href="{{asset('static/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('static/css/custom.css')}}">
        

    <link rel="stylesheet" href="{{asset('static/vendors/css/vendor.bundle.addons.css')}}">
    <link rel="stylesheet" href="{{asset('static/vendors/iconfonts/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('static/vendors/css/vendor.bundle.base.css')}}">
    

    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{asset('static/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- Datepicker --> 
    <link href="//code.jquery.com/jquery-3.3.1.min.js">
    <link rel="stylesheet" href="{{asset('static/css/datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('static/js/datepicker.js')}}">
</head>