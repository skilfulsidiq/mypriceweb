<li class="nav-item">
    <a class="nav-link" href="{{route('storebranch')}}">
        <i class="ti-home menu-icon"></i>
        <span class="menu-title">Dashboard</span>
    </a>
</li>


<li class="nav-item">
    <a href="#" class="nav-link">
    <i class="ti-shopping-cart-full menu-icon"></i>
        <span class="menu-title">Products</span>
        <i class="menu-arrow"></i></a>
    <div class="submenu">
        <ul class="submenu-item">
            <li class="nav-item"><a class="nav-link" href="{{route('storebranch.allproduct')}}">Listed Products</a></li>
            {{--<li class="nav-item"><a class="nav-link" href="">Request Product</a></li>--}}
            <li class="nav-item"><a class="nav-link" href="{{route('storebranch.hotdeal')}}">Hot Deals</a></li>
        </ul>
    </div>
</li>

{{--<li class="nav-item">--}}
    {{--<a class="nav-link" href="">--}}
    {{--<i class="ti-user menu-icon"></i>--}}
        {{--<span class="menu-title">Registered Users</span>--}}
    {{--</a>--}}
{{--</li>--}}


<li class="nav-item">
    <a href="" class="nav-link">
        <i class="ti-receipt menu-icon"></i>
        <span class="menu-title">Documentation</span>
        <i class="menu-arrow"></i></a>
    <div class="submenu">
        <ul class="submenu-item">
            <li class="nav-item"><a class="nav-link" href="">Privacy Policy</a></li>
            <li class="nav-item"><a class="nav-link" href="">Terms &amp; Conditions</a></li>
        </ul>
    </div>
</li>


<li class="nav-item">
    <a href="" class="nav-link">
        <span class="menu-title"></span>

    </a>

</li>
<li class="nav-item">
    <a href="" class="nav-link">
        <span class="menu-title"></span>

    </a>

</li>
