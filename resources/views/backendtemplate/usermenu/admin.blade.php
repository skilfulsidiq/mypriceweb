<li class="nav-item">
    <a class="nav-link" href="{{route('admin')}}">
        <i class="ti-home menu-icon text-success"></i>
        <span class="menu-title">Dashboard</span>
    </a>
</li>


<li class="nav-item">
    <a href="#" class="nav-link">
    <i class="ti-id-badge menu-icon text-success"></i>
        <span class="menu-title">Stores</span>
        <i class="menu-arrow"></i></a>
    <div class="submenu">
        <ul class="submenu-item">
            <li class="nav-item"><a class="nav-link" href="{{route('admin.store.pending')}}">Pending Stores</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('admin.store.activated')}}">Activated Stores</a></li>
            {{--<li class="nav-item"><a class="nav-link" href="{{route('admin.store.storedetail')}}">Stores Branches</a></li>--}}
        </ul>
    </div>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{route('admin.category.all')}}">
        <i class="ti-package menu-icon text-success"></i>
        <span class="menu-title">Listed Category</span>
    </a>
</li>

<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="ti-shopping-cart-full menu-icon text-success"></i>
        <span class="menu-title">Products</span>
        <i class="menu-arrow"></i></a>
    <div class="submenu">
        <ul class="submenu-item">
            <li class="nav-item"><a class="nav-link" href="{{route('admin.product.all')}}">Listed Products</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('admin.product.pending')}}">Requested Products</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('admin.product.deal')}}">Deals</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('admin.allbanner')}}"> <i class="ti-gallery menu-icon text-success"></i> Banners</a></li>
        </ul>
    </div>
</li>


<li class="nav-item">
    <a class="nav-link" href="#">
        <i class="ti-user menu-icon text-success"></i>
        <span class="menu-title">Registered Users</span>
    </a>
    <div class="submenu">
        <ul class="submenu-item">
            <li class="nav-item"><a class="nav-link" href="{{route('admin.users.store')}}">Store Managers</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('admin.users')}}">Mobile Users</a></li>
        </ul>
    </div>


</li>


<li class="nav-item">
    <a class="nav-link"  href="{{route('admin.alladvert')}}">
        <i class="ti-gallery menu-icon text-success"></i>
        <span class="menu-title">Advert&Banner</span>
    </a>


</li>



<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="ti-panel menu-icon text-success"></i>
        <span class="menu-title">Access Levels</span>
        <i class="menu-arrow"></i></a>
    <div class="submenu">
        <ul class="submenu-item">
            <li class="nav-item"><a class="nav-link" href="{{route('admin.role')}}">Roles</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('admin.permission')}}">Permissions</a></li>
        </ul>
    </div>
</li>


<li class="nav-item">
    <a href="" class="nav-link">
        <i class="ti-receipt menu-icon text-success"></i>
        <span class="menu-title">Documentation</span>
        <i class="menu-arrow"></i></a>
    <div class="submenu">
        <ul class="submenu-item">
            <li class="nav-item"><a class="nav-link" href="">Privacy Policy</a></li>
            <li class="nav-item"><a class="nav-link" href="">Terms &amp; Conditions</a></li>
        </ul>
    </div>
</li>
