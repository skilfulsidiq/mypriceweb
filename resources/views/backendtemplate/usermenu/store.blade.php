{{--@if ($thestatus['storestatus'])--}}

    <li class="nav-item">
        <a class="nav-link" href="{{route('store')}}">
            <i class="ti-home menu-icon"></i>
            <span class="menu-title">Dashboard</span>
        </a>
    </li>
    {{--@if ($thestatus['storeuserstatus'])--}}

        {{--@if($thestatus['branchestatus'])--}}
            <li class="nav-item">
                <a href="{{route('store.branchuserlist')}}" class="nav-link">
                    <i class="ti-id-badge menu-icon"></i>
                    <span class="menu-title">Branch Managers</span>

                </a>

            </li>

            <li class="nav-item">
                <a href="{{route('store.addbranchform')}}" class="nav-link">
                    <i class="ti-map-alt menu-icon"></i>
                    <span class="menu-title">Add Store Branch</span>

                </a>
            </li>


            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="ti-shopping-cart-full menu-icon"></i>
                    <span class="menu-title">Products</span>
                    <i class="menu-arrow"></i></a>
                <div class="submenu">
                    <ul class="submenu-item">
                        <li class="nav-item"><a class="nav-link" href="{{route('store.product.all')}}">Store Products</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('store.product.addform')}}">Request Product</a></li>
                    </ul>
                </div>
            </li>
        {{--@endif--}}
    {{--@endif--}}

{{--@endif--}}

{{--<li class="nav-item">--}}
    {{--<a href="#" class="nav-link">--}}
        {{--<i class="ti-panel menu-icon"></i>--}}
        {{--<span class="menu-title">Access Levels</span>--}}
        {{--<i class="menu-arrow"></i></a>--}}
    {{--<div class="submenu">--}}
        {{--<ul class="submenu-item">--}}
            {{--<li class="nav-item"><a class="nav-link" href="">Roles</a></li>--}}
            {{--<li class="nav-item"><a class="nav-link" href="">Permissions</a></li>--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</li>--}}


{{--<li class="nav-item">--}}
    {{--<a href="" class="nav-link">--}}
        {{--<i class="ti-receipt menu-icon"></i>--}}
        {{--<span class="menu-title">Documentation</span>--}}
        {{--<i class="menu-arrow"></i></a>--}}
    {{--<div class="submenu">--}}
        {{--<ul class="submenu-item">--}}
            {{--<li class="nav-item"><a class="nav-link" href="">Privacy Policy</a></li>--}}
            {{--<li class="nav-item"><a class="nav-link" href="">Terms &amp; Conditions</a></li>--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</li>--}}




