

    <nav class="bottom-navbar">
        <div class="container">
            <ul class="nav page-navigation">
                @if(Auth::user()->usertype == 'admin')
                    {{--@include('backendtemplate.usermenu.admin', ['data' => $data])--}}
                    @include('backendtemplate.usermenu.admin')

                @elseif(Auth::user()->usertype == 'store')
                        {{--@if($thestatus['storestatus'] == true)--}}

                            @include('backendtemplate.usermenu.store')
                        {{--@else--}}
                        {{--@endif--}}



                @elseif(Auth::user()->usertype == 'storebranch')

                    @include('backendtemplate.usermenu.branch')

                @else
                    @include('backendtemplate.usermenu.branch')
                @endif

            </ul>
        </div>
    </nav>
{{--</div>--}}