<footer class="footer mt-4">
    <div class="w-100 clearfix">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright &copy; 20<?php echo date("y");?> <a class="text-success" href="" target="_blank">MyPrice</a>. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Designed & Developed with <i class="ti-heart text-danger ml-1"></i>&nbsp; by 707 Digital</span>
    </div>
</footer>