@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">{{@$branch->branch_name}}</a></li>
            <li class="breadcrumb-item active" >Add Product</li>
        </ol>
    </nav>



    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <a href="{{route('storebranch.allproduct')}}" class="btn btn-primary text-white" ><i
                                        class="ti-eye"></i> view Products
                            </a>
                            <div  id="loader" style="display:none">
                                <img src="{{asset('static/images/spin.gif')}}" alt="" style="width:50px;height: 50px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <form id="editform" class="forms-sample mt-4" action="{{route('storebranch.product.add')}}" method="post">
                        {{csrf_field()}}
                        <input type="text" name="branchid" value="{{$branch->id}}" hidden>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="Full Name">Category</label>
                                    <select name="categoryid" id="categoryid" class="form-control" >
                                        <option >select category</option>
                                        @forelse($categories as $category)
                                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        @empty
                                        @endforelse
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="Full Name">Product name</label>
                                    <select name="productid" id="productid" class="form-control"  >
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="Full Name">Quantity</label>
                                    <input type="text" name="quantity" class="form-control" placeholder="quantity" id="quanity">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="Full Name">Price</label>
                                    <input type="text" name="price" class="form-control" placeholder="price" id="price">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="Full Name">Deal price</label>
                                    <input type="text" name="dealprice" class="form-control" placeholder="deal price" id="dealprice">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="Full Name">Deal End Date</label>
                                    <input type="date" name="dealexpire" class="form-control" placeholder="" id="dealexpire">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="Full Name">Product Measurement</label>
                                    <select name="measurement" class="form-control" id="measurement">
                                        <option >Select measurement</option>
                                        @foreach ($measurements as $m)
                                            <option value="{{$m->title}}">{{ucwords($m->title)}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="Full Name">Product Size</label>
                                    <input type="text" name="size" class="form-control" placeholder="size" id="size">
                                </div>
                            </div>

                        </div>


                        <button type="submit" class="btn btn-primary mr-2 mt-4 float-right" id="submitbtn">Add Product</button>
                    </form>
                </div>
            </div>


        </div>

    </div>


    <script>
        $(document).ready(function(){
            var loader = $('#loader');
            $('select[name="categoryid"]').on('change',function(){
                var category = $(this).val();
                console.log(category);
                if(category){
                    $.ajax({
                        url: 'loadproduct/' + category,
                        type: "GET",
                        beforeSent:function(){
                          loader.show();
                        },
                        success:function(data){
                            loader.hide();
                            console.log(data);
                            // data = JSON.parse(data);
                            $('select[name="productid"]').empty();
                             $.each(data, function(key,value) {
                                 $.each(value, function(p,v) {
                                     console.log(v.id);
                                     $('select[name="productid"]').append('<option value="' + v.id + '">' + v.storeproduct_name + '</option>');

                                 });





                            });
                        }
                    });
                }else{
                    loader.hide();
                }
            });
        });
    </script>
@endsection


