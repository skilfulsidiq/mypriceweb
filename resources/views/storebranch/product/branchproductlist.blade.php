@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">{{@$branch->branch_name}}</a></li>
            <li class="breadcrumb-item active" aria-current="">All Listed Products</li>
        </ol>
    </nav>



    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <a href="{{route('storebranch.product.form')}}" class="btn btn-primary text-white" ><i
                                        class="ti-pencil-alt"></i> Add Product
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="editproduct" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i>
                        <span>
                            <img id="productimg" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                        </span>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="editform" class="forms-sample mt-4" method="post">
                        {{csrf_field()}}

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Full Name">Quantity</label>
                                    <input type="text" name="quantity" class="form-control" placeholder="quantity" id="quantity">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Full Name">Price</label>
                                    <input type="text" name="price" class="form-control" placeholder="price" id="price">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Full Name">Measurement</label>
                                    <select name="measurement" class="form-control" id="measurement">
                                        <option >Select measurement</option>
                                        @foreach ($measurements as $m)
                                            <option value="{{$m->title}}">{{ucwords($m->title)}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Full Name">Product Size</label>
                                    <input type="text" name="size" class="form-control" placeholder="size" id="size">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Full Name">Deal price</label>
                                    <input type="text" name="dealprice" class="form-control" placeholder="deal price" id="dealprice">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Full Name">Deal End Date</label>
                                    <input type="date" name="dealexpire" class="form-control" placeholder="" id="dealexpire">
                                </div>
                            </div>



                        </div>

                        <button type="submit" class="btn btn-primary mr-2 mt-4 float-right" id="submitbtn">Update</button>





                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table id="order-listing" class="table">
                            <thead>
                            <tr class="bg-primary text-white">
                                <th>S/N #</th>
                                <th>Product category</th>
                                <th>Product Name</th>
                                <th>Product Image</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>DealPrice</th>
                                <th>Deal %</th>
                                <th>Deal End Date</th>
                                <th>Measurement/Size</th>
                                <th>Deal Status</th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($products))
                            @forelse($products as $pro)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{@$pro->category->category_name}}</td>
                                    <td>{{$pro->productname}}</td>
                                    <td><img src="{{$pro->productimage}}" alt="img" style="width: 50px; height: 50px;"></td>
                                    <td>{{$pro->quantity}}</td>
                                    <td>{{$pro->price}}</td>
                                    <td>{{$pro->dealprice}}</td>
                                    <td>{{number_format($pro->dealpercentage).'%'}}</td>
                                    <td>{{(new Carbon\Carbon($pro->dealexpire))->diffForHumans()}}</td>
                                    <td>{{$pro->measurement.'/'.$pro->size}}</td>
                                    <td>
                                        @if ($pro->dealexpire > $today )
                                        <label class="badge badge-success btn-inverse-success">
                                            Active
                                        </label>
                                            @else
                                            <label class="badge badge-success btn-inverse-danger">
                                                Inactive
                                            </label>
                                        @endif
                                    </td>
                                    <td>
                                        <button data-toggle="modal" data-target="#editproduct"
                                                data-categoryid="{{$pro->categoryid}}"
                                                data-storeid="{{$pro->storeid}}"
                                                data-storebranchid="{{$pro->storebranchid}}"
                                                data-storeproductid="{{$pro->storeproductid}}"
                                                data-productname="{{$pro->productname}}"
                                                data-productimage="{{$pro->productimage}}"
                                                data-quantity="{{$pro->quantity}}"
                                                data-price="{{$pro->price}}"
                                                data-dealprice="{{$pro->dealprice}}"
                                                data-dealexpire="{{$pro->dealexpire}}"
                                                data-size="{{$pro->size}}"
                                                data-measurement="{{$pro->measurement}}"
                                                data-editform="{{route('storebranch.product.update',$pro->slug)}}"
                                                class="btn-sm btn-dark btn-icon"><i
                                                    class="ti-pencil"> Edit</i>
                                        </button>

                                    </td>
                                </tr>
                                @empty
                            @endforelse
                                @endif


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>

    </div>


    <script>
        $(document).ready(function () {

            $('#editproduct').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var productname = button.data('productname');
                var productimage = button.data('productimage');
                var price = button.data('price');
                var quantity = button.data('quantity');
                var dealprice = button.data('dealprice');
                var dealexpire = button.data('dealexpire');
                var size= button.data('size');
                var measurement = button.data('measurement');
                var categoryid = button.data('categoryid');
                var formaction = button.data('editform');

                var modal = $(this);

                modal.find('.modal-header #headertitle').text('Edit ' + productname);
                modal.find('.modal-body #editform').attr('action',formaction);
                modal.find('.modal-body #price').val(price);
                modal.find('.modal-body #quantity').val(quantity);
                modal.find('.modal-body #dealprice').val(dealprice);
                modal.find('.modal-body #size').val(size);
                modal.find('.modal-body #dealexpire').val(dealexpire);
                modal.find('.modal-body #measurement').val(measurement);
                modal.find('.modal-body #productimg').attr('src',productimage);

            })
        });

    </script>
@endsection


