@extends('layouts.setupstore')
@section('content')
    <div class="row">
        <div class="offset-2 col-md-8 ">
            <div class="card">
                <div class="card-body">

                    <div class="text-center">
                        <img src="{{asset('static/images/cancel.png')}}" class="img-responsive" style="width:100px; height:100px;margin:0 auto;">
                    </div>

                    {{--<h3 class="text-center mt-4">Info</h3>--}}

                    <div class="card card-inverse-warning mt-4 mb-4">
                        <div class="card-body">
                            <p class="card-text">{{$msg}}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection