@extends('layouts.setupstore')
@section('content')
    @if($storenotready)
        <div class="row">
            <div class="offset-2 col-md-8 ">
                <div class="card">
                    <div class="card-body">

                        <div class="text-center">
                            <img src="{{asset('static/images/success.svg')}}" class="img-responsive" style="width:100px; height:100px;margin:0 auto;">
                        </div>

                        <h3 class="text-center mt-4">{{$msgtitle}}</h3>

                        <div class="card card-inverse-warning mt-4 mb-4">
                            <div class="card-body">
                                <p class="card-text">{{$statusmessage}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        {{--Store setup form--}}

        <div class="row">
            <div class="offset-2 col-md-8 ">
                <div class="card">
                    <div class="card-body">
                        <h3 class="text-center mt-2 text-primary">Set up your store</h3>

                        <div class="card card-inverse-primary mt-4 mb-4">
                            <div class="card-body">
                                <p class="card-text">
                                    Note: Your privacy is very important to us. To better
                                    serve you, the form information you enter is recorded in real
                                    time</p>
                            </div>
                        </div>

                        <form action="{{route('store.setupstore')}}" class="forms-sample" method="post"
                              enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Store Name">Store Name</label>
                                        <input type="text" name="store_name" class="form-control"
                                               placeholder="Store Name" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Store Address</label>
                                        <input type="text" class="form-control"
                                               placeholder="Store Address" name="store_address">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label>Store Logo</label>
                                        <input type="file" name="store_image" onchange="readURL(this);"
                                               class="file-upload-default">
                                        <div class="input-group">
                                            <input type="text" name="store_image"
                                                   class="form-control file-upload-info"
                                                   disabled placeholder="Upload Image">
                                            <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <img id="upz" class="mt-4" style="width:50px; height:50px;"
                                             src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-check form-check-flat form-check-primary">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    I understand and agree to the terms &amp; conditions.
                                    <i class="input-helper"></i></label>
                            </div>


                            <button type="submit" class="btn btn-primary mr-2 mt-4 float-right">Create
                                Store
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    @endif
@endsection