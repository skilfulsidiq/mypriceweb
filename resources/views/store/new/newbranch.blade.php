@extends('layouts.setupstore')
@section('content')
    <div class="row">
        <div class="offset-2 col-md-8 ">
            <div class="card">
                <div class="card-body">

                    <h3 class="text-center mt-2 text-primary">Setup Branch</h3>

                    <div class="card card-inverse-primary mt-4 mb-4">
                        <div class="card-body">
                            <p class="card-text">
                                Note: Your privacy is very important to us. To better
                                serve you, the form information you enter is recorded in real time.</p>
                        </div>
                    </div>

                    <form action="{{route('store.setupbranch')}}" class="forms-sample mt-4" method="post">
                        {{csrf_field()}}
                        {{--<input type="text" name="storeid" value="{{@$storeid}}" hidden>--}}

                        <div class="row">
                            <div class="col-md-6 offset-3">
                                <div class="form-group">
                                    <label for="Branch Name">Branch Name</label>
                                    <input type="text" name="branch_name" class="form-control" placeholder="Branch Name">
                                </div>
                            </div>


                            <div class="col-md-6 offset-3">
                                <div class="form-group">
                                    <label for="Branch Address">Branch Address</label>
                                    <input type="text" name="branch_address" class="form-control" placeholder="Branch Address">
                                </div>
                            </div>

                            {{--<div class="col-md-6 offset-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="Branch Manager">Branch Manager</label>--}}
                                    {{--<select class="form-control form-control-lg" name="userid">--}}
                                        {{--@foreach($tobeaddedusers as $user)--}}
                                            {{--<option value="{{$user->userid}}">{{ucfirst($user->store_user_name)}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select> </div>--}}
                            {{--</div>--}}
                        </div>

                        <button type="submit" class="btn btn-primary mr-2 mt-4 float-right">Create Manager</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection