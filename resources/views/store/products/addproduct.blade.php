@extends('layouts.backend')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">{{@$store->store_name}}</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed users">All Listed Users</li>
        </ol>
    </nav>


    <form action="{{route('store.product.add')}}" method="post">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-md-6 float-left">
                    @if(!empty($toBeAddedproducts))
                        <div class="row">
                            <div class="col-md-1">
                                <input id="allproduct" type="checkbox" class="btn btn-primary text-white">
                            </div>
                            <div class="col-md-3 pull-left"><h4>Check all</h4></div>
                            <div class="col-md-8"></div>
                        </div>

                    @else
                    @endif
                </div>
                <div class="col-md-6 float-right">

                    @if(!empty($toBeAddedproducts))
                        <button type="submit" class="btn btn-primary text-white pull-right" ><i
                                    class="ti-pencil-alt"></i> Save Selections
                        </button>


                        {{--<input type="submit" value="save" class="btn btn-success">--}}
                    @else
                    @endif
                </div>
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">


                        {{csrf_field()}}
                    <div class="table-responsive">
                        <table id="order-listing" class="table">
                            <thead>
                            <tr class="bg-primary text-white">
                                <th>S/N #</th>
                                <th>Product Name</th>
                                <th>Categody</th>
                                <th>Product Image</th>
                                <th>Check</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(!empty($toBeAddedproducts))

                                    @forelse($toBeAddedproducts as $t)
                                       <tr>
                                           <td>{{$loop->iteration}}</td>
                                           <td>{{$t->product_name}}<i id="icon_check" class="ti-check icon-check" style="display: none"></i></td>
                                           <td>{{$t->category->category_name}}</td>
                                           <td><div style="background-image: url('{{$t->product_image}}');width:100px;height: 100px;"></div></td>
                                           <td>
                                                   <input type="checkbox" id="singlecheck" name="createproductid[]" class="form-control" value="{{$t->id}}">
                                               </td>
                                       </tr>
                                        @empty
                                        <h3 class="text-center">No New Products</h3>
                                    @endforelse


                            @else
                                <h3 class="text-center">No New Products</h3>
                            @endif


                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
    </form>

        </div>

    </div>
    <script>
        $(document).ready(function(){
            $("#allproduct").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
                $('.icon-check').toggle();
            });
            $("#singlecheck").on('change',function(){
                $('#icon-check').show();
            })
        });
    </script>
    <style>
        .icon-check{
            margin: 0 10px;
            font-size: 20px;
            border: 5px;
            color: green;
        }
        input[type='checkbox'] {
            -webkit-appearance:none;
            width:30px;
            height:30px;
            background:white;
            border-radius:5px;
            border:2px solid #555;
        }
        input[type='checkbox']:checked {
            background: green;
            border: 5px solid green;
        }

    </style>



 @endsection


