@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">{{@$store->store_name}}</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed users">All Products</li>
        </ol>
    </nav>



    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <a class="btn btn-primary text-white" href="{{route('store.product.newform')}}"><i
                                        class="ti-pencil-alt"></i> Add Products
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addproduct" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i> Add Store Products</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('store.product.add')}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            @if(!empty($toBeAddedproducts))
                            @forelse($toBeAddedproducts as $t)
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6" >
                                        <img src="{{$t->product_image}}" alt="img" style="width:30px; height: 50px;">
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <label for="">
                                            <input type="checkbox" name="createproductid[]" class="form-control" value="{{$t->id}}">{{$t->product_name??''}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                                @empty
                                <h3 class="text-center">No New Products</h3>
                            @endforelse
                            @else
                                <h3 class="text-center">No New Products</h3>
                            @endif
                        </div>
                        @if(!empty($toBeAddedproducts))
                            <input type="submit" value="save" class="btn btn-success">
                        @else
                        @endif

                    </form>
                </div>

            </div>
        </div>
    </div>




    <div class="card">
        <div class="card-body">

                {{--<div class="row">--}}
                    {{--<div class="col-md-4"></div>--}}
                    {{--<div class="col-md-4 ">--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="">Select Category</label>--}}
                            {{--<select name="categoryid" id="categoryid" class="form-control">--}}
                                {{--<option> Select category</option>--}}
                                {{--@foreach($categories as $cat)--}}
                                    {{--<option value="{{$cat->id}}"> {{ucwords($cat->category_name)}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4"></div>--}}
                {{--</div>--}}



            <div id="categoryfilter" style="display:none">
                <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table id="listproducts" class="table">
                            <thead>
                            <tr class="bg-primary text-white">
                                <th>S/N #</th>
                                <th>Product Name</th>
                                <th>Product Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody id="tbody">
                            {{--@foreach($storeusers as $user)--}}
                                {{--<tr>--}}
                                    {{--<td>{{$loop->iteration}}</td>--}}
                                    {{--<td>{{$user->store_user_name}}</td>--}}
                                    {{--<td>{{$user->store_user_email}}</td>--}}
                                    {{--<td>{{$user->userid}}</td>--}}
                                    {{--<td>{{$user->branch_name}}</td>--}}
                                    {{--<td>--}}



                                        {{--<button data-toggle="modal" data-target="#edituser"--}}
                                                {{--data-name="{{$user->store_user_name}}"--}}
                                                {{--data-email="{{$user->store_user_email}}"--}}
                                                {{--data-storeid="{{$user->storeid}}"--}}
                                                {{--data-userid="{{$user->userid}}"--}}
                                                {{--data-storename="{{$user->store_name}}"--}}
                                                {{--data-editform="{{route('store.user.update',$user->userid)}}"--}}
                                                {{--class="btn-sm btn-dark btn-icon"><i--}}
                                                    {{--class="ti-pencil"> Edit</i>--}}
                                        {{--</button>--}}

                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            <div id="allproduct">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr class="bg-primary text-white">
                                    <th>S/N #</th>
                                    <th>Product category</th>
                                    <th>Product Name</th>
                                    <th>Product Image</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($products))
                                    @forelse($products as $pro)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{@$pro->category->category_name}}</td>
                                            <td>{{$pro->storeproduct_name}}</td>
                                            <td><div style="width: 100px; height: 100px;
background-image: url({{$pro->storeproduct_image}})"></div></td>

                                            <td>
                                                <a href="{{route('store.product.delete', $pro->slug)}}"
                                                        class="btn-sm btn-danger btn-icon"><i
                                                            class="ti-trash"> Remove</i>
                                                </a>

                                            </td>
                                        </tr>
                                    @empty
                                    @endforelse
                                @endif


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>



    <script>
        // $(document).ready(function () {
        //     var tbody = $("#listproducts #tbody");
        //     var normallist = $("#allproduct");
        //     var categoryfilterlist = $("#categoryfilter");
        //     var t;
        //     $('select[name="categoryid"]').on('change',function(){
        //
        //         var categoryid = $(this).val();
        //
        //         $.ajax({
        //             url:'storeproduct/bycategory/'+categoryid,
        //             type:"GET",
        //             beforeSend:function(){
        //                 tbody.html('');
        //             },
        //             success:function(data){
        //                 console.log(data);
        //                 $.each(data, function(key, value){
        //                     var c = key+1;
        //                     var id = value['id'];
        //                     var link = "deleteproduct/"+id;
        //                     var img = value["storeproduct_image"];
        //                     console.log(value['storeproduct_name']);
        //
        //                      t='<tr>';
        //                     t+='<td>'+c+'</td>';
        //                     t+='<td>'+value['storeproduct_name']+'</td>';
        //                     t+='<td><img src='+img+'></td>';
        //                     t+='<td><a class="btn btn-success"   href='+link +'  >disable</a></td>';
        //                     t+='</tr>';
        //
        //                     tbody.append(t);
        //
        //
        //                 });
        //             }
        //         })
        //     })
        // });

    </script>
@endsection


