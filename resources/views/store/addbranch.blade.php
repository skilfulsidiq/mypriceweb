@extends('layouts.backend')
@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="Setup Store Branch">Setup Store Branch</li>
    </ol>
</nav>


    @if ($addbranchStatus)
        <div class="row">
            <div class="offset-2 col-md-8 ">
                <div class="card">
                    <div class="card-body">

                        <h3 class="text-center mt-2 text-primary">Setup Branch</h3>

                        <div class="card card-inverse-primary mt-4 mb-4">
                            <div class="card-body">
                                <p class="card-text">
                                    Note: Your privacy is very important to us. To better
                                    serve you, the form information you enter is recorded in real time.</p>
                            </div>
                        </div>

                        <form action="{{route('store.setupbranchsecond')}}" class="forms-sample mt-4" method="post">
                            {{csrf_field()}}
                            <input type="text" name="storeid" value="{{$store->id}}" hidden>

                            <div class="row">
                                <div class="col-md-6 offset-3">
                                    <div class="form-group">
                                        <label for="Branch Name">Branch Name</label>
                                        <input type="text" name="branch_name" class="form-control" placeholder="Branch Name">
                                    </div>
                                </div>


                                <div class="col-md-6 offset-3">
                                    <div class="form-group">
                                        <label for="Branch Address">Branch Address</label>
                                        <input type="text" name="branch_address" class="form-control" placeholder="Branch Address">
                                    </div>
                                </div>

                                <div class="col-md-6 offset-3">
                                    <div class="form-group">
                                        <label for="Branch Manager">Branch Manager</label>
                                        <select class="form-control form-control-lg" name="userid">
                                            @foreach($tobeaddedusers as $user)
                                                <option value="{{$user->userid}}">{{ucfirst($user->store_user_name)}}</option>
                                            @endforeach
                                        </select> </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary mr-2 mt-4 float-right">Create Manager</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="offset-2 col-md-8 ">
                <div class="card">
                    <div class="card-body">

                        <div class="text-center">
                            <img src="{{asset('static/images/cancel.png')}}" class="img-responsive" style="width:100px; height:100px;margin:0 auto;">
                        </div>

                        {{--<h3 class="text-center mt-4">Info</h3>--}}

                        <div class="card card-inverse-warning mt-4 mb-4">
                            <div class="card-body">
                                <p class="card-text">{{$msg}}</p>
                                <a href="{{route('store.branchuserlist')}}" class="btn btn-dark btn-block mb-2">Add Store Manager</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif




@endsection
