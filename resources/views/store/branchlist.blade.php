@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">{{@$store->store_name}}</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed users">All Listed Users</li>
        </ol>
    </nav>

    </div>
    @endsection('content')