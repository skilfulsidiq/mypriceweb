@extends('layouts.backend')
@section('content')

    {{--The dashboard here--}}
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-5 mb-4 mb-xl-0">
                    <h4 class="font-weight-bold">Welcome! {{ucwords(Auth::user()->name)}}</h4>
                    <h4 class="font-weight-normal mb-0">Store Dashboard.</h4>
                </div>
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="border-right pr-4 mb-3 mb-xl-0">
                            <p class="text-muted">Total Store Managers</p>
                            <h4 class="mb-0 font-weight-bold">{{@$totalstoreusers}}</h4>
                        </div>
                        <div class="border-right pr-4 mb-3 mb-xl-0">
                            <p class="text-muted">Total Branches</p>
                            <h4 class="mb-0 font-weight-bold">{{@$totalbranch}}</h4>
                        </div>
                        <div class="border-right pr-4 mb-3 mb-xl-0">
                            <p class="text-muted">Total Products</p>
                            <h4 class="mb-0 font-weight-bold">{{@$totalproduct}}</h4>
                        </div>
                        <div class="pr-3 mb-3 mb-xl-0">
                            <p class="text-muted">Total Active Deals</p>
                            <h4 class="mb-0 font-weight-bold">{{@$totaldeals}}</h4>
                        </div>
                        <!-- <div class="mb-3 mb-xl-0">
                            <button class="btn btn-primary rounded-0 text-white">Add Product</button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="border-bottom text-center pb-4">
                                <img src="{{$store->store_image}}" alt="profile"
                                     class="img-lg rounded-circle mb-3">
                                <div class="mb-3">
                                    <h4>{{$store->store_name}}</h4>
                                    <p>
                                        <i class="ti-location-pin text-success"></i>
                                        {{$store->store_address}}
                                    </p>

                                </div>
                                <div class="d-flex justify-content-center">
                                    <a href="{{route('store.product.all')}}" class="btn btn-primary mr-1">Products</a>
                                </div>
                            </div>

                            <div class="py-4">
                                <p class="clearfix">
                                <span class="float-left">
                                    Status
                                </span>
                                    <span class="float-right text-muted">
                                    <label class="badge badge-success btn-inverse-success">
                                        @if($store->store_status == 1) Approved

                                        @else
                                            Pending
                                        @endif

                                    </label>
                                </span>
                                </p>
                                <p class="clearfix">
                                <span class="float-left">
                                    Total Products
                                </span>
                                    <span class="float-right">
                                    {{$totalproduct}}
                                </span>
                                </p>
                                <p class="clearfix">
                                <span class="float-left">
                                    Total Branches
                                </span>
                                    <span class="float-right">
                                    {{$totalbranch}}
                                </span>
                                </p>

                                <p class="mt-4">
                                    <img style="width:335px"
                                         src="https://blogs.brown.edu/amst-0191z-s01-spring-2016/files/2016/05/mcdonalds-fast-food-restaurant-the-thing-600-20635.jpg"/>
                                </p>
                            </div>

                        </div>

                        <div class="col-lg-8">
                            <div class="profile-feed">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div style="margin-bottom: 10px;">
                                                    <button data-toggle="modal" data-target="#addbranch"
                                                            class="btn-sm btn-outline-success btn-icon">
                                                        <i class="ti-plus"></i> Add new branch
                                                    </button>
                                                </div>
                                                <div class="table-responsive">
                                                    <table id="order-listing" class="table">
                                                        <thead>
                                                        <tr class="bg-success text-white">
                                                            <th>S/N #</th>
                                                            <th>Branch Name</th>
                                                            <th>Branch Address</th>
                                                            <th>Date Created</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($readystorebranches as $branch)
                                                            <tr>
                                                                <td>{{$loop->iteration}}</td>
                                                                <td>{{$branch->branch_name}}</td>
                                                                <td>{{$branch->branch_address}}</td>

                                                                <td> {{(new
                                                                Carbon\Carbon($branch->created_at))->diffForHumans()}}</td>
                                                                <td>
                                                                    <button onclick="location.href='{{route('store.branchdetail',$branch->slug)}}'"
                                                                            class="btn-sm btn-outline-warning btn-icon">
                                                                        <i class="ti-eye"></i> View
                                                                    </button>
                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="addbranch" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i>Add Store Branch</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('store.setupbranch')}}" class="forms-sample mt-4" method="post">
                        {{csrf_field()}}
                        {{--<input type="text" name="storeid" value="{{@$storeid}}" hidden>--}}

                        <div class="row">
                            <div class="col-md-6 offset-3">
                                <div class="form-group">
                                    <label for="Branch Name">Branch Name</label>
                                    <input type="text" name="branch_name" class="form-control" placeholder="Branch Name">
                                </div>
                            </div>


                            <div class="col-md-6 offset-3">
                                <div class="form-group">
                                    <label for="Branch Address">Branch Address</label>
                                    <input type="text" name="branch_address" class="form-control" placeholder="Branch Address">
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-warning mr-2 mt-4 text-white float-right">Add Branch</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
