@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">{{@$store->store_name}}</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed users">All Listed Users</li>
        </ol>
    </nav>



    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <button class="btn btn-primary text-white" data-toggle="modal" data-target="#createuser"><i
                                        class="ti-pencil-alt"></i> Create Store User
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createuser" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i> Add Store User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('store.addadminsecond')}}" class="forms-sample mt-4" method="post">
                        {{csrf_field()}}
                        <input type="text" name="storeid" value="{{$store->id}}" hidden>
                        <input type="text" name="store_name" value="{{$store->store_name}}" hidden>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Full Name">Full Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Full Name">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Email Address">Email Address</label>
                                    <input type="text" name="email" class="form-control" placeholder="Email Address">
                                </div>
                            </div>
                        </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Password">Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password ">
                                </div>
                            </div>
                        </div>



                        <button type="submit" class="btn btn-primary mr-2 mt-4 float-right">Add Store Manager</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="edituser" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="editform" class="forms-sample mt-4" method="post">
                        {{csrf_field()}}
                        <input type="text" name="storeid" id="storeid" hidden>
                        <input type="text" name="store_name" id="storename" hidden>
                        <input type="text" name="userid" id="userid" hidden>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Full Name">Full Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Full Name" id="editname">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Email Address">Email Address</label>
                                    <input type="text" name="email" class="form-control" placeholder="Email Address" id="editemail">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Password">Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password ">
                                </div>
                            </div>
                        </div>



                        <button type="submit" class="btn btn-primary mr-2 mt-4 float-right" id="submitbtn"></button>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table id="order-listing" class="table">
                            <thead>
                            <tr class="bg-primary text-white">
                                <th>S/N #</th>
                                <th>Full Name</th>
                                <th>Email Address</th>
                                <th>Store Branch</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($storeusers as $user)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$user->store_user_name}}</td>
                                    <td>{{$user->store_user_email}}</td>
                                    <td>{{$user->branch_name}}</td>
                                  <td>


                                
                                    <button data-toggle="modal" data-target="#edituser"
                                            data-name="{{$user->store_user_name}}"
                                            data-email="{{$user->store_user_email}}"
                                            data-storeid="{{$user->storeid}}"
                                            data-userid="{{$user->userid}}"
                                            data-storename="{{$user->store_name}}"
                                            data-editform="{{route('store.user.update',$user->slug)}}"
                                            class="btn-sm btn-dark btn-icon"><i
                                                class="ti-pencil"> Edit</i>
                                    </button>
                                      <a href="{{route('store.user.delete',$user->slug)}}"class="btn-sm btn-danger btn-outline-danger btn-icon">
                                          <i class="ti-trash">delete</i>

                                      </a>
                                      <a href="{{route('store.user.detachedUser',$user->slug)}}"class="btn-sm btn-danger btn-outline-danger btn-icon">
                                          <i class="ti-unlink">Unassained</i>

                                      </a>

                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>

    </div>



    <script>
        $(document).ready(function () {

            $('#edituser').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var userid = button.data('userid');
                var storeid = button.data('storeid');
                var storename = button.data('storename');
                var name = button.data('name');
                var email = button.data('email');
                var formaction = button.data('editform');
                var modal = $(this);
                modal.find('.modal-header #headertitle').text('Edit ' + name);
                modal.find('.modal-body #editform').attr('action',formaction);
                modal.find('.modal-body #editname').val(name);
                modal.find('.modal-body #editemail').val(email);
                modal.find('.modal-body #userid').val(userid);
                modal.find('.modal-body #storeid').val(storeid);
                modal.find('.modal-body #storename').val(storename);
                modal.find('.modal-body #submitbtn').text('Update '+ name);

            })
        });

    </script>
@endsection


