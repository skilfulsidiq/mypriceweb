@extends('layouts.backend')
@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="Setup Store Manager">Setup Store Manager</li>
    </ol>
</nav>

<div class="row">
    <div class="offset-2 col-md-8 ">
        <div class="card">
            <div class="card-body">

                <h3 class="text-center mt-2 text-primary">Add Manager</h3>

                <div class="card card-inverse-primary mt-4 mb-4">
                    <div class="card-body">
                        <p class="card-text">
                            Note: Your privacy is very important to us. To better
                            serve you, the form information you enter is recorded in real time.</p>
                    </div>
                </div>

                <form action="{{route('store.addadminsecond')}}" class="forms-sample mt-4" method="post">
                    {{csrf_field()}}
                    <input type="text" name="storeid" value="{{$store->id}}" hidden>
                    <input type="text" name="store_name" value="{{$store->store_name}}" hidden>

                    <div class="row">
                        <div class="col-md-6 offset-3">
                            <div class="form-group">
                                <label for="Full Name">Full Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Full Name">
                            </div>
                        </div>


                        <div class="col-md-6 offset-3">
                            <div class="form-group">
                                <label for="Email Address">Email Address</label>
                                <input type="text" name="email" class="form-control" placeholder="Email Address">
                            </div>
                        </div>

                        <div class="col-md-6 offset-3">
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Password ">
                            </div>
                        </div>
                    </div>



                    <button type="submit" class="btn btn-primary mr-2 mt-4 float-right">Create Manager</button>
                </form>

            </div>
        </div>
    </div>
</div>



@endsection
