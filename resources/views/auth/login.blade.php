@extends('layouts.frontend')

@section('content')
    <h4>Welcome back!</h4>
    <h6 class="font-weight-light">Happy to see you again! </h6>
    <form class="pt-3" method="POST" action="{{ route('login') }}" data-parsley-validate>
        {{csrf_field()}}
        <div class="form-group">
            <label for="exampleInputEmail">Email Address</label>
            <div class="input-group {{ $errors->has('email')?'has-error':''}}">
                <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="ti-email text-success"></i>
                      </span>
                </div>
                <input type="text"  name="email" class="form-control  border-left-0"
                       placeholder="Email Address">                                 
            </div>
            @if ($errors->has('email'))
                <span class="help-block"
                style="color:#f00; font-size: 10px; ;letter-spacing: 1px;">
                <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif    
        </div>
        <!-- Email Address -->

        <div class="form-group">
            <label for="exampleInputPassword">Password</label>
            <div class="input-group {{ $errors->has('password')?'has-error':''}}">
                <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="ti-lock text-success"></i>
                      </span>
                </div>
                <input type="password" name="password" class="form-control border-left-0" placeholder="Password">
            </div>
                @if ($errors->has('password'))
                <span class="help-block"
                style="color:#f00; font-size: 10px; ;letter-spacing: 1px;">
                <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
        </div>
        <!-- Password -->

        <div class="my-2 d-flex justify-content-between align-items-center">
            <div class="form-check">
                <label class="form-check-label text-muted">
                    <input type="checkbox" class="form-check-input" {{ old('remember') ? 'checked' : '' }}>
                    Keep me signed in
                    <i class="input-helper"></i>
                </label>
            </div>
            <a href="{{ route('password.request') }}" class="auth-link text-black">Forgot password?</a>
        </div>
        <div class="my-3">
        <button type="submit" class="btn btn-block btn-inverse-success btn-lg font-weight-medium btn-success">
          <i class="ti-file btn-icon-prepend"></i> Submit
                        </button>
            <!-- <a class="btn btn-block btn-primary btn-lg font-weight-medium btn-success">LOGIN</a> -->
        </div>

        <div class="text-center mt-4 font-weight-light">
            Don't have an account yet? <a href="{{route('register')}}" class="text-success">Sign Up</a>
        </div>
    </form>
@endsection
