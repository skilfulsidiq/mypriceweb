@extends('layouts.frontend')

@section('content')
    <h4>New here?</h4>
    <h6 class="font-weight-light">Get Started With A Free Account! It takes only few steps.</h6>
    <form class="pt-3" data-parsley-validate method="POST" action="{{ route('register') }}">
        {{csrf_field()}}
        <div class="form-group">
            <label>Full Name</label>
            <div class="input-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="ti-user text-success"></i>
                      </span>
                </div>
                <input type="text" name="name" class="form-control form-control-lg border-left-0" placeholder="Full Name">
            </div>
                    @if ($errors->has('name'))
                    <span class="help-block" style="color:#f00; font-size: 10px; ;letter-spacing: 1px;">
                    <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
        </div>
        <!-- Full Name -->

        <div class="form-group">
            <label>Email Address</label>
            <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="ti-email text-success"></i>
                      </span>
                </div>
                <input type="email" class="form-control form-control-lg border-left-0" placeholder="Email Address" name="email">
            </div>
                    @if ($errors->has('email'))
                    <span class="help-block" style="color:#f00; font-size: 10px; ;letter-spacing: 1px;">
                    <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
        </div>
        <!-- Email Address -->

        <div class="form-group">
            <label>Password</label>
            <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="ti-lock text-success"></i>
                      </span>
                </div>
                <input type="password" class="form-control form-control-lg border-left-0" placeholder="Password" name="password">
            </div>
                @if ($errors->has('password'))
                <span class="help-block" style="color:#f00; font-size: 10px; ;letter-spacing: 1px;">
                <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
        </div>
         <!-- Password -->

        <div class="form-group">
            <label>Confirm Password</label>
            <div class="input-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="ti-lock text-success"></i>
                      </span>
                </div>
                <input type="password" class="form-control form-control-lg border-left-0" placeholder="Password" name="password_confirmation">
            </div>
            @if ($errors->has('password_confirmation'))
                <span class="help-block" style="color:#f00; font-size: 10px; ;letter-spacing: 1px;">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                @endif
        </div>
         <!-- Confrim Password -->



        <div class="mb-4">
            <div class="form-check">
                <label class="form-check-label text-muted">
                    <input type="checkbox" class="form-check-input">
                    I agree to all Terms & Conditions
                    <i class="input-helper"></i>
                </label>
            </div>
        </div>
        <div class="mt-3">
        <button type="submit" class="btn btn-block btn-inverse-success btn-lg font-weight-medium btn-success">
          <i class="ti-file btn-icon-prepend"></i> Sign Up
                        </button>

            <!-- <input type="submit" class="btn btn-block  btn-inverse-success btn-primary btn-lg font-weight-medium auth-form-btn btn-success" value="SIGN UP"> -->
        </div>
        <div class="text-center mt-4 font-weight-light">
            Already have an account? <a href="{{route('login')}}" class="text-success">Login</a>
        </div>
    </form>
@endsection
