@extends('layouts.backend')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="All Listed Permissions">All Listed Permissions</li>
    </ol>
</nav>



<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="row">
            <div class="col-12 col-xl-7">
                <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <div class="mb-3 mb-xl-0">
                        <button class="btn btn-warning text-white" data-toggle="modal" data-target="#createpermission"><i
                                class="ti-lock"></i> Add Permission</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createpermission" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-white"><i class="ti-lock"></i> Add Permission</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('admin.permission.add')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="Store Name">New Permission</label>
                        <input type="text" class="form-control" name="name" placeholder="New Permission">
                    </div>

                    <div class="form-group">
                        <label>Assign Roles</label>
                        <select class="form-control" multiple="multiple" name="roles[]">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{ucfirst($role->name)}}</option>
                                @endforeach

                        </select>
                    </div>

                    <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="editpermission" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-white" id="headertitle"><i class="ti-lock"></i></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="editform">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="Store Name">New Permission</label>
                        <input type="text" id="name" class="form-control" name="name" placeholder="New Permission">
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label>Assign Roles</label>--}}
                        {{--<select class="form-control" multiple="multiple" name="roles[]" id="role">--}}
                            {{--@foreach($roles as $role)--}}
                                {{--<option value="{{$role->id}}">{{ucfirst($role->name)}}</option>--}}
                            {{--@endforeach--}}

                        {{--</select>--}}
                    {{--</div>--}}

                    <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="order-listing" class="table">
                        <thead>
                            <tr class="bg-success text-white">
                                <th>S/N #</th>
                                <th>Permission</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $per)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$per->name}}</td>
                                <td>{{$per->created_at}}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#editpermission"
                                            data-permission="{{$per->name}}"
                                            data-formaction="{{route('admin.permission.update',$per->id)}}"

                                            class="btn-sm btn-outline-warning btn-icon">
                                        <i class="ti-pencil"></i> Edit</button>
                                    <a href="{{route('admin.permission.delete',$per->id)}}" class="btn-sm btn-outline-danger btn-icon"> <i class="ti-trash"></i> Delete</a>
                                </td>
                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('#editpermission').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) ;// Button that triggered the modal
            var permission = button.data('permission');
            var formaction = button.data('formaction');
            var modal = $(this);
            modal.find('.modal-header #headertitle').text('Edit ' + permission);
            modal.find('.modal-body #editform').attr('action',formaction);
            modal.find('.modal-body #name').val(permission);

        })
    });
</script>
    @endsection
