<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyPrice') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('static/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('static/vendors/css/vendor.bundle.addons.css')}}">
    <link rel="stylesheet" href="{{asset('static/vendors/iconfonts/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('static/vendors/css/vendor.bundle.base.css')}}">
    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('static/images/favicon.png')}}" />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>
<body>

    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
                <div class="row flex-grow">
                    <div class="col-lg-6 d-flex align-items-center justify-content-center">
                        <div class="auth-form-transparent text-left p-3">
                            <div class="brand-logo">
                                <img src="{{asset('static/images/price_logo.png')}}" alt="MyPrice Logo">
                            </div>
                            @yield('content')

                        </div>
                    </div>

                    <div class="col-lg-6 login-half-bg d-flex flex-row">
                        <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy;20<?php echo date("y");?> MyPrice. All rights reserved.<br>

                       <!-- Powered by -->

                        <span style="font-size:10px; line-spacing:1px;">Designed &amp; Developed by
                        <a target="_blank" class="text-success" href="https://www.707digital.com">707 Digital</a></span>
                        </p>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>



    <script src="{{ asset('js/parsley.min.js') }}" defer></script>
    <script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('vendors/js/vendor.bundle.addons.js')}}"></script>
    <script src="{{asset('js/off-canvas.js')}}"></script>
    <script src="{{asset('js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('js/template.js')}}"></script>
    <script src="{{asset('js/settings.js')}}"></script>
    <script src="{{asset('js/todolist.js')}}"></script>
</body>
</html>