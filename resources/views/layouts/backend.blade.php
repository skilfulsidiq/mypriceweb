@include('backendtemplate/head')
<body>
<div class="container-scroller">
    <div class="horizontal-menu">
        @include('backendtemplate.header')
        @include('backendtemplate.navbar')
    </div>
    <div class="container-fluid page-body-wrapper">
        <div class="main-panel">
        <div class="content-wrapper">
            @include('alert.error')
            @yield('content')
            @include('backendtemplate.footer')
        </div>
    </div>
  </div>
</div>





<script src="{{asset('js/toastr.min.js')}}"></script>
<script src="{{asset('static/vendors/js/vendor.bundle.addons.js')}}"></script>




<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/js/select2.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/js/select2.min.js'></script>

<script src="{{asset('static/js/off-canvas.js')}}"></script>
<script src="{{asset('static/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('static/js/template.js')}}"></script>
<script src="{{asset('static/js/settings.js')}}"></script>

<script src="{{asset('static/js/dashboard.js')}}"></script>
<script src="{{asset('static/js/todolist.js')}}"></script>
<script src="{{asset('static/js/data-table.js')}}"></script>

<script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>






<script>
var quill = new Quill('#productdescription, #editstoreaddress, #storeaddress', {
theme: 'snow'
});

$('[data-toggle="datepicker"]').datepicker();
</script>






<!-- End custom js for this page-->
@include('alert.alert')

<script type="text/javascript">
  $('.js-example-basic-multiple').select2();

(function($) {
  'use strict';
  $(function() {
    $('.file-upload-browse').on('click', function() {
      var file = $(this).parent().parent().parent().find('.file-upload-default');
      file.trigger('click');
    });
    $('.file-upload-default').on('change', function() {
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  });
})(jQuery);

  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#upz')
                  .attr('src', e.target.result)
                  .width(60)
                  .height(60);
          };

          reader.readAsDataURL(input.files[0]);
      }
  }
  function readURLTwo(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#upztwo')
                  .attr('src', e.target.result)
                  .width(60)
                  .height(60);
          };

          reader.readAsDataURL(input.files[0]);
      }
  }


</script>


</body>

</html>
