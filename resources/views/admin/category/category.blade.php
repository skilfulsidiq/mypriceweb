@extends('layouts.backend')
@section('content')



    {{--create category Modal--}}
    <div class="modal fade" id="createcategory" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i> Create Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('admin.category.add')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="Store Name">Category Name</label>
                            <input type="text" class="form-control" name="category_name" placeholder="Store Name">
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label>File upload</label>
                                    <input type="file" name="category_image" onchange="readURL(this);"
                                           class="file-upload-default">
                                    <div class="input-group">
                                        <input type="text" name="category_image" class="form-control file-upload-info"
                                               disabled placeholder="Upload Image">
                                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <img id="upz" class="mt-4" style="width:50px; height:50px;"
                                         src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png"/>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
{{--Edit category Modal--}}
    <div class="modal fade" id="editcategory" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="edittitle"><i class="ti-pencil-alt"></i> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="editform" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="Store Name">Category Name</label>
                            <input type="text" id="editcategoryname" class="form-control" name="category_name" placeholder="Store Name">
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label>File upload</label>
                                    <input type="file" name="category_image"  onchange="readURL(this);"
                                           class="file-upload-default">
                                    <div class="input-group">
                                        <input type="text" name="category_image" class="form-control file-upload-info"
                                               disabled placeholder="Upload Image">
                                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-warning" type="button">Upload</button>
                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <img id="upz" class="mt-4" style="width:50px; height:50px;"
                                         src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png"/>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-dark mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){

            $('#editcategory').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) ;// Button that triggered the modal
                var categoryslug = button.data('categoryslug');
                var formaction = button.data('editform');
                var catname = button.data('catname');
                var modal = $(this);
                modal.find('.modal-header #edittitle').text('Edit ' + catname);
                modal.find('.modal-body #editform').attr('action',formaction);
                modal.find('.modal-body #editcategoryname').val(storename);
                modal.find('.modal-body #upz').attr('src', catimage);
                // modal.find('.modal-body #editcatimage').val(catimage);
                // modal.find('.modal-body #editcatimage2').val(catimage);

            })
        });
    </script>
@endsection




{{--<form action="{{route('category.store')}}" method="post" enctype="multipart/form-data">--}}
    {{--{{csrf_field()}}--}}
    {{--<input type="text" name="name">--}}
    {{--<input type="file" name="photo">--}}
    {{--<input type="submit" value="ago" class="btn btn-success">--}}
{{--</form>--}}