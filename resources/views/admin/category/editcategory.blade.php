@extends('layouts.backend')

@section('content')

      <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="All Listed Categories"> All Listed Categories</li>
            </ol>
          </nav>

          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="row">
                <div class="col-12 col-xl-7">
                  <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <div class="mb-3 mb-xl-0">
                      <button class="btn btn-info text-white" data-toggle="modal" data-target="#exampleModal-4"><i class="ti-pencil-alt"></i> Create Category</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                <h4 class="text-white"><i class="ti-pencil-alt"></i> Create New Category</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>



          <div class="modal-body">
              
          <form>
            <div class="form-group">
              <label for="Store Name">Category Name</label>
              <input type="text" class="form-control" placeholder="Store Name">
            </div>

              <div class="form-group">
              <div class="row">
              <div class="col-sm-8">
              <label>Category Logo</label>
              <input type="file" name="img" onchange="readURL(this);" class="file-upload-default">
              <div class="input-group">
                <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                <span class="input-group-append">
                  <button class="file-upload-browse btn btn-warning text-white" type="button">Upload</button>
                </span>
                </div>
              </div>

                <div class="col-sm-4">
                  <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png"/> 
                    </div>
                  </div>
                </div>

              <button type="submit" class="btn btn-success text-white mr-2 mt-4 float-right">Submit</button>                
                  </form>
                </div>
                
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <!-- <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12">                      
                    <div class="profile-feed">

               <div class="card">
                <div class="card-body">
                  <h4>Edit Branch Information</h4>
                  <p class="card-description">
                   edit branch information
                  </p>
                  <form class="form-inline">
                  
                    <label class="sr-only" for="Branch Name">Branch Name</label>
                    <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Branch Name">
                  
                    <label class="sr-only" for="Address">Branch Address</label>
                    <div class="input-group mb-2 mr-sm-2">
                      <input type="text" class="form-control" placeholder="Branch Address">
                    </div>

                    <button type="submit" class="btn btn-info mb-2 text-white">Update</button>
                  </form>
                </div>
              </div> -->

                        <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr class="bg-info text-white">
                            <th>S/N #</th>
                            <th>Category Name</th>
                            <th>Category Logo</th>
                            <th>Date Created</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>1</td>
                            <td>Shoprite</td>
                            <td>Ikeja, Lagos</td>
                            <td>2012/08/03</td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            <button class="btn-sm btn-outline-danger btn-icon"> <i class="ti-trash"></i> Delete</button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Konga</td>
                            <td>Victoria Island, Lagos</td>
                            <td>2015/04/01</td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            <button class="btn-sm btn-outline-danger btn-icon"> <i class="ti-trash"></i> Delete</button>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Country gorceries</td>
                            <td>oshodi, Lagos</td>
                            <td>2010/11/21</td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            <button class="btn-sm btn-outline-danger btn-icon"> <i class="ti-trash"></i> Delete</button>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Reed Supermarket</td>
                            <td>lagos-island, Lagos</td>
                            <td>2016/01/12</td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            <button class="btn-sm btn-outline-danger btn-icon"> <i class="ti-trash"></i> Delete</button>
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Ebeanor Supermarket</td>
                            <td>Lekki, Lagos</td>
                            <td>2017/12/28</td>
                            <td>
                              <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                              <button class="btn-sm btn-outline-danger btn-icon"> <i class="ti-trash"></i> Delete</button>
                            </td>                        
                          </tr>
                        <tr>
                            <td>6</td>
                            <td>Hubmart</td>
                            <td>Lekki, Lagos</td>
                            <td>2000/10/30</td>
                            <td>
                              <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                              <button class="btn-sm btn-outline-danger btn-icon"> <i class="ti-trash"></i> Delete</button>
                            </td>                        
                          </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


        </div>
                       
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        




              
@endsection