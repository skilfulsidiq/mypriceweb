@extends('layouts.backend')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="All Listed Categories"> All Listed Categories</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="row">
            <div class="col-12 col-xl-7">
                <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <div class="mb-3 mb-xl-0">
                        <button class="btn btn-warning text-white" data-toggle="modal" data-target="#createcategory"><i
                                class="ti-pencil-alt"></i> Create Category
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createcategory" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-white"><i class="ti-pencil-alt"></i> Create Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="{{route('admin.category.add')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="Store Name">Category Name</label>
                        <input type="text" class="form-control" name="category_name" placeholder="Store Name">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-8">
                                <label>File upload</label>
                                <input type="file" name="category_image" onchange="readURL(this);" class="file-upload-default">
                                <div class="input-group">
                                    <input type="text" name="category_image" class="form-control file-upload-info"
                                        disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse text-white btn btn-warning" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="editcategory" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-white" id="edittitle"><i class="ti-pencil-alt"></i></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="editform" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="Store Name">Category Name</label>
                        <input type="text" id="editcategoryname" class="form-control" name="category_name" placeholder="Store Name">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-8">
                                <label>File upload</label>
                                <input type="file" name="category_image" onchange="readURL(this);" class="file-upload-default">
                                <div class="input-group">
                                    <input type="text" name="category_image" class="form-control file-upload-info"
                                        disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse text-white btn btn-warning" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">


        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                    <tr class="bg-success text-white">
                                        <th>S/N #</th>
                                        <th>Category Name</th>
                                        <th>Category Logo</th>
                                        <th>Date Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $category)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$category->category_name}}</td>
                                        <td><img src="{{$category->category_image}}" alt="" style="width:50px; height: 50px;"></td>
                                        <td>{{(new Carbon\Carbon($category->created_at))->diffForHumans()}}</td>
                                        <td>
                                            <button data-toggle="modal" data-target="#editcategory" data-catname="{{$category->category_name}}"
                                                data-catimage="{{$category->category_image}}" data-cataction="{{route('admin.category.update',$category->slug)}}"
                                                class="btn-sm btn-outline-warning btn-icon"><i class="ti-pencil"></i>
                                                Edit
                                            </button>

                                            <button onclick="location.href='{{route('admin.category.delete',$category->slug)}}'" class="btn-sm btn-outline-danger btn-icon"><i class="ti-trash"></i>
                                            Delete
                                            </button>

                                            <!-- <a href="{{route('admin.category.delete',$category->slug)}}" class="button btn-sm btn-outline-danger btn-icon link">
                                                <i class="ti-trash"></i>
                                                Delete</a> -->
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>

</div>


<script>
    $(document).ready(function () {
        $('#editcategory').on('show.bs.modal', function (event) {
            var btn = $(event.relatedTarget);
            var name = btn.data('catname');
            var image = btn.data('catimage');
            var actionform = btn.data('cataction');
            var modal = $(this);

            modal.find('.modal-header #edittitle').text('Edit ' + name);
            modal.find('.modal-body #editform').attr('action', actionform);
            modal.find('.modal-body #editcategoryname').val(name);
            modal.find('.modal-body #upz').attr('src', image)
        })
    })

</script>


@endsection
