@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">All Listed Products</a></li>
            <li class="breadcrumb-item active" aria-current="All Requested Stores">All Requested Products</li>
        </ol>
    </nav>


    <div class="modal fade" id="requestmodal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                        
                    <div class="col-lg-12">
                      <div class="border-bottom text-center pb-4">
                        <img id="img" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" alt="profile" class="img-lg mb-3">
                        <div class="mb-3">
                          <h4 id="name">Product Name</h4>
                        </div>
                        <div class="py-4">
                        <p class="clearfix">
                          <span class="float-left">
                            Request Status
                          </span>
                          <span class="float-right ">
                            Pending
                          </span>
                        </p>
                        <p class="clearfix">
                          <span class="float-left">
                            Product Category

                          </span>
                          <span id="category" class="float-right ">

                          </span>
                        </p>
                        <p class="clearfix">
                          <span class="float-left">
                            Description
                          </span>
                          <span id="desc" class="float-right ">

                          </span>
                        </p>
                        {{--<p class="clearfix">--}}
                          {{--<span class="float-left">--}}
                            {{--Product Image--}}
                          {{--</span>--}}
                          {{--<span class="float-right ">--}}
                           {{----}}
                          {{--</span>--}}
                        {{--</p>--}}
                      </div>
                       
                        <div class="d-flex justify-content-center">
                          <a id="action" class="btn btn-success mr-1">Approve</a>
                          <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                      </div>

                    </div>


                  </div>
                </div>
              </div>
            </div>
          </div>


                </div>

            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">

                    <table id="order-listing" class="table">
                      <thead>
                        <tr class="bg-success text-white">
                        <th>S/N #</th>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Product Image</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($products as $pro)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$pro->product_name}}</td>
                            <td>{{@$pro->category->category_name}}</td>
                            <td><img src="{{$pro->product_image}}" alt=""></td>
                            <td>{{(new Carbon\Carbon($pro->created_at))->diffForHumans()}}</td>
                            <td>
                              <label class="badge badge-success btn-inverse-success">Pending</label>
                            </td>
                            <td>
                            <button data-toggle="modal" data-target="#requestmodal"
                                        data-product="{{$pro->product_name}}"
                                        data-category="{{$pro->category->category_name}}"
                                    data-categoryid="{{$pro->categoryid}}"
                                        data-img="{{$pro->product_image}}"
                                    data-desc="{{$pro->product_description}}"
                                    data-formaction="{{route('admin.product.approve',$pro->slug)}}"
                                    class="btn-sm btn-outline-warning btn-icon"> <i class="ti-eye"></i> View</button>
                            </td>
                        </tr>
                          @endforeach

                        
                      </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<<<<<<< HEAD
</div>
=======
  </div>
>>>>>>> 843fb52ccb8cd98f90a64cd90c88d7c7cf4fd604

    <script>
        $(document).ready(function () {
            $('#requestmodal').on('show.bs.modal',function(e){
                var btn = $(e.relatedTarget);
                var name = btn.data('product');
                var formaction = btn.data('formaction')
                var categoryid = btn.data('categoryid');
                var category = btn.data('category');
                var desc = btn.data('desc');
                var img = btn.data('img');

                var modal = $(this);

                modal.find('.modal-header').text(name);
                modal.find('.modal-body #action').attr('href',formaction);
                modal.find('.modal-body #name').val(name);
                modal.find('.modal-body #desc').text(desc);
                modal.find('.modal-body #category').text(category);
                modal.find('.modal-body #img').attr('src',img);
            })

        })
    </script>
@endsection