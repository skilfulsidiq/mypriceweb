@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed Stores">All Listed Products</li>
        </ol>
    </nav>



    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <button class="btn btn-warning text-white" data-toggle="modal" data-target="#createproduct"><i
                                        class="ti-pencil-alt"></i> Create Product
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createproduct" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i> Create New Product</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="" id="identifier" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="Product Category">Product Category</label>
                                  <select name="categoryid" id="categoryid" class="form-control">
                                      <option >Select Category</option>
                                      @foreach ($categories as $cat)
                                      <option value="{{$cat->id}}">{{$cat->category_name}}</option>

                                      @endforeach
                                  </select>

                        </div>
                      </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Product Name">Product Name</label>
                                    <input type="text" class="form-control" name="product_name" placeholder="Product Name">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Product Name">Product Description</label>
                            <textarea name="product_description" id="productdescription" class="form-control" cols="20" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label for="Product Image">Product Image</label>
                                    <input type="file" name="product_image" onchange="readURL(this);"
                                           class="file-upload-default">
                                    <div class="input-group">
                                        <input type="text" name="product_image" class="form-control file-upload-info"
                                               disabled placeholder="Upload Image">
                                        <span class="input-group-append">
                          <button class="file-upload-browse btn text-white btn-warning" type="button">Upload</button>
                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <img id="upz" class="mt-4" style="width:50px; height:50px;"
                                         src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png"/>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="editproduct" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="editform" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Product Category">Product Category</label>
                                    <select name="categoryid" id="categoryid" class="form-control">
                                        <option >Select Category</option>
                                        @foreach ($categories as $cat)
                                            <option value="{{$cat->id}}">{{$cat->category_name}}</option>

                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Product Name">Product Name</label>
                                    <input type="text" class="form-control" name="product_name" placeholder="Product Name" id="productname">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="productdescription">Product Description</label>

                            <textarea name="product_description" id="productdescription" class="form-control" cols="20" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label for="Product Image">Product Image</label>
                                    <input type="file" name="product_image" onchange="readURL(this);"
                                           class="file-upload-default">
                                    <div class="input-group">
                                        <input type="text" name="product_image" class="form-control file-upload-info"
                                               disabled placeholder="Upload Image">
                                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <img id="upz" class="mt-4" style="width:50px; height:50px;"
                                         src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png"/>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success mr-2 mt-4 float-right"> <i class="ti-save"></i> Update</button>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">

                        <table id="order-listing" class="table">
                      <thead>
                        <tr class="bg-success text-white">
                        <th>S/N #</th>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Product Image</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>

                        @foreach ($products as $pro)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$pro->product_name}}</td>
                                <td>{{@$pro->category->category_name}}</td>
                                <td><img src="{{$pro->product_image}}" alt="" style="width:50px;height: 50px;"></td>
                                <td>{{(new Carbon\Carbon($pro->created_at))->diffForHumans()}}</td>
                                <td>
                                    <label class="badge badge-success btn-inverse-success">Approved</label>
                                </td>
                                <td>
                                    <button data-toggle="modal" data-target="#editproduct"
                                            data-productname="{{$pro->product_name}}"
                                            data-category="{{$pro->categoryid}}"
                                            data-description ="{{$pro->product_description}}"
                                            data-img="{{$pro->product_image}}"
                                            data-formaction="{{route('admin.product.update',$pro->slug)}}"
                                            class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>

                                            <button onclick="location.href='{{route('admin.product.delete',$pro->slug)}}'" class="btn-sm btn-outline-danger btn-icon"><i class="ti-trash"></i>
                                            Delete
                                            </button>


                                    <!-- <a href="{{route('admin.product.delete',$pro->slug)}}" class="btn-sm btn-outline-danger btn-icon"> <i class="ti-trash"></i> Delete</a> -->
                                </td>
                            </tr>

                        @endforeach


                        

                      </tbody>
                    </table>
                    </div>
                </div>
            </div>


        </div>


    </div>
</div>
    <script>
        $(document).ready(function () {
            $('#editproduct').on('show.bs.modal',function(e){
                var btn = $(e.relatedTarget);
                var name = btn.data('productname');
                var formaction = btn.data('formaction')
                var categoryid = btn.data('category');
                var desc = btn.data('description');
                var img = btn.data('img');

                var modal = $(this);

                modal.find('.modal-header #headertitle').text('Edit '+name);
                modal.find('.modal-body #editform').attr('action',formaction);
                modal.find('.modal-body #productname').val(name);
                modal.find('.modal-body #productdescription').text(desc);
                modal.find('.modal-body #categoryid').val(categoryid);
                modal.find('.modal-body #upz').attr('src',img);
            })

        })
    </script>
@endsection