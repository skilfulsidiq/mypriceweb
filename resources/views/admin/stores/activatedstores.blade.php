@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
            <li class="breadcrumb-item"><a class="flip" href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">All Listed Stores</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed Stores"> Store Managers</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <button class="btn btn-warning text-white" data-toggle="modal" data-target="#createstore"><i
                                        class="ti-pencil-alt"></i> Create Store
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createstore" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i> Create Store</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('admin.store.add')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="Store Name">Store Name</label>
                            <input type="text" class="form-control" name="store_name" placeholder="Store Name">
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label for="Store Address">Store Address</label>--}}
                            {{--<textarea class="form-control" id="storeaddress" name="store_address" placeholder="Store Address" cols="20" rows="3"></textarea>--}}
                        {{--</div>--}}

                        <!-- <div class="form-group">
                            <label for="Store Address">Store Address</label>
                            <input type="text" class="form-control" name="store_address" placeholder="Store Address">
                        </div> -->

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label>File upload</label>
                                    <input type="file" name="store_image" onchange="readURL(this);" class="file-upload-default">
                                    <div class="input-group">
                                        <input type="text" name="store_image" class="form-control file-upload-info"
                                               disabled placeholder="Upload Image">
                                        <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-warning text-white" type="button">Upload</button>
                                    </span>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <img id="upz" class="mt-4" style="width:100px; height:100px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="editstore" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="editform" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="Store Name">Store Name</label>
                            <input type="text" id="editstorename" class="form-control" name="store_name" placeholder="Store Name">
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label for="Store Address">Store Address</label>--}}
                            {{--<input type="text" id="editstoreaddress" class="form-control" name="store_address" placeholder="Store Address">--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label>File upload</label>
                                    <input type="file" name="store_image" id="editstoreimage" onchange="readURL(this);"
                                           class="file-upload-default">
                                    <div class="input-group">
                                        <input type="text" name="store_image" id="editstoreimage2" class="form-control file-upload-info"
                                               disabled placeholder="Upload Image">
                                        <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary mr-2 mt-4 float-right">Update</button>
                    </form>
                </div>

            </div>
        </div>
    </div>



    <div class="row">
        @foreach($stores as $store)
            <div class="col-md-4  ">
                <div class="card">
                    <div class="card-body text-center">
                        <div>
                            <img src="{{$store->store_image}}" class="img-lg rounded-circle mb-2" alt="profile image">
                            <h4>{{$store->store_name}}</h4>
                            <p>
                                <i class="ti-location-pin text-success"></i>{{$store->store_address}}
                            </p>
                        </div>
                        <div class="row " style="margin-bottom: 10px;">
                            <div class="col-md-4">
                                <a  href="{{route('admin.store.branches',$store->slug)}}" class="btn btn-info btn-sm" style="color:white;"><i class="ti-eye"></i></a>
                            </div>
                            <div class="col-md-4">
                                <a data-toggle="modal" data-target="#editstore"
                                        data-storename="{{$store->store_name}}"
                                        data-storeaddress="{{$store->store_address}}"
                                        data-storeimage ="{{$store->store_image}}"
                                        data-formaction="{{route('admin.store.update',$store->slug)}}"
                                        class="btn btn-success btn-sm " style="color:white;"> <i class="ti-pencil" ></i> </a>
                            </div>
                            <div class="col-md-4">
                                <a  href="{{route('admin.store.deactivate',$store->slug)}}" class="btn btn-danger btn-sm" style="color:white;"><i class="ti-trash"></i></a>
                            </div>

                        </div>
                        <div class="row">

                        </div>
                        {{--<a  href="{{route('admin.store.edit',$store->slug)}}" class="btn btn-info mt-3 mb-4">View Branches</a>--}}


                        <div class="border-top pt-3">
                            <div class="row">
                                <div class="col-6">
                                    <h6>{{$store->totalproducts}}</h6>
                                    <p>Total Products</p>
                                </div>
                                <div class="col-6">
                                    <h6>{{$store->totalbranches}}</h6>
                                    <p>Total Branches</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
          
            @endforeach




    </div>
    <div class="row mt-4">
        <div class="col-md-6"></div>
        <div class="col-md-3"></div>
        <div class="col-md-3 pull-right">
            {{$stores->render()}}
        </div>

    </div>

</div> 


            
             <nav>
                    <ul class="pagination flex-wrap pagination-flat pagination-success">
                       {{$stores->render()}}
                    </ul>
                  </nav>
      


    </div>
  </div>

    <script>
        $(document).ready(function () {

            $('#editstore').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var storeid = button.data('storeid');
                var formaction = button.data('formaction');
                var storename = button.data('storename');
                var storeaddress = button.data('storeaddress');
                var storeimage = button.data('storeimage');
                var modal = $(this);
                modal.find('.modal-header #headertitle').text('Edit ' + storename);
                modal.find('.modal-body #editform').attr('action', formaction);
                modal.find('.modal-body #editstorename').val(storename);
                modal.find('.modal-body #editstoreaddress').val(storeaddress);
                modal.find('.modal-body #upz').attr('src', storeimage)
                // modal.find('.modal-body #editstoreimage').val(storeimage)
                // modal.find('.modal-body #editstoreimage2').val(storeimage)

            })
        });

    </script>
@endsection