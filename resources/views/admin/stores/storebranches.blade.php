@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{url()->previous()}}">All Listed Stores</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed Stores"> Store Branches</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="border-bottom text-center pb-4">
                                <img src="{{$store->store_image}}"
                                     alt="profile" class="img-lg rounded-circle mb-3">
                                <div class="mb-3">
                                    <h4>{{$store->store_name}}</h4>
                                    <p>
                                        <i class="ti-location-pin text-success"></i>
                                        {{$store->store_address}}
                                    </p>

                                </div>
                                <div class="d-flex justify-content-center">
                                </div>
                            </div>

                            <div class="py-4">
                                <p class="clearfix">
                          <span class="float-left">
                            Status
                          </span>
                                    <span class="float-right text-muted">
                          <label class="badge badge-success btn-inverse-success">
                              @if($store->store_status == 1) Approved

                              @else
                                  Pending
                              @endif

                          </label>
                          </span>
                                </p>
                                <p class="clearfix">
                          <span class="float-left">
                            Total Products
                          </span>
                                    <span class="float-right badge badge-pill badge-success">
                           {{$store->totalproducts}}
                          </span>
                                </p>
                                <p class="clearfix">
                          <span class="float-left">
                            Total Branches
                          </span>
                                    <span class="float-right badge badge-pill badge-warning">
                            {{$store->totalbranches}}
                          </span>
                                </p>
                            </div>

                            {{--Advert--}}
                            <div class="card data-icon-card-primary mt-4">
                                <div class="card-body">
                                    <p class="card-title text-white">Number of Meetings</p>
                                    <div class="row">
                                        <div class="col-8 text-white">
                                            <h3>3404</h3>
                                            <p class="text-white font-weight-light mb-0">The total number of sessions
                                                within the date range. It is the period time</p>
                                        </div>
                                        <div class="col-4 background-icon">
                                            <i class="ti-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="col-md-9">


                            <div class="profile-feed">

                                <div class="card">
                                    <div class="card-body">

                                        <ul class="nav nav-pills nav-pills-success" id="pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill"
                                                   href="#branch" role="tab" aria-controls="branch"
                                                   aria-selected="true">Store Branches</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                                   href="#user" role="tab" aria-controls="pills-profile"
                                                   aria-selected="false">Store Users</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                                   href="#storeproduct" role="tab" aria-controls="pills-profile"
                                                   aria-selected="false">Store Products</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                                   href="#five" role="tab" aria-controls="pills-profile"
                                                   aria-selected="false">Branch Products</a>
                                            </li>


                                        </ul>
                                        <div class="tab-content" id="pills-tabContent">
                                            <div class="tab-pane fade show active" id="branch" role="tabpanel"
                                                 aria-labelledby="branch-tab">
                                                <h6>Create New Store Branch</h6>
                                                <p class="card-description">
                                                </p>
                                                <form action="{{route('admin.store.createstorebranch')}}" method="post"
                                                      class="form-inline">{{csrf_field()}}

                                                    <input type="text" hidden name="storeid" value="{{$store->id}}"/>
                                                    <label class="sr-only" for="Branch Name">Branch Name</label>
                                                    <input type="text" class="form-control mb-2 mr-sm-2"
                                                           name="branch_name" placeholder="Branch Name">

                                                    <label class="sr-only" for="Address">Branch Address</label>
                                                    <div class="input-group mb-2 mr-sm-2">
                                                        <input name="branch_address" type="text" class="form-control"
                                                               placeholder="Branch Address">
                                                    </div>

                                                    <button type="submit" class="btn btn-warning text-white mb-2">
                                                        Create
                                                    </button>
                                                </form>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="table-responsive">
                                                            <table id="order-listing" class="table ">
                                                                <thead>
                                                                <tr class="bg-success text-white">
                                                                    <th>S/N #</th>
                                                                    <th>Branch Name</th>
                                                                    <th>Branch Address</th>
                                                                    <th>Date Created</th>
                                                                    <th>Actions</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($branches as $branch)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$branch->branch_name}}</td>
                                                                        <td>{{$branch->branch_address}}</td>
                                                                        <td>{{(new Carbon\Carbon($branch->created_at))->diffForHumans()}}</td>
                                                                        <td>
                                                                            <button class="btn-sm btn-outline-warning btn-icon"
                                                                                    data-toggle="modal"
                                                                                    data-target="#editbranch"
                                                                                    data-name="{{$branch->branch_name}}"
                                                                                    data-editform="{{route('admin.store.updatestorebranch',$branch->slug)}}"
                                                                                    data-address="{{$branch->branch_address}}"


                                                                            >
                                                                                <i
                                                                                        class="ti-pencil"></i> Edit
                                                                            </button>
                                                                            <a onclick="confirm('Are you sure to delete');return"
                                                                               href="{{route('admin.store.deletestorebranch',$branch->slug)}}"
                                                                               class="btn-sm btn-outline-danger btn-icon"

                                                                            ><i class="ti-trash"></i>Delete</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade show " id="user" role="tabpanel"
                                                 aria-labelledby="user-tab">
                                                <h6>Create Store user</h6>
                                                <p class="card-description">
                                                </p>
                                                <form action="{{route('admin.store.createstoreuser')}}" method="post"
                                                      class="form-inline">{{csrf_field()}}

                                                    <input type="text" hidden name="storeid" value="{{$store->id}}"/>
                                                    <input type="text" hidden name="store_name"
                                                           value="{{$store->store_name}}"/>
                                                    {{--<label class="sr-only" for="Branch Name">Name</label>--}}
                                                    <input type="text" class="form-control mb-2 mr-sm-2"
                                                           name="name" placeholder="Name">
                                                    <input name="email" type="text" class="form-control mb-2 mr-sm-2 "
                                                           placeholder="email">
                                                    <input name="password" type="text" class="form-control mb-2 mr-sm-2"
                                                           placeholder="password">
                                                    <select name="storebranches[]" class="form-control mb-2 mr-sm-2"
                                                            multiple>
                                                        <option>select branch</option>
                                                        @if(!empty($branches))
                                                            @foreach($branches as $b)
                                                                <option value="{{$b->id}}">{{$b->branch_name}}</option>
                                                            @endforeach
                                                        @else
                                                            <option>No active branch</option>
                                                        @endif
                                                    </select>

                                                    <button type="submit" class="btn btn-warning text-white mb-2">
                                                        Create
                                                    </button>
                                                </form>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="table-responsive">
                                                            <table id="order-listing" class="table order-listing">
                                                                <thead>
                                                                <tr class="bg-success text-white">
                                                                    <th>S/N #</th>
                                                                    <th>Name</th>
                                                                    <th>Email</th>
                                                                    <th>Branches</th>
                                                                    <th>Actions</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($storeusers as $user)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$user->store_user_name}}</td>
                                                                        <td>{{$user->store_user_email}}</td>
                                                                        <td>
                                                                            @foreach($user->storebranch as $branch)
                                                                                {{$branch->branch_name.','}}
                                                                            @endforeach
                                                                        </td>
                                                                        <td>
                                                                            <button class="btn-sm btn-outline-warning btn-icon"
                                                                                    data-toggle="modal"
                                                                                    data-target="#edituser"
                                                                                    data-name="{{$user->store_user_name}}"
                                                                                    data-editform="{{route('admin.store.updatestoreuser',$user->slug)}}"
                                                                                    data-email="{{$user->store_user_email}}"
                                                                                    data-branch="{{$user->storebranch}}"


                                                                            >
                                                                                <i
                                                                                        class="ti-pencil"></i> Edit
                                                                            </button>
                                                                            <a onclick="confirm('Are you sure to delete'); return"
                                                                               href="{{route('admin.store.deletestoreuser',$user->slug)}}"
                                                                               class="btn-sm btn-outline-danger btn-icon"
                                                                            ><i
                                                                                        class="ti-trash"></i>Delete</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade show" id="storeproduct" role="tabpanel">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="row" id="allproducts" >   <button id="showAddproduct" class="btn-sm btn-outline-success pull-right">Add Product</button>

                                                  <br>
                                                            <div  class="table-responsive">
                                                                <h3 class="text-center" style="text-decoration: underline">All products</h3>
                                                                <table id="order-listing" class="table order-listing">
                                                                    <thead>
                                                                    <tr class="bg-success text-white">
                                                                        <th>S/N #</th>
                                                                        <th>Product Name</th>
                                                                        <th>Categody</th>
                                                                        <th>Product Image</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>


                                                                    @forelse($storeproducts as $t)
                                                                        <tr>
                                                                            <td>{{$loop->iteration}}</td>
                                                                            <td>{{@$t->storeproduct_name}}
                                                                            </td>
                                                                            <td>{{@$t->category->category_name}}</td>
                                                                            <td><img src="{{$t->storeproduct_image}}" alt="image" style="width: 100px;height: 100px;">                              </td>
                                                                            <td>
                                                                                <a onclick="confirm('Are you sure to remove?'); return;"
                                                                                        href="{{route('admin.store.removeproduct',$t->slug)}}" class="btn btn-sm btn-out-line-light">Remove Product</a>
                                                                            </td>
                                                                        </tr>
                                                                    @empty

                                                                    @endforelse


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="row" id="addproduct" style="display: none">
                                                            <button id="showallproduct" class="btn-sm btn-outline-success">View Product</button><br>


                                                            <div class="col-12" >
                                                                <h3 class="text-center" style="text-decoration: underline">Add products</h3>
                                                                <form action="{{route('admin.store.addproduct',$store->slug)}}"
                                                                      method="post">
                                                                    {{csrf_field()}}
                                                                    @if(!empty($toBeAddedproducts))
                                                                        <div class="row">
                                                                            <div class="col-md-8"></div>
                                                                            <div class="col-md-4">
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <label>
                                                                                            <input id="allproduct"
                                                                                                   type="checkbox"
                                                                                                   class="btn btn-primary text-white">
                                                                                            <br>Check all</label>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <button type="submit"
                                                                                                class="btn btn-warning text-white pull-right">
                                                                                            <i
                                                                                                    class="ti-pencil-alt"></i>
                                                                                            Save
                                                                                        </button>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <br>
                                                                        <div class="row">
                                                                            <div class="table-responsive">

                                                                                <table id="order-listing" class="table order-listing">
                                                                                    <thead>
                                                                                    <tr class="bg-success text-white">
                                                                                        <th>S/N #</th>
                                                                                        <th>Product Name</th>
                                                                                        <th>Category</th>
                                                                                        <th>Product Image</th>
                                                                                        <th>Check</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>


                                                                                    @forelse($toBeAddedproducts as $t)
                                                                                        <tr>
                                                                                            <td>{{$loop->iteration}}</td>
                                                                                            <td>{{$t->product_name}}<i
                                                                                                        id="icon_check"
                                                                                                        class="ti-check icon-check"
                                                                                                        style="display: none"></i>
                                                                                            </td>
                                                                                            <td>{{@$t->category->category_name}}</td>
                                                                                            <td>
                                                                                                {{--<div style="background-image: url('{{$t->product_image}}');width:100px;height: 100px;"></div>--}}
                                                                                                <img src="{{$t->product_image}}" style="width:100px;height: 100px;" alt="imag">
                                                                                            </td>
                                                                                            <td>
                                                                                                <input type="checkbox"
                                                                                                       id="singlecheck"
                                                                                                       name="createproductid[]"
                                                                                                       class="form-control"
                                                                                                       value="{{$t->id}}">
                                                                                            </td>
                                                                                        </tr>
                                                                                    @empty
                                                                                        <h3 class="text-center">No New
                                                                                            Products</h3>
                                                                                    @endforelse


                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                        <h6>No New products</h6>
                                                                    @endif

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="tab-pane fade show" id="branchproduct"
                                                     role="tabpanel"></div>
                                            </div>
                                            <div class="tab-pane fade show" id="five" role="tabpanel" aria-labelledby="branchproduct-tab" >
                                                <div id="allbranchproduct">
                                                    <button id="btnAddBranchProduct" class="btn-sm btn-outline-success pull-right">Add Branch Product</button>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h3 class="text-center" style="text-decoration: underline">Branches Products</h3>
                                                            <div class="table-responsive">
                                                                <table id="order-listing" class="table order-listing">
                                                                    <thead>
                                                                    <tr class="bg-success text-white">
                                                                        <th>S/N #</th>
                                                                        <th>Branch</th>
                                                                        <th>Product category</th>
                                                                        <th>Product Name</th>
                                                                        <th>Product Image</th>
                                                                        <th>Quantity</th>
                                                                        <th>Price</th>
                                                                        <th>DealPrice</th>
                                                                        <th>Deal %</th>
                                                                        <th>Deal End Date</th>
                                                                        <th>Measurement/Size</th>
                                                                        <th>Deal Status</th>

                                                                        <th>Actions</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @if(!empty($storebranchesProducts))
                                                                        @forelse($storebranchesProducts as $pro)
                                                                            <tr>
                                                                                <td>{{$loop->iteration}}</td>
                                                                                <td>{{@$pro->storebranch->branch_name}}</td>
                                                                                <td>{{@$pro->category->category_name}}</td>
                                                                                <td>{{$pro->productname}}</td>
                                                                                <td><img src="{{$pro->productimage}}" alt="img" style="width: 50px; height: 50px;"></td>
                                                                                <td>{{$pro->quantity}}</td>
                                                                                <td>{{$pro->price}}</td>
                                                                                <td>{{$pro->dealprice}}</td>
                                                                                <td>{{number_format($pro->dealpercentage).'%'}}</td>
                                                                                <td>{{(new Carbon\Carbon($pro->dealexpire))->diffForHumans()}}</td>
                                                                                <td>{{$pro->measurement.'/'.$pro->size}}</td>
                                                                                <td>
                                                                                    @if ($pro->dealexpire > $today )
                                                                                        <label class="badge badge-success btn-inverse-success">
                                                                                            Active
                                                                                        </label>
                                                                                    @else
                                                                                        <label class="badge badge-success btn-inverse-danger">
                                                                                            Inactive
                                                                                        </label>
                                                                                    @endif
                                                                                </td>
                                                                                <td>
                                                                                    <button data-toggle="modal" data-target="#editproduct"
                                                                                            data-categoryid="{{$pro->categoryid}}"
                                                                                            data-storeid="{{$pro->storeid}}"
                                                                                            data-storebranchid="{{$pro->storebranchid}}"
                                                                                            data-storeproductid="{{$pro->storeproductid}}"
                                                                                            data-productname="{{$pro->productname}}"
                                                                                            data-productimage="{{$pro->productimage}}"
                                                                                            data-quantity="{{$pro->quantity}}"
                                                                                            data-price="{{$pro->price}}"
                                                                                            data-dealprice="{{$pro->dealprice}}"
                                                                                            data-dealexpire="{{$pro->dealexpire}}"
                                                                                            data-size="{{$pro->size}}"
                                                                                            data-measurement="{{$pro->measurement}}"
                                                                                            data-isspecial="{{$pro->isspecial}}"
                                                                                            data-editform="{{route('admin.store.branch.updateproduct',$pro->slug)}}"
                                                                                            class="btn-sm btn-dark btn-icon"><i
                                                                                                class="ti-pencil"> Edit</i>
                                                                                    </button>

                                                                                </td>
                                                                            </tr>
                                                                        @empty
                                                                        @endforelse
                                                                    @endif


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="addbranchproduct" style="display: none;">
                                                    <button id="btnBranchesProduct" class="btn-sm btn-outline-success pull-right">View Branches Product</button>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h3 class="text-center" style="text-decoration: underline">Add Branch Product</h3>
                                                            <form  class="forms-sample mt-4" action="{{route('admin.store.branch.addproduct')}}" method="post">
                                                                {{csrf_field()}}
                                                                {{--<input type="text" name="branchid" value="{{$branch->id}}" hidden>--}}
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Branch</label>
                                                                            <select name="storebranchid" id="storebranchid" class="form-control" >
                                                                                <option >select Branch</option>
                                                                                @forelse($branches as $branch)
                                                                                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                                                                @empty
                                                                                @endforelse
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Category</label>
                                                                            <select name="categoryid" id="categoryid" class="form-control" disabled>
                                                                                <option>select category</option>
                                                                                @forelse($categories as $category)
                                                                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                                                                @empty
                                                                                @endforelse
                                                                            </select>
                                                                            <span id="errorcategory" style="color:red;display: none;"></span>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Product name</label>
                                                                            <select name="productid" id="productid" class="form-control"  >
                                                                            </select>
                                                                            <span id="spinloader" style="display:none;" ><img style="width:30px; height:30px;"
                                                                                src="{{asset('static/images/loader.png')}}"></span>
                                                                            <span id="errorproduct" style="color:red;display: none;"></span>
                                                                        </div>
                                                                    </div>

                                                                {{-- </div>
                                                                <div class="row"> --}}
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="Full Name">Product type</label>
                                                                                <select name="producttype" id="isspecial" class="form-control" required>
                                                                                    <option value="0">Offer</option>
                                                                                   <option value="1">Special</option>

                                                                                </select>

                                                                            </div>
                                                                        </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Quantity</label>
                                                                            <input type="text" name="quantity" class="form-control" placeholder="quantity" id="quanity">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Price</label>
                                                                            <input type="text" name="price" class="form-control" placeholder="price" id="price">
                                                                        </div>
                                                                    </div>

                                                                {{-- </div> --}}
                                                                {{-- <div class="row"> --}}
                                                                    <div class="col-md-4" id="dealpricebox">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Deal price</label>
                                                                            <input type="text" name="dealprice" class="form-control" placeholder="deal price" id="dealprice">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Measurement Unit</label>
                                                                            <select name="measurement" class="form-control" id="measurement">
                                                                                <option >Select measurement</option>
                                                                                @foreach ($measurements as $m)
                                                                                    <option value="{{$m->title}}">{{ucwords($m->title)}}</option>
                                                                                @endforeach
                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Product Measurement</label>
                                                                            <input type="text" name="size" class="form-control" placeholder="size" id="size">
                                                                        </div>
                                                                    </div>
                                                                {{-- </div> --}}
                                                                {{-- <div class="row"> --}}
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="Full Name">Deal End Date</label>
                                                                            <input type="date" name="dealexpire" class="form-control" placeholder="" id="dealexpire">
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <button type="submit" class="btn btn-primary mr-2 mt-4 float-right" id="submitbtn">Add Product</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade" id="edituser" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
             aria-hidden="true"
             style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form id="editform" method="post">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Store Name"> Name</label>
                                        <input type="text" id="name" class="form-control" name="name"
                                               placeholder=" Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Store Address">Email</label>
                                        <input type="email" id="email" class="form-control" name="email"
                                               placeholder="Email Address" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Store Address">Branches</label>
                                        <select name="storebranches[]" id="branch" class="form-control mb-2 mr-sm-2"
                                                multiple>
                                            <option>select branch</option>
                                            @if(!empty($branches))
                                                @foreach($branches as $b)
                                                    <option value="{{$b->id}}">{{$b->branch_name}}</option>
                                                @endforeach
                                            @else
                                                <option>No active branch</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Store Address">Password</label>
                                        <input type="password" class="form-control" name="password"
                                               placeholder="password">
                                    </div>
                                </div>
                            </div>


                            <button type="submit" class="btn btn-success mr-2 mt-4 float-right">Submit</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="editbranch" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
             aria-hidden="true"
             style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form id="editform" method="post">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Store Name"> Name</label>
                                        <input type="text" id="name" class="form-control" name="branch_name"
                                               placeholder="branch Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Store Address">Address</label>
                                        <input type="text" id="address" class="form-control" name="branch_address"
                                               placeholder=" Address" required>
                                    </div>
                                </div>
                            </div>


                            <button type="submit" class="btn btn-success mr-2 mt-4 float-right">Submit</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="editproduct" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
             style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i>
                            <span>
                            <img id="productimg" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                        </span>
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form id="editform" class="forms-sample mt-4" method="post">
                            {{csrf_field()}}

                            <div class="row">
                                    <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="Full Name">Product type</label>
                                                <select name="producttype" id="isspecial" class="form-control" required>
                                                    <option value="0">Offer</option>
                                                   <option value="1">Special</option>

                                                </select>

                                            </div>
                                        </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Full Name">Quantity</label>
                                        <input type="text" name="quantity" class="form-control" placeholder="quantity" id="quantity">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Full Name">Price</label>
                                        <input type="text" name="price" class="form-control" placeholder="price" id="price">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Full Name">Measurement</label>
                                        <select name="measurement" class="form-control" id="measurement">
                                            <option >Select measurement</option>
                                            @foreach ($measurements as $m)
                                                <option value="{{$m->title}}">{{ucwords($m->title)}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            {{-- </div>
                            <div class="row"> --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Full Name">Product Size</label>
                                        <input type="text" name="size" class="form-control" placeholder="size" id="size">
                                    </div>
                                </div>
                                <div class="col-md-4" id='dealpricebox'>
                                    <div class="form-group">
                                        <label for="Full Name">Deal price</label>
                                        <input type="text" name="dealprice" class="form-control" placeholder="deal price" id="dealprice">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Full Name">Deal End Date</label>
                                        <input type="date" name="dealexpire" class="form-control" placeholder="" id="dealexpire">
                                    </div>
                                </div>



                            </div>

                            <button type="submit" class="btn btn-primary mr-2 mt-4 float-right" id="submitbtn">Update</button>





                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="editspecialproduct" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
             style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i>
                            <span>
                            <img id="productimg" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                        </span>
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form id="editform" class="forms-sample mt-4" method="post">
                            {{csrf_field()}}
                            <input type="text" name="productid" id="productid" hidden>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Full Name">Quantity</label>
                                        <input type="text" name="quantity" class="form-control" placeholder="quantity" id="quantity">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Full Name">Price</label>
                                        <input type="text" name="price" class="form-control" placeholder="price" id="price">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Full Name">Measurement</label>
                                        <select name="measurement" class="form-control" id="measurement">
                                            <option >Select measurement</option>
                                            @foreach ($measurements as $m)
                                                <option value="{{$m->title}}">{{ucwords($m->title)}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Full Name">Product Size</label>
                                        <input type="text" name="size" class="form-control" placeholder="size" id="size">
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Full Name">Deal End Date</label>
                                        <input type="date" name="offerexpire" class="form-control" placeholder="" id="offerexpire">
                                    </div>
                                </div>



                            </div>

                            <button type="submit" class="btn btn-primary mr-2 mt-4 float-right" id="submitbtn">Update</button>





                        </form>
                    </div>

                </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $('.order-listing').dataTable();
                var btnShowStoreProduct = $("#showallproduct");
                var btnShowAddProduct = $("#showAddproduct");
                var allstoreproduct = $("#allproducts");
                var addstoreproduct = $('#addproduct');

                var btnShowBranchesProduct = $("#btnBranchesProduct");
                var btnAddBranchProduct = $("#btnAddBranchProduct");
                var allbranchesproducts = $("#allbranchproduct");
                var addbranchproduct = $("#addbranchproduct");

                // var viewSpecialProductBtn = $("#viewSpecialProductBtn");
                // var addSpecialProductBtn = $("#addSpecialProductBtn");
                // var allspecialproduct = $("#allspecialproduct");
                // var addspecialproduct = $("#addspecialproduct")

                var branchid;
                var errorproduct = $("#errorproduct");
                var errorproducttwo = $("#errorproduct2");
                var errorcategory = $("#errorcategory");
                var errorcategorytwo = $("#errorcategory2");
                var categorybox = $("#categoryid");
                var categoryboxtwo = $("#categoryid2");
                var productboxtwo = $("#productid2");



                // viewSpecialProductBtn.click(function () {
                //     addspecialproduct.fadeOut();
                //     allspecialproduct.fadeIn();
                // });
                // addSpecialProductBtn.click(function () {
                //     allspecialproduct.fadeOut();
                //     addspecialproduct.fadeIn();
                // });

                btnShowStoreProduct.click(function () {
                    addstoreproduct.fadeOut();
                    allstoreproduct.fadeIn();
                });
                btnShowAddProduct.click(function(){
                    allstoreproduct.fadeOut();
                    addstoreproduct.fadeIn();

                });

                btnAddBranchProduct.click(function(){
                    allbranchesproducts.fadeOut();
                    addbranchproduct.fadeIn();
                });
                btnShowBranchesProduct.click(function(){
                    addbranchproduct.fadeOut();
                    allbranchesproducts.fadeIn();

                });
                $("#allproduct").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                    $('.icon-check').toggle();
                });
                $("#singlecheck").on('change', function () {
                    $('#icon-check').show();
                });

                $('select[name="storebranchid"]').on('change',function(){
                    branchid = $(this).val();
                    categorybox.attr('disabled','disabled');
                    if(branchid){
                        categorybox.attr('disabled',false);
                    }else{
                        categorybox.attr('disabled','disabled');
                        errorcategory.text('select a branch')
                        errorcategory.show();
                    }
                });
                $('#storebranchid2').on('change',function(){
                    branchid = $(this).val();
                    categoryboxtwo.attr('disabled','disabled');
                    if(branchid){
                        categoryboxtwo.attr('disabled',false);
                    }else{
                        categoryboxtwo.attr('disabled','disabled');
                        errorcategorytwo.text('select a branch')
                        errorcategorytwo.show();
                    }
                });
                //Ajax
                $('select[name="categoryid"]').on('change',function(){
                    var category = $(this).val();
                    console.log(category);
                    console.log('this is branch '+branchid);
                    errorproduct.hide();
                    $("#spinloader").hide();

                        if(category){
                            $.ajax({
                                url: 'loadproduct/'+branchid+'/' + category,
                                type: "GET",
                                beforeSent:function(){
                                    $("#spinloader").fadeIn();
                                },
                                success:function(data){
                                    $("#spinloader").hide();
                                    console.log(typeof data);
                                    if(data !='') {
                                        $('select[name="productid"]').empty();
                                        $.each(data, function (key, value) {
                                            $.each(value, function (p, v) {
                                                console.log(v.id);
                                                $('select[name="productid"]').append('<option value="' + v.id + '">' + v.storeproduct_name + '</option>');

                                            });
                                            // data = JSON.parse(data);


                                        });
                                    }else {
                                        $("#spinloader").hide();
                                        errorproduct.text('products not available ');
                                        errorproduct.show();
                                    }
                                },
                                error:function(err){

                                }
                            });
                        }else{

                        }



                });
                categoryboxtwo.on('change',function(){
                    var category = $(this).val();
                    console.log(category);
                    console.log('this is branch '+branchid);
                    errorproduct.hide();

                    if(category){
                        $.ajax({
                            url: 'loadproduct/'+branchid+'/' + category,
                            type: "GET",
                            beforeSent:function(){
                                // loader.show();
                            },
                            success:function(data){
                                // loader.hide();
                                console.log(typeof data);
                                if(data !='') {
                                    productboxtwo.empty();
                                    $.each(data, function (key, value) {
                                        $.each(value, function (p, v) {
                                            console.log(v.id);
                                            productboxtwo.append('<option value="' + v.id + '">' + v.storeproduct_name + '</option>');

                                        });
                                        // data = JSON.parse(data);


                                    });
                                }else {
                                    errorproducttwo.text('products not available ');
                                    errorproducttwo.show();
                                }
                            },
                            error:function(err){

                            }
                        });
                    }else{

                    }



                });
                $('select[name="producttype"]').on('change',function(){
                    var ans = $(this).val();
                    if(ans == 1){
                        $("#dealpricebox").hide();
                    }else{
                        $("#dealpricebox").show();
                    }
                });

                //modal
                $('#edituser').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget);// Button that triggered the modal
                    var name = button.data('name');
                    var formaction = button.data('editform');
                    var email = button.data('email');
                    var branch = button.data('branch');
                    var modal = $(this);
                    modal.find('.modal-header #headertitle').text('Edit ' + name);
                    modal.find('.modal-body #editform').attr('action', formaction);
                    modal.find('.modal-body #name').val(name);
                    modal.find('.modal-body #email').val(email);
                    modal.find('.modal-body #branch').val(branch);



                })
                $('#editbranch').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget);// Button that triggered the modal
                    var name = button.data('name');
                    var formaction = button.data('editform');
                    var address = button.data('address');
                    var modal = $(this);
                    modal.find('.modal-header #headertitle').text('Edit ' + name);
                    modal.find('.modal-body #editform').attr('action', formaction);
                    modal.find('.modal-body #name').val(name);
                    modal.find('.modal-body #address').val(address);

                });
                $('#editproduct').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget); // Button that triggered the modal
                    var productname = button.data('productname');
                    var productimage = button.data('productimage');
                    var price = button.data('price');

                    var quantity = button.data('quantity');
                    var dealprice = button.data('dealprice');
                    var dealexpire = button.data('dealexpire');
                    var size= button.data('size');
                    var measurement = button.data('measurement');
                    var categoryid = button.data('categoryid');
                    var formaction = button.data('editform');
                    var isspecial = button.data('isspecial');

                    var modal = $(this);

                    modal.find('.modal-header #headertitle').text('Edit ' + productname);
                    modal.find('.modal-body #editform').attr('action',formaction);
                    modal.find('.modal-body #price').val(price);

                    modal.find('.modal-body #quantity').val(quantity);
                    modal.find('.modal-body #dealprice').val(dealprice);
                    modal.find('.modal-body #size').val(size);
                    modal.find('.modal-body #dealexpire').val(dealexpire);
                    modal.find('.modal-body #measurement').val(measurement);
                    modal.find('.modal-body #isspecial').val(isspecial);
                    modal.find('.modal-body #productimg').attr('src',productimage);

                    var dealpricebox = modal.find('.modal-body #dealpricebox');
                    var producttype = modal.find('.modal-body #isspecial');
                    if(producttype.val() == 1){
                        dealpricebox.hide();
                    }

                    producttype.on("change",function(){
                        var ans = $(this).val();
                    if(ans == 1){
                        dealpricebox.hide();
                    }else{
                        dealpricebox.show();
                    }
                    })


                });
                $('#editspecialproduct').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget); // Button that triggered the modal
                    var productname = button.data('productname');
                    var productimage = button.data('productimage');
                    var price = button.data('price');
                    var productid = button.data('storeproductid');
                    var quantity = button.data('quantity');
                    var offerexpire = button.data('offerexpire');
                    var size = button.data('size');
                    var measurement = button.data('measurement');
                    var formaction = button.data('editform');

                    var modal = $(this);

                    modal.find('.modal-header #headertitle').text('Edit Special' + productname);
                    modal.find('.modal-body #editform').attr('action',formaction);
                    modal.find('.modal-body #price').val(price);
                    modal.find('.modal-body #productid').val(productid);
                    modal.find('.modal-body #quantity').val(quantity);
                    modal.find('.modal-body #size').val(size);
                    modal.find('.modal-body #offerexpire').val(offerexpire);
                    modal.find('.modal-body #measurement').val(measurement);

                });

            });
        </script>
        <style>
            .icon-check {
                margin: 0 10px;
                font-size: 20px;
                border: 5px;
                color: green;
            }

            input[type='checkbox'] {
                -webkit-appearance: none;
                width: 20px;
                height: 20px;
                background: white;
                border-radius: 5px;
                border: 2px solid #555;
            }

            input[type='checkbox']:checked {
                background: green;
                border: 5px solid green;
            }

        </style>

@endsection
