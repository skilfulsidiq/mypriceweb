@extends('layouts.backend')

@section('content')

                <nav aria-label="breadcrumb">
                      <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url()->previous()}}">All Listed Stores</a></li>
                        <li class="breadcrumb-item active" aria-current="All Listed Stores"> Edit Store Branches</li>
                      </ol>
                    </nav>

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="border-bottom text-center pb-4">
                        <img src="https://res.cloudinary.com/dqso22qws/image/upload/v1529530160/ula8lsy1xj2pjzf6bkzb.png" alt="profile" class="img-lg rounded-circle mb-3">
                        <div class="mb-3">
                        <h4>Shoprite Nigeria</h4>
                      <p>
                        <i class="ti-location-pin text-success"></i>
                        15 Abayomi Durosinmi-Etti Street, Lekki Phase 1 (Ocean Side), Lagos
                      </p>
                      <p>
                        <i class="ti-email text-success"></i>
                        admin@shoprite.com
                      </p>

                        </div>
                        <div class="d-flex justify-content-center">
                          <button class="btn btn-warning mr-1">Edit</button>
                          <button class="btn btn-danger">Suspend</button>
                        </div>
                      </div>

                      <div class="py-4">
                        <p class="clearfix">
                          <span class="float-left">
                            Status
                          </span>
                          <span class="float-right text-muted">
                          <label class="badge badge-success btn-inverse-success">Approved</label>
                          </span>
                        </p>
                        <p class="clearfix">
                          <span class="float-left">
                            Total Products
                          </span>
                          <span class="float-right badge badge-pill badge-success">
                           564
                          </span>
                        </p>
                        <p class="clearfix">
                          <span class="float-left">
                            Total Branches
                          </span>
                          <span class="float-right badge badge-pill badge-warning">
                            52
                          </span>
                        </p>
                      </div>


                  <div class="card data-icon-card-primary mt-4">
                    <div class="card-body">
                      <p class="card-title text-white">Number of Meetings</p>                      
                      <div class="row">
                        <div class="col-8 text-white">
                          <h3>3404</h3>
                          <p class="text-white font-weight-light mb-0">The total number of sessions within the date range. It is the period time</p>
                        </div>
                        <div class="col-4 background-icon">
                          <i class="ti-calendar"></i>
                        </div>
                      </div>
                    </div>
                  </div>

                    </div>


                    <div class="col-lg-8">


                      <div class="profile-feed">

               <div class="card">
                <div class="card-body">
                  <h4>Edit Branch Information</h4>
                  <p class="card-description">
                   edit branch information
                  </p>
                  <form class="form-inline">
                  
                    <label class="sr-only" for="Branch Name">Branch Name</label>
                    <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Branch Name">
                  
                    <label class="sr-only" for="Address">Branch Address</label>
                    <div class="input-group mb-2 mr-sm-2">
                      <input type="text" class="form-control" placeholder="Branch Address">
                    </div>

                    <button type="submit" class="btn btn-info mb-2 text-white">Update</button>
                  </form>
                </div>
              </div>

                        <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr class="bg-primary text-white">
                            <th>S/N #</th>
                            <th>Branch Name</th>
                            <th>Branch Address</th>
                            <th>Date Created</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>1</td>
                            <td>Shoprite</td>
                            <td>Ikeja, Lagos</td>
                            <td>2012/08/03</td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Konga</td>
                            <td>Victoria Island, Lagos</td>
                            <td>2015/04/01</td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Country gorceries</td>
                            <td>oshodi, Lagos</td>
                            <td>2010/11/21</td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Reed Supermarket</td>
                            <td>lagos-island, Lagos</td>
                            <td>2016/01/12</td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Ebeanor Supermarket</td>
                            <td>Lekki, Lagos</td>
                            <td>2017/12/28</td>
                            <td>
                              <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>                        
                          </tr>
                        <tr>
                            <td>6</td>
                            <td>Hubmart</td>
                            <td>Lekki, Lagos</td>
                            <td>2000/10/30</td>
                            <td>
                              <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>                        
                          </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


        </div>
                       
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        




              
@endsection