@extends('layouts.backend')

@section('content')

                <nav aria-label="breadcrumb">
                      <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-primary">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url()->previous()}}">All Listed Stores</a></li>
                        <li class="breadcrumb-item active" aria-current="All Listed Stores">All Registered Managers</li>
                      </ol>
                    </nav>



          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="row">
                <div class="col-12 col-xl-7">
                  <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <div class="mb-3 mb-xl-0">
                      <button class="btn btn-primary text-white" data-toggle="modal" data-target="#exampleModal-4"><i class="ti-pencil-alt"></i> Create Store</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                        <h4 class="text-white"><i class="ti-pencil-alt"></i> Create Store</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                      
                  <form>
                    <div class="form-group">
                      <label for="Store Name">Store Name</label>
                      <input type="text" class="form-control" placeholder="Store Name">
                    </div>
                    <div class="form-group">
                      <label for="Store Address">Store Address</label>
                      <input type="text" class="form-control" placeholder="Store Address">
                    </div>

                     <div class="form-group">
                     <div class="row">
                     <div class="col-sm-8">
                     <label>File upload</label>
                      <input type="file" name="img" onchange="readURL(this);" class="file-upload-default">
                      <div class="input-group">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                        </div>
                      </div>

                        <div class="col-sm-4">
                          <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png"/> 
                           </div>
                         </div>
                       </div>

                      <button type="submit" class="btn btn-success mr-2 mt-4 float-right">Submit</button>                
                          </form>
                        </div>
                       
                      </div>
                    </div>
                  </div>


          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr class="bg-primary text-white">
                            <th>S/N #</th>
                            <th>Store Name</th>
                            <th>Store Location</th>
                            <th>Store Logo</th>
                            <th>Store Products #</th>
                            <th>Date Created</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>1</td>
                            <td>Shoprite</td>
                            <td>Ikeja, Lagos</td>
                            <td>New York</td>
                            <td>150</td>
                            <td>2012/08/03</td>
                            <td>
                              <label class="badge badge-success btn-inverse-success">Approved</label>
                            </td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Konga</td>
                            <td>Victoria Island, Lagos</td>
                            <td>Brazil</td>
                            <td>450</td>
                            <td>2015/04/01</td>
                            <td>
                              <label class="badge badge-success btn-inverse-success">Approved</label>
                            </td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Country gorceries</td>
                            <td>oshodi, Lagos</td>
                            <td>Tokyo</td>
                            <td>100</td>
                            <td>2010/11/21</td>
                            <td>
                              <label class="badge badge-success btn-inverse-success">Approved</label>
                            </td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Reed Supermarket</td>
                            <td>lagos-island, Lagos</td>
                            <td>Tokyo</td>
                            <td>21</td>
                            <td>2016/01/12</td>
                            <td>
                              <label class="badge badge-success btn-inverse-success">Approved</label>
                            </td>
                            <td>
                            <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Ebeanor Supermarket</td>
                            <td>Lekki, Lagos</td>
                            <td>Tokyo</td>
                            <td>2100</td>
                            <td>2017/12/28</td>
                            <td>
                              <label class="badge badge-success btn-inverse-success">Approved</label>
                            </td>
                            <td>
                              <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>                        
                          </tr>
                        <tr>
                            <td>6</td>
                            <td>Hubmart</td>
                            <td>Lekki, Lagos</td>
                            <td>Tokyo</td>
                            <td>80</td>
                            <td>2000/10/30</td>
                            <td>
                              <label class="badge badge-success btn-inverse-success">Approved</label>
                            </td>
                            <td>
                              <button class="btn-sm btn-outline-warning btn-icon"> <i class="ti-pencil"></i> Edit</button>
                            </td>                        
                          </tr>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


        </div>
@endsection