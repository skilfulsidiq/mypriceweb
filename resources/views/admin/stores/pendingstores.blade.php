@extends('layouts.backend')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
        <li class="breadcrumb-item"><a class="flip" href="{{url()->previous()}}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="All Listed Stores">All Listed Stores</li>
    </ol>
</nav>



<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="row">
            <div class="col-12 col-xl-7">
                <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <div class="mb-3 mb-xl-0">
                        <button class="btn btn-warning text-white" data-toggle="modal" data-target="#createstore"><i
                                class="ti-pencil-alt"></i> Create Store
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createstore" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-white"><i class="ti-pencil-alt"></i> Create Store</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="{{route('admin.store.add')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="Store Name">Store Name</label>
                        <input type="text" class="form-control" name="store_name" placeholder="Store Name">
                    </div>

                     {{--<div class="form-group">--}}
                     {{--<label for="Store Address">Store Address</label>--}}
                            {{--<textarea class="form-control" id="storeaddress" name="store_address" placeholder="Store Address" cols="20" rows="3"></textarea>--}}
                        {{--</div>--}}

                    <!-- <div class="form-group">
                        <label for="Store Address">Store Address</label>
                        <input type="text" class="form-control" name="store_address" placeholder="Store Address">
                    </div> -->

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-8">
                                <label>File upload</label>
                                <input type="file" name="store_image" onchange="readURL(this);" class="file-upload-default">
                                <div class="input-group">
                                    <input type="text" name="store_image" class="form-control file-upload-info"
                                        disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-warning text-white" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <img id="upz" class="mt-4" style="width:100px; height:100px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="editstore" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="editform" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="Store Name">Store Name</label>
                        <input type="text" id="editstorename" class="form-control" name="store_name" placeholder="Store Name">
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label for="Store Address">Store Address</label>--}}
                        {{--<input type="text" id="editstoreaddress" class="form-control" name="store_address" placeholder="Store Address">--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-8">
                                <label>File upload</label>
                                <input type="file" name="store_image" id="editstoreimage" onchange="readURL(this);"
                                    class="file-upload-default">
                                <div class="input-group">
                                    <input type="text" name="store_image" id="editstoreimage2" class="form-control file-upload-info"
                                        disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary mr-2 mt-4 float-right">Update</button>
                </form>
            </div>

        </div>
    </div>
</div>


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="order-listing" class="table">
                        <thead>
                            <tr class="bg-success text-white">
                                <th>S/N #</th>
                                <th>Store Name</th>
                                <th>Store Location</th>
                                <th>Store Logo</th>
                                <th>Store Products #</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($stores as $store)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$store->store_name}}</td>
                                <td>{{$store->store_address}}</td>
                                <td>
                                    <img src="{{$store->store_image}}" style=" width:50px;height: 50px;">
                                </td>
                                <td>{{$store->totalsproduct}}</td>
                                <td>
                                @if($store->store_status == 1)
                                        <label class="badge badge-success btn-inverse-success">Approved </label>
                                        @elseif($store->store_status==0) 
                                        <label class="badge badge-success btn-inverse-danger">Pending </label>
                                        @endif
                                </td>
                              <td>
                                  @if($store->store_status == 1)
                                      <a href="{{route('admin.store.deactivate',$store->slug)}}" class="btn-sm btn-outline-success btn-icon">
                                          <i class="ti-cancel"></i>
                                          Deactivate</a>
                                  @elseif($store->store_status==0)
                                      <a href="{{route('admin.store.activate',$store->slug)}}" class="btn-sm btn-outline-success btn-icon">
                                          <i class="ti-check"></i>
                                          Activate</a>
                                  @endif

                              </td>
                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>
</div>
<script>
    $(document).ready(function () {

        $('#editstore').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var storeid = button.data('storeid');
            var formaction = button.data('editform');
            var storename = button.data('storename');
            var storeaddress = button.data('storeaddress');
            var storeimage = button.data('storeimage');
            var modal = $(this);
            modal.find('.modal-header #headertitle').text('Edit ' + storename);
            modal.find('.modal-body #editform').attr('action', formaction);
            modal.find('.modal-body #editstorename').val(storename);
            modal.find('.modal-body #editstoreaddress').val(storeaddress);
            modal.find('.modal-body #upz').attr('src', storeimage)
            modal.find('.modal-body #editstoreimage').val(storeimage)
            modal.find('.modal-body #editstoreimage2').val(storeimage)

        })
    });

</script>
@endsection


