@extends('layouts.backend')
@section('content')


    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed Stores">Registered Store Users</li>
        </ol>
    </nav>



    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <button class="btn btn-warning text-white" data-toggle="modal" data-target="#createuser"><i
                                        class="ti-pencil-alt"></i> Create Store User
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createuser" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i> Register User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('admin.user.add')}}" method="post" >
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name"> Name</label>
                                    <input type="text" class="form-control" name="name"
                                           placeholder=" Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Address">Email</label>
                                    <input type="email"  class="form-control" name="email"
                                           placeholder="Email Address" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Address">User Type</label>
                                    <select name="usertype" id="" class="form-control">
                                        @foreach($usertypes as $t)
                                            <option value="{{strtolower($t->user_type_name)}}">{{ucfirst($t->user_type_name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Address">Password</label>
                                    <input type="password"  class="form-control" name="password"
                                           placeholder="password" required>
                                </div>
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--@foreach ($roles as $role)--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<label for="Store Address">--}}
                                            {{--<input type="radio"  class="form-control" value="{{$role->id}}" name="roles[]"--}}
                                                   {{--placeholder="password" required>--}}
                                            {{--{{ucfirst($role->name)}}</label>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="edituser" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="headertitle"><i class="ti-pencil-alt"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form  id="editform"  method="post" >
                        {{csrf_field()}}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name"> Name</label>
                                    <input type="text" id="name" class="form-control" name="name"
                                           placeholder=" Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Address">Email</label>
                                    <input type="email" id="email" class="form-control" name="email"
                                           placeholder="Email Address" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Address">User Type</label>
                                    <select name="usertype" id="usertype" class="form-control">
                                        @foreach($usertypes as $t)
                                            <option value="{{strtolower($t->user_type_name)}}">{{ucfirst($t->user_type_name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Address">Password</label>
                                    <input type="password"  class="form-control" name="password"
                                           placeholder="password" >
                                </div>
                            </div>
                        </div>


                        <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table id="order-listing" class="table">
                            <thead>
                            <tr class="bg-success text-white">
                                <th>S/N #</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Store Name</th>
                                <th>User Type</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($storeusers as $users)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$users->name}}</td>
                                    <td>{{$users->email}}</td>
                                    <td><a href="{{route('admin.users.storebranch.users',$users->storeslug)}}">{{ucwords($users->store_name)}}</a></td>
                                    <td>{{$users->usertype}}</td>
                                    <td>{{(new Carbon\Carbon($users->created_at))->diffForHumans()}}</td>
                                    <td>

                                        <button
                                        data-toggle="modal" data-target="#edituser"
                                        data-name="{{$users->name}}"
                                        data-editform="{{route('admin.user.update',$users->slug)}}"
                                        data-email="{{$users->email}}"
                                        data-usertype="{{$users->usertype}}"
                                        class="btn-sm btn-outline-warning btn-icon"><i class="ti-pencil"></i> Edit
                                        </button>
                                        {{--<button onclick="location.href='{{route('admin.users.storebranch.users',$users->storeslug)}}'" class="btn-sm btn-outline-primary btn-icon"><i class="ti-eye"></i>--}}
                                            {{--view users--}}
                                        {{--</button>--}}
                                        <button onclick="location.href='{{route('admin.user.delete',$users->slug)}}'" class="btn-sm btn-outline-danger btn-icon"><i class="ti-trash"></i>
                                            Delete
                                            </button>
                                        <!-- <a class="btn-sm btn-out-line-light btn-icon btn-danger"
                                           href="{{route('admin.user.delete',$users->slug)}}"><i
                                                    class="ti-link"></i> Delete</a> -->

                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>


    </div>
</div>
    <script>
        $(document).ready(function(){

            $('#edituser').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) ;// Button that triggered the modal
                var name = button.data('name');
                var formaction = button.data('editform');
                var email = button.data('email');
                var usertype = button.data('usertype');
                var modal = $(this);
                modal.find('.modal-header #headertitle').text('Edit ' + name);
                modal.find('.modal-body #editform').attr('action',formaction);
                modal.find('.modal-body #name').val(name);
                modal.find('.modal-body #email').val(email);
                modal.find('.modal-body #usertype').val(usertype);

            })
        });
    </script>
@endsection
