@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
            <li class="breadcrumb-item text-white "><a class="text-white" href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="Settings">Change Password</li>
        </ol>
    </nav>

     <div class="row">
            <div class="offset-2 col-md-8 ">
                <div class="card">
                    <div class="card-body">

                        <h3 class="text-center mt-2 text-success">Reset Password</h3>

                        <div class="card card-inverse-warning mt-4 mb-2">
                            <div class="card-body">
                                <p class="card-text">
                                    Note: Your privacy is very important to us. To better
                                    serve you, the form information you enter is recorded in real time.</p>
                            </div>
                        </div>

                        <form action="" class="forms-sample mt-4" method="post">
                           

                            <div class="row">
                                <div class="col-md-6 offset-3">
                                    <div class="form-group">
                                        <label for="New Password">New Password</label>
                                        <input type="password" name="" class="form-control" placeholder="New Password">
                                    </div>
                                </div>


                                <div class="col-md-6 offset-3">
                                    <div class="form-group">
                                        <label for="Confirm New Password">Confirm New Password</label>
                                        <input type="password" name="" class="form-control" placeholder="Confirm New Password">
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success mr-2 mt-2 float-right">Reset Password</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @endsection('content')