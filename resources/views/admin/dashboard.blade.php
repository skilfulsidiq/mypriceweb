@extends('layouts.backend')

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="row">
            <div class="col-12 col-xl-5 mb-4 mb-xl-0">
                <h4 class="font-weight-bold">Hi, {{ucwords(Auth::user()->name)}}</h4>
                <h4 class="font-weight-normal mb-0">Administrator Dashboard,</h4>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-md-center text-xl-left">Total Products</p>
                <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                    <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{$products}}</h3>
                    <i class="ti-shopping-cart-full icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p class="mb-0 mt-2 text-success">All Customs Value</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-md-center text-xl-left">Total Stores</p>
                <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                    <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{$stores}}</h3>
                    <i class="ti-shopping-cart icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p class="mb-0 mt-2 text-success">Customer Review</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-md-center text-xl-left">Registered Users</p>
                <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                    <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{$users}}</h3>
                    <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p class="mb-0 mt-2 text-success">Joined New User</span></p>
            </div>
        </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title text-md-center text-xl-left">Running Ads</p>
                <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                    <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{$adverts}}</h3>
                    <i class="ti-gallery icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p class="mb-0 mt-2 text-success">Active Ads</span></p>
            </div>
        </div>
    </div>
</div>

<!-- <div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card bg-primary border-0 position-relative">
            <div class="card-body">
                <p class="card-title text-white">Performance Overview</p>
                <div id="performanceOverview" class="carousel slide performance-overview-carousel position-static pt-2"
                    data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-4 mt-md-0">
                                        <div class="icon icon-a text-white mr-3">
                                            <i class="ti-cup icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Revenue</h3>
                                                <h3 class="mb-0">34040</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">+34040</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">0.036%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                                        <div class="icon icon-b text-white mr-3">
                                            <i class="ti-bar-chart icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Sales</h3>
                                                <h3 class="mb-0">$9672471</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">-7.34567</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">2.036%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                                        <div class="icon icon-c text-white mr-3">
                                            <i class="ti-shopping-cart-full icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Purchases</h3>
                                                <h3 class="mb-0">6358</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">+9082</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">35.54%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-4 mt-md-0">
                                        <div class="icon icon-a text-white mr-3">
                                            <i class="ti-cup icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Revenue</h3>
                                                <h3 class="mb-0">34040</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">+34040</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">0.036%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                                        <div class="icon icon-b text-white mr-3">
                                            <i class="ti-bar-chart icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Sales</h3>
                                                <h3 class="mb-0">$9672471</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">-7.34567</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">2.036%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                                        <div class="icon icon-c text-white mr-3">
                                            <i class="ti-shopping-cart-full icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Purchases</h3>
                                                <h3 class="mb-0">6358</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">+9082</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">35.54%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-4 mt-md-0">
                                        <div class="icon icon-a text-white mr-3">
                                            <i class="ti-cup icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Revenue</h3>
                                                <h3 class="mb-0">34040</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">+34040</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">0.036%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                                        <div class="icon icon-b text-white mr-3">
                                            <i class="ti-bar-chart icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Sales</h3>
                                                <h3 class="mb-0">$9672471</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">-7.34567</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">2.036%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 item">
                                    <div class="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                                        <div class="icon icon-c text-white mr-3">
                                            <i class="ti-shopping-cart-full icon-lg ml-3"></i>
                                        </div>
                                        <div class="content text-white">
                                            <div class="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                                                <h3 class="font-weight-light mr-2 mb-1">Purchases</h3>
                                                <h3 class="mb-0">6358</h3>
                                            </div>
                                            <div class="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                                                <h5 class="mb-0">+9082</h5>
                                                <div class="d-flex align-items-center">
                                                    <i class="ti-angle-down mr-2"></i>
                                                    <h5 class="mb-0">35.54%</h5>
                                                </div>
                                            </div>
                                            <p class="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of
                                                sessions within the date range. It is the period time a user is
                                                actively engaged with your website, page or app, etc</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#performanceOverview" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#performanceOverview" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="row">
    <div class="col-12 col-xl-12 mb-4 mb-xl-2">
        <h4 class="font-weight-normal">Manage Stores</h4>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="order-listing" class="table">
                        <thead>
                            <tr class="bg-success text-white">
                                <th>S/N #</th>
                                <th>Store Name</th>
                                <th>Store Logo</th>
                                <th>Store Branches</th>
                                <th>Store Products #</th>
                                <th>Date Created</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($storelist as $store)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$store->store_name}}</td>
                                <td><img class="img-lg rounded-circle" src="{{$store->store_image}}" alt="" style="width: 50px; height: 50px;"></td>
<!-- <td style="background:url('{{$store->store_image}}') center; background-size:cover; height: 30px; width:80%;"></td> -->
                                <td>{{$store->totalbranches}}</td>
                                <td>{{$store->totalproducts}}</td>
                                <td>{{(new Carbon\Carbon($store->created_at))->diffForHumans()}}</td>
                                <td>
                                    @if($store->store_status == 1)
                                    <label class="badge badge-success btn-inverse-success">Approved </label>
                                    @elseif($store->store_status==0)
                                    <label class="badge badge-success btn-inverse-danger">Pending </label>
                                    @endif

                                </td>

                                <td>
                                    <a href="{{route('admin.store.branches',$store->slug)}}" class="btn-sm btn-warning text-white btn-icon "> <i class="ti-eye"></i> View</a>
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
@endsection