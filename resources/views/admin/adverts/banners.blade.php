@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed Categories"> All Banners</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <button class="btn btn-warning text-white" data-toggle="modal" data-target="#createbanner"><i
                                        class="ti-pencil-alt"></i> Add Banner
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createbanner" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i> Add Promo </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('admin.banner.add')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Banner Sponsor</label>
                                    <select name="storeid" id="sponsor" class="form-control">
                                        <option >Select Sponsor</option>
                                        @foreach($stores as $store)
                                            <option value="{{$store->id}}">{{ucwords($store->store_name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Promo Name</label>
                                    <input type="text" name="promoname" id="promoname" class="form-control" required>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group offset-2">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label>Upload Banner Image</label>
                                        <input type="file" name="bannerimage" onchange="readURL(this);" class="file-upload-default">
                                        <div class="input-group">
                                            <input type="text" name="bannerimage" class="form-control file-upload-info"
                                                   disabled placeholder="Upload Image">
                                            <span class="input-group-append">
                                        <button class="file-upload-browse text-white btn btn-warning" type="button">Upload</button>
                                    </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group offset-2">
                                 <label>Upload Promo Images</label>
                                 <input type="file" class="form-control" id="images" name="promoimage[]" onchange="preview_images();" multiple/>


                            </div>
                            <div class="row offset-2" id="image_preview" style="margin-bottom:5px;" >
                            </div>

                        </div>

                        <div class="row ">
                            <div class="offset-2 col-md-8">
                                <div class="form-group">
                                    <label for="Store Name">Advert Expire date</label>
                                    <input type="date" name="promoexpire" class="form-control" id="promoexpire">
                                </div>

                            </div>
                        </div>

                        <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>
                <script>
                        function preview_images()
                        {
                         var total_file=document.getElementById("images").files.length;
                         for(var i=0;i<total_file;i++)
                         {
                          $('#image_preview').append("<div class='col-md-3'><img style='width:50px; height:50px;' class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
                         }
                        }
             </script>


            </div>
        </div>

    </div>

    <div class="modal fade" id="editbanner" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="edittitle"><i class="ti-pencil-alt"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="editform" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Banner Sponsor</label>
                                    <select name="storeid" id="sponsor" class="form-control">
                                        <option >Select Sponsor</option>
                                        @foreach($stores as $store)
                                            <option value="{{$store->id}}">{{ucwords($store->store_name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Promo Name</label>
                                    <input type="text" name="promoname" id="promoname" class="form-control" required>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group offset-2">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label>Upload Banner Image</label>
                                        <input type="file" name="bannerimage" onchange="readURL(this);" class="file-upload-default">
                                        <div class="input-group">
                                            <input type="text" name="bannerimage" class="form-control file-upload-info"
                                                   disabled placeholder="Upload Image">
                                            <span class="input-group-append">
                                        <button class="file-upload-browse text-white btn btn-warning" type="button">Upload</button>
                                    </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group offset-2">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label>Upload Promo Image</label>
                                        <input type="file" name="promoimage" onchange="readURLTwo(this);" class="file-upload-default">
                                        <div class="input-group">
                                            <input type="text" name="promoimage" class="form-control file-upload-info"
                                                   disabled placeholder="Upload Image">
                                            <span class="input-group-append">
                                        <button class="file-upload-browse text-white btn btn-warning" type="button">Upload</button>
                                    </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <img id="upztwo" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row ">
                            <div class="offset-2 col-md-8">
                                <div class="form-group">
                                    <label for="Store Name">Advert Expire date</label>
                                    <input type="date" name="promoexpire" class="form-control" id="promoexpire">
                                </div>

                            </div>
                        </div>

                        <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">


            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr class="bg-success text-white">
                                        <th>S/N #</th>
                                        <th>Promo Title</th>
                                        <th>Banner Sponsor</th>
                                        <th>Banner Img</th>
                                        <th>Promo Image</th>
                                        <th>Date Created</th>
                                        <th>Expire Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($banners as $b)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{@$b->promoname}}</td>
                                            <td>{{@$b->store->store_name}}</td>
                                            <td><img src="{{@$b->bannerimage}}" alt="banner"></td>
                                            <td><img src="{{$b->promoimage}}" alt="img" style="width:50px; height: 50px;"></td>
                                            <td>{{(new Carbon\Carbon($b->created_at))->diffForHumans()}}</td>

                                            <td>{{(new Carbon\Carbon($b->promoexpire))->diffForHumans()}}</td>
                                            <td>
                                                <button data-toggle="modal" data-target="#editbanner" data-promoname="{{$b->promoname}}"

                                                        data-promoimage="{{$b->promoimage}}"
                                                        data-sponsor="{{$b->store->id}}"
                                                        data-bannerimage="{{@$b->bannerimage}}"
                                                        data-expire="{{$b->promoexpire}}"
                                                        data-editform="{{route('admin.banner.update',$b->slug)}}"
                                                        class="btn-sm btn-outline-warning btn-icon"><i class="ti-pencil"></i>
                                                    Edit
                                                </button>

                                                <button onclick="location.href='{{route('admin.banner.delete',$b->slug)}}'" class="btn-sm btn-outline-danger btn-icon"><i class="ti-trash"></i>
                                                    Delete
                                                </button>
                                            <!-- <a href="{{route('admin.banner.delete',$b->slug)}}" class="btn-sm btn-outline-danger btn-icon">
                                                    <i class="ti-trash"></i>
                                                    Delete</a> -->
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>




    <script>
        $(document).ready(function () {
            $('#editbanner').on('show.bs.modal', function (event) {
                var btn = $(event.relatedTarget);
                var sponsor = btn.data('sponsor');
                var promoimage = btn.data('promoimage');
                var promoname = btn.data('promoname');
                var bannerimage = btn.data('bannerimage');
                var expire = btn.data('expire');
                var actionform = btn.data('editform');
                var modal = $(this);

                modal.find('.modal-header #edittitle').text('Edit ' + promoname);
                modal.find('.modal-body #editform').attr('action', actionform);
                modal.find('.modal-body #sponsor').val(sponsor);
                modal.find('.modal-body #promoexpire').val(expire);
                modal.find('.modal-body #promoname').val(promoname);
                modal.find('.modal-body #upz').attr('src', bannerimage);
                modal.find('.modal-body #upztwo').attr('src', promoimage);

            })
        })

    </script>


@endsection
