@extends('layouts.backend')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light breadcrumb-custom bg-inverse-success">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="All Listed Categories"> All Adverts</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="mb-3 mb-xl-0">
                            <button class="btn btn-warning text-white" data-toggle="modal" data-target="#createadvert"><i
                                        class="ti-pencil-alt"></i> Add Advert
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createadvert" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white"><i class="ti-pencil-alt"></i> Add Advert </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('admin.advert.add')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Advert Type</label>
                                    <select name="adverttype" id="adverttype" class="form-control">
                                        <option >Select advert type</option>
                                        @foreach($adverttypes as $ad)
                                            <option value="{{$ad->adtype_name}}">{{ucwords($ad->adtype_name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Advert Sponsor</label>
                                    <input type="text" name="advert_sponsor" class="form-control">

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="offset-3 col-md-6">                        
                                <div class="form-group">
                             <label for="Store Name">Advert Expire date</label> 
                                    <input type="date" name="advert_expire" class="form-control" id="advert_expire">
                                </div>

                          </div>

                <!-- <div class="col-md-6">
                                <div class="form-group">
                             <label for="Store Name">Advert Expire date</label> 
                                    <input type="date" name="advert_expire" class="form-control" id="advert_expire">
                                </div>
                            </div>  -->
                        </div>



                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label>Upload Advert Image</label>
                                    <input type="file" name="advert_image" onchange="readURL(this);" class="file-upload-default">
                                    <div class="input-group">
                                        <input type="text" name="advert_image" class="form-control file-upload-info"
                                               disabled placeholder="Upload Image">
                                        <span class="input-group-append">
                                        <button class="file-upload-browse text-white btn btn-warning" type="button">Upload</button>
                                    </span>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-warning text-white mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="editadvert" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-white" id="edittitle"><i class="ti-pencil-alt"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="editform" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Advert Type</label>
                                    <select name="adverttype" id="adverttype" class="form-control">
                                        <option >Select advert type</option>
                                        @foreach($adverttypes as $ad)
                                            <option value="{{$ad->adtype_name}}">{{ucwords($ad->adtype_name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Advert Sponsor</label>
                                    <input type="text" name="advert_sponsor" id="sponsor" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Store Name">Advert Expire date</label>
                                    <input type="date" name="advert_expire" class="form-control" id="advert_expire">
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>



                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label>File upload</label>
                                    <input type="file" name="advert_image"    onchange="readURL(this);" class="file-upload-default">
                                    <div class="input-group">
                                        <input type="text" name="advert_image" id="adimage" class="form-control file-upload-info"
                                               disabled placeholder="Upload Image">
                                        <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <img id="upz" class="mt-4" style="width:50px; height:50px;" src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Images-icon.png" />
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success mr-2 mt-4 float-right">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">


            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr class="bg-success text-white">
                                        <th>S/N #</th>
                                        <th>Advert type</th>
                                        <th>Advert sponsor</th>
                                        <th>Advert Image</th>
                                        <th>Date Created</th>
                                        <th>Expire Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($adverts as $advert)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$advert->adverttype}}</td>
                                            <td>{{@$advert->store->store_name}}</td>
                                            <td><img src="{{$advert->advert_image}}" alt="img" style="width:50px; height: 50px;"></td>
                                            <td>{{(new Carbon\Carbon($advert->created_at))->diffForHumans()}}</td>

                                            <td>{{(new Carbon\Carbon($advert->expire_date))->diffForHumans()}}</td>
                                            <td>
                                                <button data-toggle="modal" data-target="#editadvert"
                                                        data-type="{{$advert->adverttype}}"
                                                        data-sponsor="{{$advert->advert_sponsor}}"
                                                        data-image="{{$advert->advert_image}}"
                                                        data-expire="{{$advert->expire_date}}"
                                                        data-editform="{{route('admin.advert.update',$advert->slug)}}"
                                                        class="btn-sm btn-outline-warning btn-icon"><i class="ti-pencil"></i>
                                                    Edit
                                                </button>

                                                 <button onclick="location.href='{{route('admin.advert.delete',$advert->slug)}}'" class="btn-sm btn-outline-danger btn-icon"><i class="ti-trash"></i>
                                            Delete
                                            </button>
                                                <!-- <a href="{{route('admin.advert.delete',$advert->slug)}}" class="btn-sm btn-outline-danger btn-icon">
                                                    <i class="ti-trash"></i>
                                                    Delete</a> -->
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>

    </div>


    <script>
        $(document).ready(function () {
            $('#editadvert').on('show.bs.modal', function (event) {
                var btn = $(event.relatedTarget);
                var type = btn.data('type');
                var sponsor = btn.data('sponsor');
                var image = btn.data('image');
                var expire = btn.data('expire');
                var actionform = btn.data('editform');
                var modal = $(this);

                modal.find('.modal-header #edittitle').text('Edit ' + type);
                modal.find('.modal-body #editform').attr('action', actionform);
                modal.find('.modal-body #adverttype').val(type);
                modal.find('.modal-body #sponsor').val(sponsor);
                modal.find('.modal-body #advert_expire').val(expire);
                modal.find('.modal-body #upz').attr('src', image);
                modal.find('.modal-body #adimage').val(image);
            })
        })

    </script>


@endsection
